using System;
using SharedLibrary.DAL;
using SharedLibrary.DL;
using System.Data;
using System.Collections.Generic;
using System.Security;


namespace SharedLibrary.LIB
{
	public class DemoCreator
	{

		public static Property Create()
		{
			var property = CreateProperty ();
			CreateRates (property);
			CreateFees (property);
			CreateDiscounts (property);
			CreateTaxes (property);
			CreateDeposits (property);
			CreateTemplates (property);
			CreateInquiries (property);
			CreateReservedWord (property);
			CreateMailServer (property);
			CreateListing (property);

			return property;
		}

		private static Property CreateProperty()
		{
			var demo = Property.DemoProperty ();
			if(demo != null)  // Remove old demo
				demo.Delete ();
			demo = new Property ();
			demo.Name = "Demonstration Property";
			demo.ShortName = "DEMO Property";
			demo.Address1 = "2191 South Kihei Rd";
			demo.Address2 = "Unit 2117";
			demo.City = "Kihei";
			demo.State = "HI";
			demo.Country = "USA";
			demo.Zip = "96793";
			demo.ComplexName = "Maui Vista Resort";
	//		demo.Area = "South Kihei";
			demo.About = @"An updated very comfortable one bedroom condo near Charlie Young Beach. Unit has a Memory Foam King bed in the Master and a double Murphy in the living room.";
			demo.ShortDescription = "Across from Charlie Young beach with Memory Foam King";
			demo.Sleeps = 4;
			demo.ResponseEmail = "demo_user@vrwizard.com";
			demo.RejectEmail = "demo_user@vrwizard.com";
			demo.CopyEmail = "demo_user@vrwizard.com";
			demo.WeekendDays = "FRI SAT";
			demo.Weekday = 196.00m;
			demo.Weekend = 203.00m;
			demo.Weekly = 1295.00m;
			demo.Monthly = 5600.00m;
			demo.MinNights = 7;
			demo.ExtraGuestRate = 10.00m;
			demo.RatePeople = 2;
			demo.AvailableDocument = "Available with Quote";
			demo.NotAvailableDocument = "Sorry";
			demo.MinimumNightsDocument = "";
//			demo.WelcomeDocument = "Arrival";
			//			demo.ContractDocument = "Contract";  // Part of Pro
			demo.SecurityCode1 = "1342";
			demo.Url1 = "demo_property.vrwizard.com";
			//			demo.Url2 				// Part of Pro
			//			demo.Uri  				// Not used in Demo
			demo.IsDemo = true;


			demo.Save ();
			return demo;

			Console.WriteLine ("Demo Created");
		}



		private static void CreateRates(Property prop)
		{
			Rate r;
//			r = prop.NewRate ();
//			r.Name = "Default";
//			r.Weekday = 126.00m;
//			r.Weekend = 154.00m;
//			r.WeekendDays = "FRI, SAT";
//			r.Weekly = 770.00m;
//			r.Monthly = 2800.00m;
//			r.RateType = "DEF";
//			r.Save ();

			r = prop.NewRate ();
			r.Name = "Low 2014";
			r.StartDate = new DateTime (2014, 03, 15);
			r.EndDate = new DateTime (2014, 12, 15);
			r.Weekday = 126.00m;
			r.Weekend = 154.00m;
			r.WeekendDays = "FRI, SAT";
			r.Weekly = 770.00m;
			r.Monthly = 2800.00m;
			r.RateType = "SEA";
			r.MinNights = 6;
			r.Save ();

//			r = prop.NewRate ();
//			r.Name = "Low 2015";
//			r.StartDate = new DateTime (2015, 04, 01);
//			r.EndDate = new DateTime (2015, 12, 15);
//			r.Weekday = 112.00m;
//			r.Weekly = 700.00m;
//			r.Monthly = 2800.00m;
//			r.MinNights = 5;
//			r.RatePeople = 2;
//			r.ExtraGuestRate = 10.00m;
//			r.RateType = "SEA";
//			r.Save ();


//			// Possible Pro items
//			r = prop.NewRate ();
//			r.Name = "Wedding";
//			r.RateType = "PRO";
//			r.Weekly = 600.00m;
//			r.Save ();

//			r = prop.NewRate();
//			r.Name = "Remaining Dates";
//			r.StartDate = new DateTime (2014, 2, 1);
//			r.EndDate = new DateTime (2014, 3, 30);
//			r.Weekly = 700.00m;
//			r.RateType = "SPL";
//			r.Save ();
		}

		private static void CreateFees(Property prop)
		{
			var f = prop.NewFee ();
			f.Name = "Cleaning";
			f.Amount = 100.00m;
			f.Required = true;
			f.Taxable = true;
			f.Save ();

			f = prop.NewFee ();
			f.Name = "Damage Insurance";
			f.Amount = 49.00m;
			f.Required = true;
			f.Taxable = false;
			f.Save ();
//
//			f = prop.NewFee ();
//			f.Name = "Pool Usage";
//			f.Amount = 50.00m;
//			f.Required = false;
//			f.Taxable = true;
//			f.Save ();
		}

		private static void CreateDiscounts(Property prop)
		{
			var d = prop.NewDiscount();
			d.Name = "General";
			d.Amount = 1.00m;
			d.Save ();
//
//			d = prop.NewDiscount ();
//			d.Name = "Returning Guest";
//			d.Percentage = 0.10m;
//			d.Save ();

//			d = prop.NewDiscount ();
//			d.Name = "Friends & Family";
//			d.Percentage = 0.20m;
//			d.Save ();
		}

		private static void CreateTaxes(Property prop)
		{
			var t = prop.NewTax ();
			t.Name = "Hawaii";
			t.Percentage = .1342m;
			t.Required = true;
			t.Save ();
//
//			t = prop.NewTax ();
//			t.Name = "Other Tax";
//			t.Percentage = .10m;
//			t.Required = false;
//			t.Save ();
		}


		private static void CreateDeposits(Property prop)
		{
//			var d = prop.NewDeposit ();
//			d.Name = "Party Deposit";
//			d.Description = "Refundable 10% Damage Deposit";
//			d.Percentage = 0.10m;
//			d.Refundable = true;
//			d.Save ();

			var d = prop.NewDeposit ();
			d.Name = "Damage Deposit";
			d.Description = "REfundable Damage Depsoit";
			d.Amount = 200.00m;
			d.Refundable = true;
			d.Save ();

		}



		private static void CreateMailServer (Property prop)
		{
			var m = prop.NewMailServer ();
			m.ServerName = "north.securenet-server.net";
			m.UserLogin = "test_inquiry@vrwizard.com";
			m.UserPassword = "!QAZxsw2";
			m.Port = 993;
			m.InBox = "InBox";
			m.Archive = "VrWizard/Archive";
			m.ApiKey = "DEMOPROPERTYTOKEN";
			m.Save ();
		}

		private static void CreateTemplates(Property prop)
		{
			string text = "@today\n\n@Aloha\nYour Vacation condo is currently available for arrival @Arrivaldate departing @departuredateshort for a total of @nights nights, however it is quiet popular. The complex has three tennis courts and three pools, plus the fabulous Charley Young beach is across the street. Charley Young is a great sandy beach for swimming or just relaxing. Also Cove Beach is short walk down the street if you are interested in beginning surfing. Another great beach, Kamaole I, is right next to Charley Young. @CommentResponse\nYour price for the condo is as follows:\n\nLength of Stay:  @nights nights\nNumber of Guests:  @guests\n\nBase Rental:  @baserental\n\nCleaning Fee:  @cleaning\nHawaii Tax (@hawaii_percentage%):  @hawaii\nDamage Insurance:  @DAMAGE_INSURANCE\nTotal Rental Charge:  @totalcharge\n\nI know you will find no other unit in the south Kihei area 'tricked' out like this one.\n\nIf you would like additional information or like to reserve it, please call me at 702-483-0810.\n\n@signed";
			var t = prop.NewTemplate ();
			t.Name = "Available with Quote";
			t.TemplateType = "TXT";
			t.Content = text;
			t.Subject = "RE: Your inquiry through @ChannelName/@listingNumber ";
			t.Save ();

			text = "@today\n\n@Aloha\nYour Vacation condo is currently not available for arrival @Arrivaldate departing @departuredateshort for a total of @nights nights, however the calendar is up to date. The complex has three tennis courts and three pools, plus the fabulous Charley Young beach is across the street. Charley Young is a great sandy beach for swimming or just relaxing. Also Cove Beach is short walk down the street if you are interested in beginning surfing. Another great beach, Kamaole I, is right next to Charley Young.@CommentResponse\nYour price for the condo for the requested period is as follows:\n\nLength of Stay:  @nights nights\nNumber of Guests:  @guests\n\nBase Rental:  @baserental\n\nCleaning Fee:  @cleaning\nHawaii Tax (@hawaii_percentage%):  @hawaii\nDamage Insurance:  @DAMAGE_INSURANCE\nTotal Rental Charge:@totalcharge|R|10\n\nI know you will find no other unit in the south Kihei area 'tricked' out like this one.\n\nIf you would like additional information or like to consider a different time, please call me at 702-483-0810.\n@CommentResponse\n@signed\n\n";
			t = prop.NewTemplate ();
			t.Name = "Sorry";
			t.TemplateType = "TXT";
			t.Content = text;
			t.Subject = "RE: Your inquiry through @channelName/@listingNumber ";
			t.Save ();


		}

		private static void CreateInquiries(Property prop)
		{
			Reservation res;
			Inquiry inq;

			inq = prop.NewInquiry ();
			inq.GuestName = "Collette LaCour";
			inq.Adults = 2;
			inq.Children = 0;
			var arrive = inq.ArrivalDate = new DateTime(2014, 8, 1 );
			var depart = inq.DepartureDate = arrive.AddDays (10);
			inq.Comment = "Please confirm price and availability";
			inq.Phone = "702-555-1212";
			inq.Channel = "HOMEAWAY";
			inq.ListingNumber = "H1000";
			inq.Email = "dresponse@vrwizard.com";
			inq.ReplyTo = inq.Email;
			inq.CurrentState = Inquiry.InquiryState.Responded;;
			inq.Save ();
			inq.Reserve (Reservation.Availability.Tentative);

			inq = prop.NewInquiry ();
			inq.GuestName = "Abigail Kiss";
			inq.Adults = 2;
			inq.Children = 0;
			inq.ArrivalDate = DateTime.Now.AddDays(230);
			inq.DepartureDate = DateTime.Now.AddDays (244);
			inq.Comment = "Please confirm price and availability";
			inq.Phone = "702-555-1212";
			inq.Channel = "HOMEAWAY";
			inq.ListingNumber = "H1000";
			inq.Email = "dresponse@vrwizard.com";
			inq.ReplyTo = inq.Email;
			inq.CurrentState = Inquiry.InquiryState.New;;
			inq.Save ();

			inq = prop.NewInquiry ();
			inq.GuestName = "Theresa Enyedi";
			inq.Adults = 2;
			inq.Children = 0;
			inq.ArrivalDate = new DateTime(2015, 08, 05);
			inq.DepartureDate = new DateTime (2015, 08, 25);
			inq.Comment = "Our Honeymoon";
			inq.Phone = "702-555-1212";
			inq.Channel = "VRBO";
			inq.ListingNumber = "V1000";
			inq.Email = "dresponse@vrwizard.com";
			inq.ReplyTo = inq.Email;
			inq.CurrentState = Inquiry.InquiryState.New;
			inq.InquiryDate = new DateTime (2014, 1, 28); //DateTime.Today.AddDays (-10);
			inq.Save ();

//
//			inq = prop.NewInquiry ();
//			inq.GuestName = "Donald French";
//			inq.Adults = 2;
//			inq.Children = 0;
//			inq.ArrivalDate = DateTime.Now.AddDays (1);
//			inq.DepartureDate = DateTime.Now.AddDays (10);
//			inq.Comment = "Our Honeymoon";
//			inq.Phone = "702-555-1212";
//			inq.Channel = "VRBO";
//			inq.ListingNumber = "209114";
//			inq.Email = "dresponse@vrwizard.com";
//			inq.ReplyTo = inq.Email;
//			inq.CurrentState = Inquiry.InquiryState.New;
//			inq.InquiryDate = DateTime.Today.AddDays (-15);
//			inq.Save ();

			inq = prop.NewInquiry ();
			inq.GuestName = "Mini Knight";
			inq.Adults = 2;
			inq.Children = 0;
			inq.ArrivalDate = DateTime.Now.AddDays (10);
			inq.DepartureDate = DateTime.Now.AddDays (15);
			inq.Comment = "Our Honeymoon";
			inq.Phone = "702-555-1212";
			inq.Channel = "FLIP";
			inq.ListingNumber = "F1000";
			inq.Email = "dresponse@vrwizard.com";
			inq.ReplyTo = inq.Email;
			inq.CurrentState = Inquiry.InquiryState.New;
			inq.InquiryDate = new DateTime(2014, 2, 12);
			inq.Save ();


			inq = prop.NewInquiry ();
			inq.GuestName = "Roger Guest";
			inq.Adults = 2;
			inq.Children = 0;
			inq.ArrivalDate = arrive;
			inq.DepartureDate = depart;
			inq.Comment = "Our Vacation";
			inq.Phone = "702-555-1212";
			inq.Channel = "VRENTAL";
			inq.ListingNumber = "V1001";
			inq.Email = "dresponse@vrwizard.com";
			inq.ReplyTo = inq.Email;
			inq.CurrentState = Inquiry.InquiryState.New;
			inq.InquiryDate = new DateTime(2014, 2, 10);
			inq.Save ();
			//	res = inq.Reserve (Reservation.Availability.Reserved); // Saves the inquiry and the reservation
			//res.Save ();

			inq = prop.NewInquiry ();
			inq.GuestName = "William LaCour";
			inq.Adults = 1;
			inq.Children = 0;
			inq.ArrivalDate = DateTime.Now.AddDays (100);
			inq.DepartureDate = DateTime.Now.AddDays(107);
			inq.Comment = "relaxation";
			inq.Phone = "702-555-1212";
			inq.Channel = "VILLA";
			inq.ListingNumber = "V1002";
			inq.Email = "dresponse@vrwizard.com";
			inq.ReplyTo = inq.Email;
			inq.CurrentState = Inquiry.InquiryState.New;
			inq.InquiryDate = new DateTime(2014, 2, 2);
			inq.Save ();
		
			res = new Reservation (prop.ID, DateTime.Now.AddDays(100), DateTime.Now.AddDays(102), 
				Reservation.Availability.Blocked, "Repair Service", "Repairs");
			res.Save ();
			inq = Inquiry.Find (res.InquiryID);
			inq.CurrentState = Inquiry.InquiryState.Responded;
			inq.Save ();

			inq = prop.NewInquiry ();
			inq.GuestName = "Noah Kiss";
			inq.Adults = 1;
			inq.Children = 0;
			inq.ArrivalDate = DateTime.Now.AddDays(230);
			inq.DepartureDate = DateTime.Now.AddDays (244);
			inq.Comment = "relaxation";
			inq.Phone = "702-555-1212";
			inq.Channel = "VRBO";
			inq.ListingNumber = "209114";
			inq.Email = "dresponse@vrwizard.com";
			inq.ReplyTo = inq.Email;
			inq.CurrentState = Inquiry.InquiryState.Responded;;
			inq.Save ();
			inq.Reserve (Reservation.Availability.Reserved);
	
//
//			inq = prop.NewInquiry ();
//			inq.GuestName = "Tenative Guest";
//			inq.Adults = 1;
//			inq.Children = 0;
//			inq.ArrivalDate = DateTime.Now.AddDays (200);
//			inq.DepartureDate = DateTime.Now.AddDays (220);
//			inq.Comment = "relaxation";
//			inq.Phone = "702-555-1212";
//			inq.Channel = "VRBO";
//			inq.ListingNumber = "209114";
//			inq.Email = "dresponset@vrwizard.com";
//			inq.ReplyTo = inq.Email;
//			inq.CurrentState = Inquiry.InquiryState.Responded;
//			inq.Save ();
//			inq.Reserve (Reservation.Availability.Tentative);
//			inq.Save ();

		}

		private static void CreateListing(Property prop)
		{
			var lis = prop.NewListing ();
			lis.ChannelCode = "VRBO";
			lis.ChannelName = "VRBO";
			lis.ListingNumber = "V1000";
			lis.Save ();

			lis = prop.NewListing ();
			lis.ChannelCode = "HOMEAWAY";
			lis.ChannelName = "HomeAway";
			lis.ListingNumber = "H1000";
			lis.Save ();	

			lis = prop.NewListing ();
			lis.ChannelCode = "FLIP";
			lis.ListingNumber = "F1000";
			lis.ChannelName = "FlipKey";
			lis.Save ();

			lis = prop.NewListing ();
			lis.ListingNumber = "V1001";
			lis.ChannelCode = "VRENTAL";
			lis.ChannelName = "VacationRentals";
			lis.Save ();

			lis = prop.NewListing ();
			lis.ListingNumber = "V1002";
			lis.ChannelCode = "VILLA";
			lis.ChannelName = "Villas4Vacations";
			lis.Save ();
		}

		private static void CreateReservedWord(Property prop)
		{
			var rw = prop.NewReservedWord ();
			rw.Name = "Aloha";
			rw.Text = "Aloha @FirstName,\n";
			rw.WordType = "C";
			rw.Save ();

			rw = prop.NewReservedWord ();
			rw.Name = "Signed";
			rw.Text = "Mahalo\n";
			rw.WordType = "C";
			rw.Save ();

		}




//			CreateQuote ();
//			CreateReservation ();





//		inquiries << Inquiry.new(name: 'Theresa Enyedi', adults: 2, children: 0, arrival: 'May 25, 2013', departure: 'June 5, 2013', comments: 'Please confirm price and availability',
//		                         phone: '702 555-1212', property_num: '209114', channel: 'VRBO', email: 'demo_enyedi@vrwizard.com')
//			inquiries << Inquiry.new(name: 'Donald French', adults: 2, children: 1, arrival: 'Dec. 15, 2013', departure: 'Jan 15, 2014', comments: 'Very nice unit',
//			                         phone: '808 555-1212', property_num: '292391', channel: 'HOME', email: 'demo_french@vrwizard.com')
//
//				inquiries << Inquiry.new(name: 'Don Johnson', adults: 3, children: 0, arrival: 'April 20, 2013', departure: 'April 30, 2013', comments: 'Child is 2',
//				                         phone: '808 555-1212', property_num: '292392', channel: 'HOME', email: 'demo_don@vrwizard.com')
//
//				inquiries << Inquiry.new(name: 'Theresa French', adults: 2, children: 0, arrival: 'August 30, 2013', departure: 'Sept 15, 2013', comments: 'Please confirm price and availability',phone: '702 555-1212', property_num: '209115', channel: 'VRBO', email: 'demo_theresa@vrwizard.com')
//
//				inquiries << Inquiry.new(name: 'William Mark', adults: 2, children: 0, arrival: 'June 1, 2013', departure: 'July 16, 2013', comments: 'Please confirm price and availability',phone: '702 555-1212', property_num: '209115', channel: 'VRBO', email: 'demo_theresa@vrwizard.com')
//				inquiries << Inquiry.new(name: 'George Jones', adults: 1, children: 0, arrival: 'Sept. 15, 2013', departure: 'Oct. 15, 2013', comments: 'Please confirm price and availability',phone: '702 555-1212', property_num: '209115', channel: 'VRBO', email: 'demo_theresa@vrwizard.com')
//

	}
}

