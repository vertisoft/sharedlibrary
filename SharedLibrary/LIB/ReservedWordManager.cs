using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Provider;

namespace SharedLibrary.LIB
{
	public class ReservedWordManager
	{
		Context _context;

		public ReservedWordManager( Context context)
		{
			_context = context;
		}

		// Currently not being called words are setup in ReserveWord. 
		public void AddSystemReservedWords()
		{
//			Console.WriteLine ("------Adding All System Reserved Words-----");
			AddPropertyWords ();
			AddInquiryWords ();
			AddQuoteWords ();
		}

		public void AddWord(string word)
		{
//			Console.WriteLine ("adding word {0}", word);
			word = word.Replace (' ', '_');
//			UserDictionary.Words.AddWord (_context, "@" + word, 1, LocaleType.All);
		}


		private void AddPropertyWords()
		{
			AddWord("PropertyName");
			AddWord("PropertyShortName");
			AddWord("PropertyAddress1");
			AddWord("PropertyAddress2");
			AddWord("PropertyCity");
			AddWord("PropertyState");
			AddWord("PropertyDescription");
			AddWord("ComplexName");
			AddWord("Sleeps");
			AddWord("VoicePhone");
			AddWord("Fax");
			AddWord("SecurityCode");
			AddWord("ResponseEmail");
			AddWord("URL");
		}

		private  void AddInquiryWords()
		{
			AddWord("FirstName");
			AddWord("LastName");
			AddWord("GuestName");
			AddWord("Adults");
			AddWord("Children");
			AddWord("Guests");
			AddWord("ArrivalDateFull");
			AddWord("DepartureDateFull");
			AddWord("ArrivalDateShort"); 
			AddWord("DepartureDateShort");
			AddWord("ArrivalDate");
			AddWord("DepartureDate");
			AddWord("Nights"); 
			AddWord("NightsFull");
			AddWord("GuestPhone");
			AddWord("Comment");
			AddWord("ListingNumber");
			AddWord("ChannelCode");
			AddWord("ChannelName");
			AddWord("GuestEmail");
			AddWord("InquiryDate");
		}

		private void AddQuoteWords()
		{
			AddWord("BaseRental");
			AddWord("ExtraGuestCharge");
			AddWord("Taxable");
			AddWord("NonTaxable");
			AddWord("TotalFees");
			AddWord("TotalDiscounts");
			AddWord("TotalDeposits");
			AddWord("TotalTaxes");
			AddWord("TotalCharge");
			AddWord("DiscountedRental");
			AddWord("MinimumNights");
			AddWord("CommentResponse");
		}
	}
}

