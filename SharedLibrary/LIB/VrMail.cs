﻿using System;
using Android.OS;
using Android.Renderscripts;
using Limilabs.Mail;
using Limilabs.Client.IMAP;
using Limilabs.Client;
using Limilabs.Mail.Licensing;
using Limilabs.Mail.Fluent;
using Limilabs.Mail.Headers;
using System.Text.RegularExpressions;


namespace SharedLibrary
{
	public class VrMail
	{
		string _messageID;
		string _text;
		string _bodyAsHTML;
		string _bodyAsText;
		string _bodyTextFromHTML;
		DateTime _receivedDate;
		DateTime? _sentDate;
		string _subject;
		string _from;
		string _to;
		string _replyTo;
		long _uid;
		string _travelerSource;
		string _listing;
		string _listingSource;
		string _raw_eml;


		public VrMail (IMail iMail)
		{
			Initialize (iMail);
		}

		public string RawEml {
			get {return _raw_eml;}
			set {_raw_eml = value;}
		}
		public string TravelerSource {
			get {return _travelerSource;}
			set {_travelerSource = value;}
		}

		public string Listing {
			get {return _listing;}
			set {_listing = value;}
		}

		public string ListingSource {
			get {return _listingSource;}
			set {_listingSource = value;}
		}

		public string MessageID {
			get{return _messageID;}
			set{ _messageID = value; }
		}

		public string Text {
			get{return _text;}
			set{_text = value;}
		}

		public string BodyAsHTML {
			get{return _bodyAsHTML;}
			set{_bodyAsHTML = value;}
		}

		public string BodyAsText {
			get{return _bodyAsText;}
			set{_bodyAsText = value;}
		}

		public string BodyTextFromHTML {
			get{return _bodyTextFromHTML;}
			set{_bodyTextFromHTML = value;}
		}

		public DateTime ReceivedDate {
			get {return _receivedDate;}
			set { _receivedDate = (value == null ? new DateTime() : (DateTime)value);}
		}

		public DateTime? SentDate {
			get {return _sentDate;}
			set {_sentDate = value;}
		}

		public string Subject {
			get {return _subject;}
			set {_subject = value;}
		}

		public string From {
			get {return _from;}
			set {_from = value;}
		}

		public string To {
			get {return _to;}
			set {_to = value;}
		}

		public string ReplyTo {
			get {return _replyTo;}
			set {_replyTo = value;}
		}

		public long UID {
			get {return _uid;}
			set {_uid = value;}
		}

		private void Initialize(IMail email)
		{
//			Console.WriteLine ("start initialize");
			_from = GetFrom (email);
			_bodyAsHTML = email.GetBodyAsHtml ();
			_bodyAsText = email.GetBodyAsText ();
			_bodyTextFromHTML = email.GetTextFromHtml ();
			_messageID = email.MessageID;
//			Console.WriteLine ("messageid = {0}", _messageID);
			_receivedDate = (email.Date == null ? new DateTime() : (DateTime)email.Date);
//			Console.WriteLine ("ReceivedData = {0}", _receivedDate);
			_subject = email.Subject;
//			Console.WriteLine ("subject = {0}", _subject);
			_text = email.Text;
//			Console.WriteLine ("text ");
			_to = GetTo (email);
//			Console.WriteLine ("_to = {0}", _to);

			_travelerSource = email.Document.Root.Headers["X-Inquiry-TravelerSource"];
//			Console.WriteLine ("travelerSource = {0}", _travelerSource);
			_listing = email.Document.Root.Headers["x-inquiry-listing"];
//			Console.WriteLine ("Listing = {0}", _listing);
			_listingSource = email.Document.Root.Headers["x-inquiry-listingsource"];
//			Console.WriteLine ("listingSource = {0}", _listingSource);
			_replyTo = email.Document.Root.Headers["Reply-To"];
//			Console.WriteLine ("done initialize Relpy = {0}", _replyTo);
		}
			
		private string GetFrom (IMail _email)
		{
			if (_email.From.Count >0){
				var from = _email.From[0];
				var mb = from.GetMailboxes ();
				return mb [0].Address;

			}
			return null;
		}

		private string GetTo (IMail _email)
		{
			if (_email.To [0].Name == @"undisclosed-recipients")
				return null;
//			if (Regex.Match (_email.To[0].Name, @"undisclosed-recipients").Success)
//				return null;

			if (_email.To.Count >0){
				var to = _email.To[0];
				var mb = to.GetMailboxes ();
				return mb [0].Address;

			}
			return null;
		}

			
	}
}

