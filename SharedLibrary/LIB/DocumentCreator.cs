using System;
using System.Security.Cryptography;
using System.Runtime.InteropServices;

namespace SharedLibrary.LIB
{
	public class DocumentCreator
	{
		string _baseTemplate;

		public DocumentCreator (string template)
		{
			_baseTemplate = template;
		}

		string _document;
		public string Document 
		{
			get {
				if (_document == null)
					_document = Transform ();
				return _document;
			}

			set {
				_document = value;
			}
		}


		public static string FillTemplate(string template)
		{
			var creator = new DocumentCreator (template);
			return creator.Transform ();
		}

		string Transform()
		{
//			FillReservedWords
			return "";
		}
	}
}

