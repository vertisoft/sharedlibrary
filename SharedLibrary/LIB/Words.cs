using System;
using System.Linq;
using System.Collections.Generic;
using SharedLibrary.DL;
using System.Net.NetworkInformation;
using System.Diagnostics;
using System.Collections;
using System.Net;
using System.CodeDom.Compiler;

namespace SharedLibrary.LIB
{
	public class Words
	{

		private List<Word> _reservedWords;
		public List<Word> ReservedWord {
			get {
		
				return _reservedWords;
			} 
		}

		public List<Word> ReservedWords(Inquiry inq)
		{
			return _reservedWords;
		}



		public Words(Property property)
		{
			_reservedWords = property.ReservedWords;
		}

		public List<Word> FillWords(Inquiry inq)
		{
			FillGeneralWords ();
			FillPropertyWords (inq.Property);
			FillInquiryWords (inq);
			var quote = inq.CurrentQuote;
			FillQuoteWords (inq.CurrentQuote);
			FillQuoteDetailWords (inq.CurrentQuote);
			return _reservedWords;
		}

		private void FillGeneralWords()
		{
			UpdateWord ("Today", DateTime.Today.ToString ("MMMMM dd, yyyy"));
		}

		private void FillPropertyWords(Property prop)
		{
			UpdateWord ("PropertyName", prop.Name);
			UpdateWord ("PropertyShortName", prop.ShortName);
			UpdateWord ("PropertyAddress1", prop.Address1);
			UpdateWord ("PropertyAddress2", prop.Address2);
			UpdateWord ("PropertyCity", prop.City);
			UpdateWord ("PropertyState", prop.State);
			UpdateWord ("PropertyDescription", prop.About);
			UpdateWord ("ComplexName", prop.ComplexName);
			UpdateWord ("Sleeps", prop.Sleeps.ToString ("D"));
			UpdateWord ("VoicePhone", prop.Voice);
			UpdateWord ("Fax", prop.Fax);
			UpdateWord ("SecurityCode", prop.SecurityCode1);
			UpdateWord ("ResponseEmail", prop.ResponseEmail);
			UpdateWord ("URL", prop.Url1);

		}

		private void FillInquiryWords(Inquiry inq)
		{
			var names = inq.GuestName.Split (' ');
			UpdateWord ("FirstName", names.First ());
			UpdateWord ("LastName", string.Join(" ", names.Skip(1)));
			UpdateWord ("GuestName", inq.GuestName);
			UpdateWord ("Adults", inq.Adults.ToString ());
			UpdateWord ("Children", inq.Children.ToString ());
			UpdateWord ("Guests", GenerateGuests (inq.Adults, inq.Children));
			UpdateWord ("ArrivalDateFull", inq.ArrivalDate.ToString ("D"));
			UpdateWord ("DepartureDateFull", inq.DepartureDate.ToString ("D"));
			UpdateWord ("ArrivalDateShort", inq.ArrivalDate.ToShortDateString()); 
			UpdateWord ("DepartureDateShort", inq.DepartureDate.ToShortDateString ());
			UpdateWord ("ArrivalDate", inq.ArrivalDate.ToString ("MMMMM dd, yyyy"));
			UpdateWord ("DepartureDate", inq.DepartureDate.ToString ("MMMMM dd, yyyy"));
			var nights = (inq.DepartureDate - inq.ArrivalDate).Days;
			string nightsString;
			if (nights == 1)
				nightsString = String.Format ("{0} night", nights);
			else
				nightsString = String.Format ("{0} nights", nights);
			UpdateWord ("Nights", nights.ToString ("D")); 
			UpdateWord ("NightsFull", nightsString);
			UpdateWord ("GuestPhone", inq.Phone);
			UpdateWord ("Comment", inq.Comment);
			UpdateWord ("ListingNumber", inq.ListingNumber);
			UpdateWord ("ChannelCode", inq.Channel);
			Channel channel = Channel.FindByChannel (inq.Channel);
			if (channel == null)
				UpdateWord ("ChannelName", "");
			else
				UpdateWord ("ChannelName", channel.Name); 
			UpdateWord ("GuestEmail", inq.Email);
			UpdateWord ("InquiryDate", inq.InquiryDate.ToShortDateString ());
		}


		private void FillQuoteWords(Quote quote)
		{
			UpdateWord ("ExtraGuestFee", quote.ExtraGuestsCharge.ToString ("C"));
			UpdateWord ("BaseRental", quote.BaseRental.ToString ("C"));
			UpdateWord ("Taxable", quote.Taxable.ToString ("C"));
			UpdateWord ("NonTaxable", quote.NonTaxable.ToString ("C"));
			UpdateWord ("TotalFees", quote.TotalFees.ToString ("C"));
			UpdateWord ("TotalDiscounts", quote.TotalDiscounts.ToString("C"));
			UpdateWord ("TotalDeposits", quote.TotalDeposits.ToString ("C"));
			UpdateWord ("TotalTaxes", quote.TotalTaxes.ToString ("C"));
			UpdateWord ("TotalCharge", quote.TotalCharge.ToString ("C"));
			UpdateWord ("DiscountedRental",quote.DiscountedRental.ToString ("C"));
			UpdateWord ("MinimumNights", quote.MinNights.ToString ("D")); 
			if(!String.IsNullOrWhiteSpace(quote.CommentResponse))
//				;
//			}
//			else
				UpdateWord ("CommentResponse", "\n" + quote.CommentResponse + "\n");
//			else {
//				UpdateWord ("CommentResponse", "\n");
//			}
		}

		private void FillQuoteDetailWords(Quote quote)
		{
			List<QuoteDetail> quoteDetails = quote.Details ();
			foreach(QuoteDetail qd in quoteDetails)
			{
				var dName = qd.DetailName.Replace (" ", "_");
//				Console.WriteLine ("Updating {0} with {1}", dName, qd.DetailCalculatedAmountString);

				UpdateWord (dName, qd.DetailCalculatedAmount.ToString ("C"));
				UpdateWord (dName + "_Percentage", qd.DetailPercentageString);
//				Console.WriteLine ("Added {0} = {1}", dName + "_Percentage", qd.DetailPercentageString);
//				UpdateWord (dName + "_PerNight", qd.DetailPerNightAmount.ToString ("C"));
			} 

		}

		private void UpdateWord(string key, string value)
		{
			var word = Find (key);
			if (word == null)
			{
//				Console.WriteLine ("Adding AddHoc word {0}", key);
				word = new Word ();
				word.KeyWord = key;
				word.WordType = "A";   // AdHoc word
				_reservedWords.Add (word);
			}
//			Console.WriteLine ("Updating {0} with {1}", key, value);
			word.StringValue = value;
			word.DataType = 0;
		}

		public Word Find(string value)
		{

//			Console.WriteLine ("In Find Looking for {0}", value);
//			foreach(Word w in _reservedWords)
//			{
//				Console.WriteLine ("Word = [{0}] = [{1}]", w.KeyWord, w.StringValue);
//			}
//
			Word word = _reservedWords.FirstOrDefault (item => item.KeyWord.ToLower () == value.ToLower ());
//			Word word = _reservedWords.First(item => item.KeyWord == value);
//			Console.WriteLine ("  Found [{0}]", word.KeyWord);
			return word;
		}

		public List<Word> Like(string value)
		{
//			Console.WriteLine ("In Like looking for {0}", value);

			var results = _reservedWords.Where(rw => rw.KeyWord.ToLower().StartsWith(value.ToLower ()));
//			Console.WriteLine ("  Found {0} matches", results.ToList().Count);
			return results.ToList ();
		}
	
		private string GenerateGuests(int adults, int children)
		{
			string guests = "";
			if (adults == 1)
				guests = "1 Adult, ";
			else
				guests = adults.ToString () + " Adults, ";

			if (children == 1)
				guests += "1 Child";
			else if (children == 0)
				guests += "No Children";
			else
				guests += children.ToString () + " Children";
			return guests;
		}

	}
}

