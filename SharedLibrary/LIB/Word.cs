using System;
using SharedLibrary.DL;
using System.Reflection.Emit;
using System.Collections.Generic;
using System.Dynamic;
using System.Configuration;

namespace SharedLibrary.LIB
{
	public class Word
	{
		public string   KeyWord{ get; set;}
		public int		DataType{ get; set;} // 0 = string, 1 = int, 2 = Decimal 3 - Date
		public string   WordType{ get; set;}
		public string   StringValue{ get; set;}
		public int		IntValue{ get; set;}
		public decimal	DecimalValue{ get; set;}
		public DateTime DateValue{ get; set;}

	}
}

