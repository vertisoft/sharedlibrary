using System;

using System.Collections.Generic;
using SharedLibrary.SAL.MailProcessing;
using SharedLibrary.DL;
using System.Text.RegularExpressions;
using System.IO;
using Limilabs.Mail;
using Limilabs.Client.IMAP;
using Limilabs.Client;
using Limilabs.Mail.Licensing;





namespace SharedLibrary
{
	public class MailReceiver
	{
		private string _hostName;
		private int _port;
		private string _userName;
		private string _password;
		private string _inbox;
		private string _archive;
//		private SslMode _security;
		private Inquiry inq;
		Imap _client = null;
		MailServer ms;

		public MailReceiver (Property property)
		{


			var licenseStatus = LicenseHelper.Load ();
//			Console.WriteLine ("\nLicense Status = {0}\n", licenseStatus.ToString ());

			try {
//				Console.WriteLine ("Calling property MailServer for {0}", property.Name);
				ms = property.MailServer;
//				Console.WriteLine ("We have a mail server");

				_port = ms.Port;  //143 if none
				_hostName = ms.ServerName;

				_userName = ms.UserLogin;
				_password = ms.UserPassword;
				_inbox = ms.InBox;
				_archive = ms.Archive;
			} catch (Exception ex) {
//				Console.WriteLine ("Mail Server does not exist in MailReceiver");
				return;
				// throws exception if no mailserver setup
			}
		}


		public void ProcessNewMail (bool archiveInquiries)
		{
//			Console.WriteLine ("Processing new mail");
			if (_hostName == null) {
				throw new Exception ("MailServer is not set up");
			}
			List<long> processedInquiries = new List<long>();

			using (Imap client = new Imap ()) {
//				Console.WriteLine ("Connecting to server {0} with {1}/{2} on port {3}", _hostName, _userName, _password, _port);
				client.Connect (_hostName);
				client.Login (_userName, _password);
				client.SelectInbox ();
//				Console.WriteLine ("Connected to server");
				List<long> uidList = client.Search (Flag.Unseen);
//				Console.WriteLine (	"ProcessMail Number of inquiries = {0}", uidList.Count);
				foreach (long uid in uidList) {
					if (ProcessInquiry (client, uid)) {
						Console.WriteLine ("   ------   Inquiry was processed");
						processedInquiries.Add (uid);
					}
				}
				if (archiveInquiries) {
					Console.WriteLine ("Archive the inquiry");
					IndicateProcessed (client, processedInquiries);
				}
				else
					Console.WriteLine ("Do not archive Inquiry");
				client.Close ();
			}
		}
	
		private bool ProcessInquiry(Imap _client, long uid)
		{
			IMail email = new MailBuilder ().CreateFromEml (_client.PeekMessageByUID (uid));
//			Console.WriteLine ("Subject =  {0}", email.Subject);
			MailProcessor mp = new MailProcessor (email);
			Inquiry inq = mp.ExtractInquiry ();
			if (inq == null){
//				Console.WriteLine ("---- Invalid ProcessInquiry Not a valid inquiry Skip it");
				return false;
			}
//			Console.WriteLine ("---- Valid Inquiry for {0} from [{1}]", inq.GuestName, inq.Channel);

			return true ;

		}


		private void IndicateProcessed(Imap _client, List<long> uids)
		{
			if (Property.CurrentProperty.IsDemo) {
				Console.WriteLine ("Leave email for Demo");
				return;  //Leave emails there
			}

			Console.WriteLine ("IndicateProcessed not Demo");

			FolderInfo archive = ArchiveFolder (_client);
			foreach(long uid in uids){
				_client.MarkMessageSeenByUID(uid);

				#if !DEBUG
				Console.WriteLine (	"Moving Processed Inquiry to {0}", ms.Archive);
					if (!String.IsNullOrWhiteSpace (ms.Archive)){
						_client.MoveByUID (uid, ms.Archive);
					}
				#endif
			}
		}

		private FolderInfo ArchiveFolder(Imap _client)
		{
			List<FolderInfo> all = _client.GetFolders();
//			foreach(FolderInfo i in all)
//				Console.WriteLine ("Folder [{0}]", i.Name);
			CommonFolders folders = new CommonFolders(all);
			#if DEBUG
			Console.WriteLine("Inbox folder: " + folders.Inbox.Name);
			Console.WriteLine("Sent folder: " + folders.Sent.Name);
			Console.WriteLine("Spam folder: " + folders.Spam.Name);
			#endif

//			var archive = folders.Inbox.Name + "/" + ms.Archive;
			var archive = ms.Archive;
//			FolderInfo folder = all.Find(x => x.Name == ms.Archive);
			FolderInfo folder = all.Find(x => x.Name == archive);
			if (folder == null)
			{
//				Console.WriteLine ("Archive Folder [{0}] does not exist, creating", archive);
				_client.CreateFolder (archive);
//				{
//					Console.WriteLine ("---- Archive Folder was not created");
//					return null;
//				}
				folder = all.Find(x => x.Name == archive);
//				Console.WriteLine ("After Create");
			}
//			Console.WriteLine ("ArchiveFolder is returning {0}", folder.Name);
			return folder;
		}


	}
}

