using System;
using SharedLibrary.DL;
using System.Text.RegularExpressions;
using SharedLibrary.SAL.MailProcessing;
using System.Reflection;
//using Limilabs.Mail;
//using Limilabs.Client.IMAP;
using Limilabs.Client;




namespace SharedLibrary
{
	public class MailProcessor
	{
		private string _mailText;
		private string _emailHTML;
		private VrMail _email;

		public MailProcessor (VrMail email)
		{
//			Console.WriteLine ("initiailaze MailProcessor");
			_email = email;
			_emailHTML = email.BodyAsHTML; //.GetBodyAsHtml ();
			_mailText = email.BodyTextFromHTML; //.GetTextFromHtml ();
//			System.IO.File.WriteAllText(@"../../Samples/flip.html", _emailHTML);
		}

		public Inquiry ExtractInquiry()
		{

			Inquiry inq = null;

//			Console.WriteLine ("Checking for Template receipt");
			var vrwizard = VrWizard.Subject (_email);
			if(vrwizard != null){
//				Console.WriteLine ("Processing VrWizard");
				inq = vrwizard.Extract ();  // not really an inquiry.
				return inq;
			}

//			Console.WriteLine ("Processor Checking for vrbo");
			var vrbo = VRBO.From (_email);
//			Console.WriteLine ("VRBO = {0}", vrbo);
			if (vrbo != null) {
//				Console.WriteLine ("Processor found vrbo");
				if (vrbo.Inquiry == null)
					return null;
				inq = vrbo.Extract ();
//				if(inq != null)
//					inq.Save ();
//				Console.WriteLine ("Saved VRBO/HomeAway inquiry {0}", inq.ID);
				return inq;
			}

//			Console.WriteLine ("Processor Checking for FlipKey");
			var flip = FlipKey.From (_email);
//			Console.WriteLine ("FlipKey = {0}", flip);
			if (flip != null) {
				if (flip.Inquiry == null)
					return null;
//				Console.WriteLine ("Processor found FlipKey");
				inq = flip.Extract ();

				return inq;
			}

//			Console.WriteLine ("Processor Checking for VacationRental.com (VR-VRental");
			var vr = VRentals.From (_email);
//			Console.WriteLine ("VRentals = {0}", flip);
			if (vr != null) {
//				Console.WriteLine ("Processor found VacationRental.com");
				if (vr.Inquiry == null) // skipping valid VR Inquiry
					return null;
				inq = vr.Extract ();
//				if(inq != null)
//				  inq.Save ();
				return inq;
			}

//			Console.WriteLine ("Processor Checking for Villas 4 Vacations");
			var villa = Villas.From (_email);
//			Console.WriteLine ("Villas = {0}", flip);
			if (villa != null) {
				if (villa.Inquiry == null)
					return null;
//				Console.WriteLine ("Processor found Villas 4 vacations");
				inq = villa.Extract ();
//				if(inq != null)
//					inq.Save ();
				return inq;
			}

//			throw new VsException ("Invalid inquiry");
			#if DEBUG
			Console.WriteLine ("Processor did not find a valid Inquiry Skip it subject = {0}", _email.Subject);
			Console.WriteLine ("Text is {0}", _email.Text);
			#endif
			return null;
		}



	}
}

