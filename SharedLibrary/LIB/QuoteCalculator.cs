using System;
using System.Collections.Generic;
using SQLite;
using SharedLibrary.DL;
using SharedLibrary.DAL;
using Mono.Security.X509;



namespace SharedLibrary.LIB
{



	public class QuoteCalculator : BL.Contracts.BusinessEntityBase
	{
		public DateTime ArriveDate{ get; set;}
		public DateTime DepartDate{ get; set;}


		[Indexed]
		public int		 InquiryID { get; set;}

		private Inquiry inquiry{ get; set;}
		private Property property{ get; set;}
//		private List<QuoteDetail> quoteDetails;
		private Quote activeQuote;

		private bool noDatesGiven = false;

		public QuoteCalculator (Inquiry inq)
		{
			inquiry = inq;
//			Console.WriteLine ("QuoteCalculator inq id = {0}", inq.ID);
//			Console.WriteLine ("QuoteCalculator with inquiry for {0}", inq.GuestName);
			InquiryID = inq.ID;
			property = Property.Find (inq.PropertyID);
			ArriveDate = inq.ArrivalDate;
			DepartDate = inq.DepartureDate;
		}

		public QuoteCalculator(Quote quote, Inquiry inq)
		{
			inquiry = inq;
//			Console.WriteLine ("QuoteCalculator with quote and inquiry for {0}", inq.GuestName);
			InquiryID = inq.ID;
			property = Property.Find (inq.PropertyID);
			ArriveDate = inq.ArrivalDate;
			DepartDate = inq.DepartureDate;

			activeQuote = quote;
		}

		public Quote ResetToStandard(Quote quote)
		{
			activeQuote = quote;
			CalculateStandardQuote ();
			return activeQuote;
		}

		public Quote Standard()
		{
			activeQuote = new Quote (inquiry);
			CalculateStandardQuote ();
			return activeQuote;
		}

		private Quote CalculateStandardQuote()
		{
//			Console.WriteLine ("Calculate StandardQuote calling Rate.WhichRate");
			if ((inquiry.ArrivalDate == DateTime.MinValue )||( inquiry.DepartureDate == DateTime.MinValue)) {
//				Console.WriteLine ("Arrival date or Departure date missing.");
				noDatesGiven = true;
				activeQuote.MinNights = property.MinNights;
				activeQuote.BaseRental = 0.00m;
				activeQuote.ExtraGuestsCharge = 0.00m;
			} else {
				var rates = Rate.WhichRate (property, ArriveDate, DepartDate);
//				Console.WriteLine ("--------- Calcualte Standard Quote");

//			foreach (Rate r in rates){
//				Console.WriteLine ("       Rate {0} nights {1}", r.Name, r.MinNights);
//			}


				activeQuote.MinNights = rates [0].MinNights;
				CalculateBaseRental (activeQuote);  
//				activeQuote.BaseRental = CalculateBaseRental (rates);
			}
//			Collect individual lists of standard items
			var requiredFees = RequiredFees ();
//			var requiredTaxes = RequiredTaxes ();
			var requiredDeposits = RequiredDeposits ();

			// No required discounts allowed. It would be stupid. if need to provide a special discount gdo it through the specials

			var taxableFees = CalculateFees (requiredFees, true);
			var nonTaxableFees = CalculateFees (requiredFees, false);

			activeQuote.Taxable = activeQuote.DiscountedRental + taxableFees;

			var requiredTaxes = RequiredTaxes ();
			var taxes = CalculateTaxes (requiredTaxes);


			activeQuote.TotalFees = taxableFees + nonTaxableFees;
			activeQuote.TotalTaxes = Math.Round(taxes, 2);
			activeQuote.TotalDeposits = CalculateDeposits (requiredDeposits);
			activeQuote.TotalCharge = activeQuote.BaseRental + activeQuote.ExtraGuestsCharge - activeQuote.TotalDeposits + activeQuote.TotalFees + activeQuote.TotalTaxes;

//			Console.WriteLine ("Base Rental of {0} - Discounts of {1}  + Taxable Fees of {2}, Total Taxable of {3} + Non Taxable Fees of {4} + Taxes of {5} = {6}",
//			                   activeQuote.BaseRental, activeQuote.TotalDiscounts, taxableFees, activeQuote.Taxable, nonTaxableFees, activeQuote.TotalTaxes, activeQuote.TotalCharge);

			activeQuote.CurrentTemplate = activeQuote.DefaultTemplate (null);
			// Everything is calculated. Need to save the quote then link in the detail
//			Console.WriteLine ("Saving a quote for inquiry {0}", activeQuote.InquiryID);
//			Log.Debug ("VrWizard-Calculate", String.Format ("Saving Extra = {0}", activeQuote.ExtraGuestsCharge));
			activeQuote.Save ();
			foreach(QuoteDetail q in requiredFees)
			{
				q.QuoteID = activeQuote.ID;
//				Console.WriteLine ("QUoteDetail Saving Fee{0} - {1}", q.DetailName, q.DetailCalculatedAmountString);
				q.Save ();
			}

			foreach(QuoteDetail q in requiredTaxes)
			{
				q.QuoteID = activeQuote.ID;
//				Console.WriteLine ("QuoteDetail Saving Tax {0} - {1}", q.DetailName,  q.DetailCalculatedAmountString);
				q.Save ();
			}

			foreach(QuoteDetail q in requiredDeposits)
			{
				q.QuoteID = activeQuote.ID;
				q.Save ();
			}

			return activeQuote;
		}

		public  void Recalculate(bool includeBase)
		{
//			Log.Debug ("VrWizard-Calculate", String.Format ("Recalculate with base = {0}, extra = {1}", activeQuote.BaseRental, activeQuote.ExtraGuestsCharge));
			if (activeQuote.ID == 0)
				throw new VsException ("Quote must have been saved inorder to recalculate");

			if (includeBase)
				CalculateBaseRental (activeQuote);

			var includedFees = activeQuote.DetailsByType ("FEE");
			var includedTaxes = activeQuote.DetailsByType ("TAX");
			var includedDiscounts = activeQuote.DetailsByType ("DIS");
			var includedDeposits = activeQuote.DetailsByType ("DEP");
			var taxableFees = CalculateFees (includedFees, true);
			var nonTaxableFees = CalculateFees (includedFees, false);
			;
			activeQuote.TotalDiscounts = CalculateDiscounts (includedDiscounts);
			activeQuote.Taxable = activeQuote.DiscountedRental + taxableFees;
			activeQuote.TotalTaxes = CalculateTaxes (includedTaxes);
			activeQuote.TotalDeposits = CalculateDeposits (includedDeposits);
			activeQuote.TotalFees = taxableFees + nonTaxableFees;
			activeQuote.TotalCharge = activeQuote.BaseRental + activeQuote.ExtraGuestsCharge - activeQuote.TotalDiscounts + activeQuote.TotalFees + activeQuote.TotalTaxes;


//
//			activeQuote.DeleteAllDetail();
//
//			foreach(QuoteDetail q in includedFees)
//			{
//				q.QuoteID = activeQuote.ID;
//				q.Save ();
//			}
//
//			foreach(QuoteDetail q in includedTaxes)
//			{
//				q.QuoteID = activeQuote.ID;
//				q.Save ();
//			}
//
//			foreach(QuoteDetail q in includedDeposits)
//			{
//				q.QuoteID = activeQuote.ID;
//				q.Save ();
//			}
//
//			foreach(QuoteDetail q in includedDiscounts)
//			{
//				q.QuoteID = activeQuote.ID;
//				q.Save ();
//			}
			#if DEBUG
			Console.WriteLine ("Reclculated Quote: Base Rental for quote {8} of {0} + Extra of {8} - Discounts of {1}  + Taxable Fees of {2}, Total Taxable of {3} + Non Taxable Fees of {4} + Taxes of {5} = {6} and deposit of {7}",
				activeQuote.BaseRental, activeQuote.TotalDiscounts, taxableFees, activeQuote.Taxable, nonTaxableFees, activeQuote.TotalTaxes, activeQuote.TotalCharge, activeQuote.TotalDeposits, activeQuote.ExtraGuestsCharge, activeQuote.ID);
			#endif
		}

//		decimal CalculateBaseRental(List<Rate> rates)
		void CalculateBaseRental(Quote quote)
		{
//			Console.WriteLine ("CaucluateBaseRental Calling Rate#WhichRate");
			var rates = Rate.WhichRate (property, ArriveDate, DepartDate);
//			foreach( Rate r in rates)
//			{
//				var days = DaysInRate (r);
//				Console.WriteLine ("Request includes {0} {1} days", days, r.Name);
//			}


			decimal baseRate = 0;
			decimal extraGuestCharge = 0;
			var totalGuests = inquiry.Adults + inquiry.Children;
//			Console.WriteLine ("Adults {0} children {1}", inquiry.Adults, inquiry.Children);



			foreach(Rate r in rates)
			{
				var days = DaysInRate (r);  // Calculate the number of days in the current rate.
				var extraGuests = totalGuests - r.RatePeople;

				var fullRate = FillMissingRate (r);
//				Console.WriteLine ("total Guests = {0}  extra = {1}, people =  {2}", totalGuests, extraGuests, fullRate.RatePeople);
				if (inquiry.TotalNights < 7) {

					baseRate += CalculateDaily (fullRate);
					if (totalGuests > fullRate.RatePeople)
						extraGuestCharge += fullRate.ExtraGuestRate * extraGuests * fullRate.DaysInRate;
					Console.WriteLine ("     {0} daily rate = {1} or {1} per night for {2} nights total = {3}", fullRate.Name, fullRate.Weekday, fullRate.DaysInRate, fullRate.DaysInRate * fullRate.Weekday);

				}
				else if (inquiry.TotalNights < 28) {
					decimal weeklyRate;
					decimal dailyRate;
					decimal baseWeekly;
					decimal weekDaily = 0;

					if(Decimal.Equals(fullRate.Weekly, 0.0))
					{
						weeklyRate = fullRate.Weekday;

					}
					if (fullRate.DaysInRate % 7 == 0) { // Avoid even weeks ending up with odd rates becaus of division.
						baseWeekly = fullRate.Weekly * fullRate.DaysInRate / 7;
					} else {
						weekDaily = fullRate.Weekly / 7;
						baseWeekly = (decimal)fullRate.DaysInRate * weekDaily;
					}
					baseRate += baseWeekly;
					if (totalGuests > fullRate.RatePeople) {

						extraGuestCharge += fullRate.ExtraGuestRate * extraGuests * fullRate.DaysInRate;
//						Console.WriteLine ("extraGuestCharge = {0} = {1} * {2} * {3}", extraGuestCharge, r.ExtraGuestRate, extraGuests, r.DaysInRate);
					}
//					Console.WriteLine ("     {0} weekly rate = {4} or {1} per night for {3} nights total = {2}", r.Name, weekDaily, baseWeekly, r.DaysInRate, r.Weekly);

				}
				else
				{

					var monthDaily = fullRate.Monthly / 28;

					var baseMonthly = fullRate.DaysInRate * monthDaily;
//					Console.WriteLine ("     {0} monthly rate = {4} or {1} per night for {3} nights total = {2}", fullRate.Name, monthDaily, baseMonthly, fullRate.DaysInRate, fullRate.Monthly);
					baseRate += baseMonthly;
					if (totalGuests > fullRate.RatePeople) {
//						Log.Debug ("VrWizard-Calculate", String.Format ("rate = {0}, extraguests = {1}, days = {2}",
//							fullRate.ExtraGuestRate, extraGuests, fullRate.DaysInRate));
						extraGuestCharge += fullRate.ExtraGuestRate * extraGuests * fullRate.DaysInRate;
//						Log.Debug ("VrWizard-Calculate", String.Format ("ExtraGuestCharge = {0}", extraGuestCharge));
					}
				}
			}
			quote.BaseRental = baseRate;
			quote.ExtraGuestsCharge = extraGuestCharge;
//			return baseRate;
		}



		List<QuoteDetail> RequiredFees()
		{
			var allFees = property.Fees ();
			List<QuoteDetail> list = new List<QuoteDetail>();
			foreach(Fee f in allFees)
			{
				if (f.Required)
				{
					var qd = new QuoteDetail ();
					qd.DetailType = "FEE";
					qd.DetailName = f.Name;
					qd.DetailAmount = f.Amount;
					qd.DetailTaxable = f.Taxable;
					qd.DetailRequired = f.Required;
					list.Add (qd);
				}
			}
			return list;
		}

		List<QuoteDetail> RequiredTaxes()
		{
			var allTaxes = property.Taxes ();
			List<QuoteDetail> list = new List<QuoteDetail>();
			foreach(Tax t in allTaxes)
			{
//				Console.WriteLine ("Looking at tax {0} - {1}", t.Name, t.Required);
				if (t.Required)
				{
					var qd = new QuoteDetail ();
					qd.DetailType = "TAX";
					qd.DetailName = t.Name;
					qd.DetailID = t.ID;
					qd.DetailPercentage = t.Percentage;
					qd.DetailRequired = t.Required;
//					Console.WriteLine ("{0} is a required Tax", qd.DetailName);
					list.Add (qd);
//					Console.WriteLine ("{0} was added to required tax list", qd.DetailName);
				}
			}
			return list;
		}

		List<QuoteDetail> RequiredDeposits()
		{
			var allDeposits = property.Deposits ();
			List<QuoteDetail> list = new List<QuoteDetail>();

			foreach(Deposit d in allDeposits)
			{
				if (d.Required)
				{
					var qd = new QuoteDetail();
					qd.DetailType = "DEP";
					qd.DetailAmount = d.Amount;
					qd.DetailName = d.Name;
					qd.DetailPercentage = d.Percentage;
					qd.DetailRequired = d.Required;
					list.Add (qd);

				}
			}
			return list;
		}

		private Rate FillMissingRate(Rate r)
		{
			if (r.Weekend == 0m) 
			{
				r.Weekend = r.Weekday;
				r.WeekendString = r.WeekdayString;
			}
			if (r.Weekly == 0m)
			{
				r.Weekly = r.Weekday * 7;
				r.WeeklyString = r.Weekly.ToString ();
			}
			if(r.Monthly == 0m)
			{
				r.Monthly = r.Weekday * 30;
				r.MonthlyString = r.Monthly.ToString ();
			}
			return r;
		}

		private decimal CalculateAmount(decimal rental, decimal amount, decimal percentage, decimal perNight)
		{
//			Console.WriteLine ("    CalculateAmount = {0} percentage = {1}  perNight = {2}", amount, percentage, perNight);
			decimal _amount =  0.00m;
			if (perNight > 0.00m)
				_amount = perNight * (decimal)inquiry.TotalNights;
			else if (percentage > 0.00m)
				_amount = rental * percentage;
			else
				_amount = amount;
//			Console.WriteLine ("    Returning {0}", _amount);
			return _amount;
		}

		decimal CalculateFees(List<QuoteDetail> details, bool taxable) 
		{
			decimal totals = 0;

//			Console.WriteLine ("Fee Calculating taxable = {0}", taxable);
			foreach(QuoteDetail q in details) {
			
//				Console.WriteLine ("Looking at {0} taxable = {1} QuoteID = {2}", q.DetailName, q.DetailTaxable, q.QuoteID);
//				Console.WriteLine ("  Looking at {0} amount = {1} - PerNight = {2} CalculatedAmount = {3}", q.DetailName, q.DetailAmount, q.DetailPerNightAmount, q.DetailCalculatedAmount);
				if (q.DetailTaxable == taxable) {
//					Console.WriteLine ("    Before Fee {0} amount = {1} - PerNight {2}   Calculated = {3}", q.DetailName, q.DetailAmount, q.DetailPerNightAmount, q.DetailCalculatedAmount);

					q.DetailCalculatedAmount = CalculateAmount (activeQuote.DiscountedRental, q.DetailAmount, q.DetailPercentage, q.DetailPerNightAmount);
//					Console.WriteLine ("    After  Fee {0} amount = {1} - PerNight {2}   Calculated = {3}", q.DetailName, q.DetailAmount, q.DetailPerNightAmount, q.DetailCalculatedAmount);
					totals += q.DetailCalculatedAmount;
				}
//				Console.WriteLine ("    Current total = {0}", totals);
			}
//			Console.WriteLine ("  Returning {0}", totals);
			return totals;
		}

		decimal CalculateDiscounts(List<QuoteDetail> details) 
		{
			decimal totals = 0;
			foreach(QuoteDetail q in details)
			{
//				Console.WriteLine ("    CalculateDiscounts name = {0}  Amount = {1}  percentage = {2}", q.DetailName, q.DetailAmount, q.DetailPercentage);
				totals += q.DetailCalculatedAmount = CalculateAmount (activeQuote.BaseRental, q.DetailAmount, q.DetailPercentage, q.DetailPerNightAmount);

			}
			return totals;
		}

		
		decimal CalculateTaxes(List<QuoteDetail> details) 
		{
			decimal totals = 0;
			decimal tax = 0;
			foreach(QuoteDetail q in details)
			{
//				Console.WriteLine ("----------Calculate tax for {0} percentage = {1}  QuoteID = {2}", q.DetailName, q.DetailPercentage, q.QuoteID);
				if (q.DetailPercentage > 0.00m) {
					tax = Utils.CalculateTax (activeQuote.Taxable, q.DetailPercentage);  //)Math.Round(q.DetailPercentage * activeQuote.Taxable, 2);
//					Console.WriteLine ("{0} of {1}% on a taxable of {2} = {3} ", q.DetailName, q.DetailPercentage, activeQuote.Taxable, tax);
					totals += tax;
					q.DetailCalculatedAmount = tax;
					if (q.QuoteID > 0)
						q.Save ();
				} else {
//					Console.WriteLine ("{0} is a flat tax of {1}", q.DetailName, q.DetailAmount);
					totals += q.DetailAmount;
				}
			}
//			Console.WriteLine ("  Returning total tax of {0}", totals);
//			Console.WriteLine (" ");
			return totals;
		}

		
		decimal CalculateDeposits(List<QuoteDetail> details) 
		{
			decimal totals = 0;
//			Console.WriteLine ("Calculate Deposits");
			foreach(QuoteDetail q in details)
			{
//				Console.WriteLine ("    CalculateDeposits name = {0}  Amount = {1}  percentage = {2}", q.DetailName, q.DetailAmount, q.DetailPercentage);
				totals += q.DetailCalculatedAmount = CalculateAmount (activeQuote.DiscountedRental, q.DetailAmount, q.DetailPercentage, q.DetailPerNightAmount);

			}
			return totals;
		}

		decimal DaysInRate(Rate rate)
		{
			return rate.DaysInRate;

//			TimeSpan span = 
			//	if the rA in the rate and the rD is in the rate
			// 		calculate the number of days between the rA and rD
			//  if the rA is in the rate and the rD is not
			//		calculate the number of days between the rA and the EndDate
			//  if the rA is not in the rate however the rD is
			//		calculate the number of days from the StartDate and the rD
			//  if neither the rA nor rD is in the rate, the number of days is the number of days in the rate.


//			Console.WriteLine ("Calculating Days in rate for {0} of a total of {1}", rate.Name, (DepartDate - ArriveDate).Days);
			DateRange rateRange = new DateRange (rate.StartDate, rate.EndDate);

			if (rateRange.Includes (ArriveDate) && rateRange.Includes (DepartDate)) 
			{
				rate.DaysInRate = (DepartDate - ArriveDate).Days;
				rate.RateStart = ArriveDate;
				rate.RateEnd = DepartDate;
//				Console.WriteLine ("    DaysInRate dates in rate {0} = {1} ({2} - {3})", rate.Name, rate.DaysInRate, ArriveDate.ToShortDateString (), DepartDate.ToShortDateString ());
				return rate.DaysInRate;
			}
				
			if (rateRange.Includes(ArriveDate) && !rateRange.Includes(DepartDate))
			{
				rate.DaysInRate = (rate.EndDate - ArriveDate).Days + 1;  // Add one to cover the last night. 
				rate.RateStart = ArriveDate;
				rate.RateEnd = rate.EndDate;
//				Console.WriteLine ("    DaysInRate dates in rate {0} = {1} ({2} - {3})", rate.Name, rate.DaysInRate, ArriveDate.ToShortDateString (), DepartDate.ToShortDateString ());
				return rate.DaysInRate;
			}
			if (!rateRange.Includes (ArriveDate) && rateRange.Includes (DepartDate))
			{
				rate.DaysInRate = ((rate.EndDate - rate.StartDate).Days);
				rate.RateStart = rate.StartDate;
				rate.RateEnd = rate.EndDate;
//				Console.WriteLine ("    DaysInRate dates in rate {0} = {1} ({2} - {3})", rate.Name, rate.DaysInRate, ArriveDate.ToShortDateString (), DepartDate.ToShortDateString ());
				return rate.DaysInRate;
			}
//			rate.DaysInRate = (rate.EndDate - rate.StartDate).Days + 1;
//			rate.RateStart = rate.StartDate;
//			rate.RateEnd = rate.EndDate;
//			Console.WriteLine ("dates in rate = {0} - {1}", rate.StartDate, rate.EndDate);
			return rate.DaysInRate;

		}

		decimal CalculateDaily(Rate rate) //, DateTime arrivalDate, DateTime departureDate)
		{
			DateTime arrivalDate = inquiry.ArrivalDate;
			DateTime departureDate = inquiry.DepartureDate;
//			Console.WriteLine ("Calculating Daily Rate for {0}", rate.Name);
			var weekDays = NumberDays (arrivalDate, departureDate, true);
//			var weekDays = NumberDays (rate.StartDate, rate.EndDate, true);
			decimal daily = weekDays * rate.Weekday;

			var weekend = (decimal)((int)rate.DaysInRate - weekDays) * rate.Weekend;
//			Console.WriteLine (" CalculateDaily weekdays = {0} = {1}  WeekEnd = {2} = {3}", weekDays, daily, (int)inquiry.Nights - weekDays, weekend);
			return daily + weekend;
		}



		public int NumberDays(DateTime startDate, DateTime endDate, Boolean excludeWeekends) //, List<DateTime> excludeDates)
		{
			int count = 0;
			for (DateTime index = startDate; index < endDate; index = index.AddDays(1))
			{
				if (excludeWeekends && index.DayOfWeek != DayOfWeek.Friday && index.DayOfWeek != DayOfWeek.Saturday)
				{
//					bool excluded = false; ;
//					for (int i = 0; i < excludeDates.Count; i++)
//					{
//						if (index.Date.CompareTo(excludeDates[i].Date) == 0)
//						{
//							excluded = true;
//							break;
//						}
//					}
//
//					if (!excluded)
//					{
						count++;
//					}
				}
			}

			return count;
		}

	}

	public interface IRange<T>
	{
		T Start { get; }
		T End { get; }
		bool Includes(T value);
		bool Includes(IRange<T> range);
	}

	public class DateRange : IRange<DateTime>         
	{
		public DateRange(DateTime start, DateTime end)
		{
			Start = start;
			End = end;
		}

		public DateTime Start { get; private set; }
		public DateTime End { get; private set; }

		public bool Includes(DateTime value)
		{
			return (Start <= value) && (value <= End);
		}

		public bool Includes(IRange<DateTime> range)
		{
			return (Start <= range.Start) && (range.End <= End);
		}
	}
//	DateRange range = new DateRange(startDate, endDate);
//	range.Includes(date)

}

