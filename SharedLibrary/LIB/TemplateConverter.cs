using System;
using SharedLibrary.DL;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Security.Policy;

namespace SharedLibrary.LIB
{
	public class TemplateConverter
	{
		private string _template;
		private Words _words;
		private List<Word> _wordList;
		private Property _property;
		private Inquiry _inquiry;

		public TemplateConverter ( Inquiry inquiry, string template)
		{

// TODO check that the word list is not constantly being refilled
			_inquiry = inquiry;
			_property = inquiry.Property;
//			_template = template;
			_words = new Words(_property);
			_words.FillWords (inquiry);
			_wordList = _words.ReservedWords(inquiry);
		}

//		public Template FindTemplate(string templateName)
//		{
//			var templates = _property.Templates ();
//			Console.WriteLine ("{0} templates", templates.Count);
//			Console.WriteLine ("Template {0} found = {1}", templates[0].Name, templates[0].Content);
//			var template = _property.TemplateByName (templateName);
//		}

//		public string ConvertTemplate(string templateName)
//		{
////			var templates = _property.Templates ();
////			Console.WriteLine ("{0} templates", templates.Count);
////			Console.WriteLine ("Template {0} found = {1}", templates[0].Name, templates[0].Content);
////
////			var template = _property.TemplateByName (templateName);
//			var template = FindTemplate
//			Console.WriteLine ("Template = {0}", template.Content);
////			_template = template.Content;
//			return Convert (template.Content);
//		}

//		public string ConvertSubject(string templateName)
//		{
//			var templates = _property.Templates ();
//			Console.WriteLine ("{0} templates", templates.Count);
//			Console.WriteLine ("Template {0} found = {1}", templates[0].Name, templates[0].Content);
//
//			var template = _property.TemplateByName (templateName);
//			Console.WriteLine ("Subject = {0}", template.Subject);
////			_template = template.Content;
//			return Convert (template.Subject);
//		}

		public string Convert(string text)
		{ 
			if (text == null)
				return "";
//			_template = text;
//			Console.WriteLine ("Starting Convert");
//			const string _pattern = @"{(.*?)}";
			const string _pattern = @"@([a-zA-Z0-9_&|]+)";
//			Console.WriteLine ("Initializing Match");
			string temp;
//			string workString = _template;
			string workString = text;
			do{
//				Console.WriteLine ("Looking for {0} in {1}", _pattern, workString);
				var match =  Regex.Match(workString, _pattern);
				if (!match.Success) {
//					Console.WriteLine (" Nothing Found");
					break;
				}
//				Console.WriteLine ("Found group for {0}", match.Groups [1].Value);
//				var rword = match.Groups[1].Value;
//
//				var word = _words.Find(rword);
//				if (word == null) {
//					Console.WriteLine ("   Not a reserved word.");
//					temp = workString.Replace (match.Groups [0].Value,  "{{" + rword);	
//				}
//				else{
//					Console.WriteLine ("Reserved Word {0} = {1}", rword, word.StringValue);
//					temp = workString.Replace (match.Groups [0].Value,  word.StringValue);	                          
//				}

//				Console.WriteLine ("FormatWord = {0}", match.Groups[1].Value);
				var formatWord = match.Groups[1].Value.Split ('|');
				var rWord = formatWord[0];
//				Console.WriteLine ("rWord =[{0}]", rWord);
				var word = _words.Find (rWord); //match.Groups [1].Value);
				if (word == null){
//					Console.WriteLine ("Word [{0}] was not found substituting blank.", match.Groups [1].Value);
					temp = workString.Replace (match.Groups [0].Value,  "{{" + formatWord[0]);	
//					temp = workString.Replace(match.Groups[0].Value, "");
				}
				else {
//					Console.WriteLine ("Found {0} - {1}", word.KeyWord, word.StringValue);
					if (formatWord.Length > 1){
//						Console.WriteLine ("FormatWord = {0}", match.Groups[1].Value);
						temp = workString.Replace (match.Groups [0].Value, FormatString(formatWord[1], formatWord[2], word.StringValue));
					}
					else
						temp = workString.Replace (match.Groups [0].Value,  word.StringValue);	                          

				}
				workString = temp;
//				Console.WriteLine ("Found {0}", _template);
			} while (true);
			var pattern = @"{{(.+)";
			do {
				var match =  Regex.Match(workString, pattern);
				if (!match.Success) {
	//					Console.WriteLine (" Nothing Found");
					break;
				}
				temp = workString.Replace (match.Groups [0].Value,  "@" + match.Groups[1].Value);
				workString = temp;
			} while (true);
			return workString;
		}

		private string FormatString(string justification, string numSpaces, string value)
		{
//			Console.WriteLine ("Justification = [{0}], spaces = [{1}]", justification, numSpaces);

			int spaces = Utils.ParseInt (numSpaces);
			if (justification == "R")
				return value.PadLeft (spaces);
			else
				return value.PadRight (spaces);
		}

	}
}

