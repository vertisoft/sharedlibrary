﻿using System;
using SharedLibrary.DL;
using RestSharp;
using System.Runtime.InteropServices;
using Limilabs.Client;
using Android.Provider;
using Java.Util;
using System.Collections.Generic;
using System.Data;
using System.Security.Permissions;

using ServiceStack.Text;
using Java.Net;
using System.Collections;
using RestSharp.Deserializers;
using Org.Json;
using Java.Util.Zip;
using Android.Util;
using Android.OS;
using Javax.Xml.Datatype;

namespace SharedLibrary.LIB
{
	public class VrWizardServer
	{
		private string _url = "http://incoming.vrwizard.com";
		//private string _url = "http://10.0.1.42:5000";
//		private string _url = "http://192.168.1.19:5000";
		private RestClient _client;
		private string api_token;
		private MailServer _ms;

		public VrWizardServer()
		{

			_client = new RestClient (_url);
//			Console.WriteLine ("VrWizard Initialized = {0}", _client.ToString ());
		}



		public User CreateAccount(string _name, string _email, string _password)
		{
			var request = new RestRequest ("/create_account", Method.POST);
			request.AddHeader ("Accept", "application/json");
			//request.AddHeader ("Content_type", "application/json");
			request.RequestFormat = DataFormat.Json;
			var user = new ServerUser{email = _email, password = _password, name = _name};
			var json = JsonSerializer.SerializeToString (user);
			request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
			//			string data = String.Format (@"{{email: {0}, password: {1} }}", email, password);
			//			request.AddBody (request.JsonSerializer.Serialize(user));

			var response = _client.Execute (request);
			if (response.StatusCode == System.Net.HttpStatusCode.NotFound) {
				Console.WriteLine ("CreateAccount Account not found");
				return null;
			}


			RestSharp.Deserializers.JsonDeserializer deserial= new JsonDeserializer();
			var JSONObj = deserial.Deserialize<Dictionary<string, string>>(response); 
			_email = JSONObj["email"];
			Console.WriteLine ("createAccount Looking for email {0}", _email);
//			foreach (User usr in User.Users())
//			{
//				Console.WriteLine ("Server User {0} - {1} - {2}", usr.ID, usr.Name, usr.Email);
//			}
			User u = User.FindByEmail (_email);
			if (u == null) {
				Console.WriteLine ("Create Account User [(0)] not found", _email);
				u = new User ();
			}
			u.Name = JSONObj["name"];
			u.Email = _email;
			u.Password = _password;


			u.Token = JSONObj ["api_token"];
			u.Status = JSONObj["status"];
			u.RemoteID = JSONObj["id"];
			var val = u.Save ();


			return u;
		}



		public bool ProcessMail(VrMail vmail, string apiKey )
		{
//			Console.WriteLine ("Starting ProcessMail [{0}]", apiKey);
			var eml = vmail.RawEml;
			var request = new RestRequest ("/mail_reader/", Method.POST);
			request.AddHeader ("Accept", "application/json");
			//request.AddHeader ("Content_type", "application/json");
			request.AddHeader ("X_user_token", apiKey); 
			request.RequestFormat = DataFormat.Json;

			var msg = new ReceivedEmail {message = eml};

			var json = JsonSerializer.SerializeToString (msg);
//			Console.WriteLine ("json = {0}", json);
			request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
			//			string data = String.Format (@"{{email: {0}, password: {1} }}", email, password);
			//			request.AddBody (request.JsonSerializer.Serialize(user));
//			Console.WriteLine ("processMail calling server");
			var response = _client.Execute (request);
//			Console.WriteLine ("Response.Content = {0}", response.Content);
//			Console.WriteLine ("execute Response = {0}", response.StatusCode);
			if (response.StatusCode == System.Net.HttpStatusCode.NotFound) {
//				Console.WriteLine ("email is not inquiry, ignore");
				return false;
			}
			// Found a valid email inquiry

//			Console.WriteLine ("Response Content = {0}", response.Content);
//			Console.WriteLine ("Number of items = {0}",response.ContentLength);
			RestSharp.Deserializers.JsonDeserializer deserial= new JsonDeserializer();

			var data = deserial.Deserialize<Dictionary<string, string>>(response); 
//			Console.WriteLine ("create inquiry");
			Inquiry inq = Inquiry.FindByMessageID (data ["message_id"]);


			if (inq != null)
			{
//				Console.WriteLine ("Inquiry from {0} already exists", data["guest_name"]);
				return false;
			}
			else
			{
			//	Console.WriteLine ("existing inq {0} for message-id = {1}", inq, data["message_id"]);
			}

			var propertyListingId = data ["listing_id"];
			var source = data ["source"].ToUpper();

//			Console.WriteLine ("channel = {0} listing = {1}", source, propertyListingId);
			var listing = Listing.FindByCodeAndNumber (source, propertyListingId);
			if (listing == null) {
//				Console.WriteLine ("This property is not PropertyListingId {0} on channel {1}",
//					propertyListingId, source );
				return false;
			}

			inq = new Inquiry ();

			inq.GuestName = data["guest_name"];

			if (data ["arrival"] == null  || data ["arrival"] == "") {
//				Console.WriteLine ("Arrival Date is empty");
				inq.ArrivalDate = DateTime.MinValue;
			} else {
//				Console.WriteLine ("Arrival Date = {0}", data ["arrival"]);
				inq.ArrivalDate = DateTime.Parse (data ["arrival"]);
			}
			if (data ["departure"] == null || data ["departure"] == "" )
				inq.DepartureDate = DateTime.MinValue;
			else
				inq.DepartureDate = DateTime.Parse (data ["departure"]);

			inq.Email = data["email"];
			int adults;
			int children;
			int.TryParse (data ["adults"], out adults);
			int.TryParse (data ["children"], out children) ;
			Console.WriteLine ("Setting Adults");
			inq.Adults = adults;
			inq.Children = children;
			inq.ReplyTo = data ["reply_to"];
			inq.Phone = data ["phone"];
			inq.ListingNumber = propertyListingId;
			inq.Comment = data ["comment"];
			if(data["request_date"] == null || data["request_date"] == "")
				inq.InquiryDate = DateTime.Now;
			else
				inq.InquiryDate = DateTime.Parse (data ["request_date"]);
			inq.Channel = source;
			inq.MessageID = data ["message_id"];


//			Console.WriteLine ("inq.id = {0}", inq.ID);
//			Console.WriteLine ("json inq = {0}", inq.ToJson ().ToString ());

			inq.PropertyID = listing.PropertyID;
			inq.SourceDocument = vmail.BodyAsHTML;
			inq.Save ();
//			Console.WriteLine ("saved inq.id = {0}", inq.ID);
			return true;
		}

		private class ReceivedEmail
		{
			public string message{ get; set;}
		}

	}




	public class ServerUser
	{
		public string email{ get; set;}
		public string password{ get; set;}
		public string name { get; set;}
		public string server{ get; set;}
		public int    port{ get; set;}
		public string inbox{ get; set;}
		public string archive{ get; set; }
		public bool   use_ssl{get; set;}
		public string api_token { get; set;}
		public bool   receive_email{ get; set;}
		public string inbound_email{ get; set;}

		public ServerUser  FillFromMailServer(MailServer ms)
		{
			email = ms.UserLogin;
			password = ms.UserPassword;
			server = ms.ServerName;
			port = ms.Port;
			inbox = ms.InBox;
			archive = ms.Archive;
			use_ssl = ms.UseSSL;
			return this;
		}

	}
}

