using System;

using SharedLibrary;
using SQLite;
using SharedLibrary.DAL;


namespace SharedLibrary.BL.Contracts
{
	public  interface IDetail
	{
		int			ID{ get; set; }
		string   	Name { get; set; }
		string 		Description { get; set; }
		decimal		Amount { get; set; }
		decimal		Percentage { get; set; }
		decimal 	PerNightAmount{ get; set;}
		bool		Required { get; set; }
		bool		Refundable{ get; set;}
		bool		Taxable{ get; set;}
		int 		PropertyID { get; set;}

		void		Save ();
		void 		Delete();


	}
}
