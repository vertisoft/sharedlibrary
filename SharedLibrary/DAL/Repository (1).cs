using System;
using System.IO;
using SQLite;
using SharedLibrary.DL;

namespace SharedLibrary.DAL
{
	public class Repository
	{

		static SQLiteConnection _db = null;
		public static SQLiteConnection DB 
		{ 
			get {
				if (_db == null) {
//					Console.WriteLine ("DB _db is null");
					_db = GetConnection ();
				}
				return _db;
			}
			set {
				_db = value;
			}
		}

//		static SQLiteAsyncConnection _adb;
//		public static SQLiteAsyncConnection ADB 
//		{ 
//			get {
//				if (_adb == null)
//					_adb = GetAsyncConnection ();
//				return _adb;
//			}
//			set {
//				_adb = value;
//			}
//		}

		static string _dbName = "VrWizard.db3";
		public static string DBName {
			get {return _dbName;}
			set{ _dbName = value;}
		}

		static string _dbpath = null;
		public  static string DBPath { 
			get {
				if (_dbpath == null) {
					_dbpath = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.Personal), DBName);
				}
				return _dbpath;
			}
			set {
				_dbpath = Path.Combine (value, DBName);
			}
		}

		public static SQLiteConnection Open()
		{
			return GetConnection ();
		}

		public static void DropDatabase()
		{

			if (_db != null) {
//				Console.WriteLine ("Database Connection is open, closing it");
				_db.Close ();
				_db = null;
			}
			

//			Console.WriteLine("Checking if the DB file exists {0}", DBPath);
			if (File.Exists (DBPath)){
				File.Delete (DBPath);
//				Console.WriteLine("File Exists = {0}", File.Exists(DBPath));
			}
		}

//		public static SQLiteAsyncConnection AsyncOpen()
//		{
//			return GetAsyncConnection ();
//		}

		public static SQLiteConnection GetConnection ()
		{
//			Console.WriteLine ("Getting a connection using {0}", DBPath);


			if (_db == null) {
//				Console.WriteLine ("_db is null Creating the connection");
				_db = new SQLiteConnection (DBPath);
				CreateDatabase ();
			}

			return _db;
		}

//		public static SQLiteAsyncConnection GetAsyncConnection ()
//		{
//			if (DBPath == null)
//				DBPath = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.Personal), DBName);
//			//			Console.WriteLine ("Open the DataBase  {0} -- {1}", DBPath, DBName);
//			if (_db == null) {
//				ADB = new SQLiteAsyncConnection (DBPath);
//				CreateDatabase ();
//			}
//
//			return ADB;
//		}

		public static void Close()
		{
			if (_db != null)
				_db.Close();
			_db = null;
		}

		public static void Remove ()
		{
			Close ();
			if (File.Exists (DBPath)) {
				File.Delete (DBPath);
//				Console.WriteLine ("Remove file exists for {0} = {1}", DBPath, File.Exists (DBPath));
			}
		}

		static void CreateDatabase ()
		{
			DB.CreateTable<User> ();
			DB.CreateTable<Property> ();
			DB.CreateTable<Fee> ();
			DB.CreateTable<Deposit> ();
			DB.CreateTable<ReservedWord> ();
			DB.CreateTable<Tax> ();
			DB.CreateTable<Discount> ();
			DB.CreateTable<Channel> ();
			DB.CreateTable<Listing> ();
			DB.CreateTable<Rate> ();

//			var prop = Property.Demo ();
//			Console.WriteLine ("CreateDatabase prop.id = {0}", prop.ID);

		}
	}
}

