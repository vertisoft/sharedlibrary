
using System;
using SharedLibrary.DL;
using SharedLibrary.SAL;
using RestSharp;

namespace SharedLibrary.DAL
{
	public class User
	{
		private string _name;
		public string name {
			get {
				return _name;
			}
			set {
				Console.WriteLine ("Name value = {0}", value);
				_name = value;
			}
		}
		public string token { get; set; }
	
		public User(){
			Console.WriteLine ("Initializing DAL.User");
		}

	}
}