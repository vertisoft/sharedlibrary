using System;
using System.IO;
using SQLite;
using SharedLibrary.DL;
using SharedLibrary.LIB;
using System.Security.Cryptography;
using SharedLibrary.SAL.MailProcessing;
using SharedLibrary.SAL.Amazon;
//using Environment = Android.OS.Environment;
using DataNuage.Aws;
using Android.OS;
using Android.Provider;
using Android.App;


namespace SharedLibrary.DAL
{
	public class Repository
	{
		const int DBVersion = 8;
		static ReservedWordManager _rwm;
		static SQLiteConnection _db;
		public static SQLiteConnection DB 
		{ 
			get {
//				Console.WriteLine ("Get DB COnnection");
				if (_db == null) {
					Console.WriteLine ("DB _db is null");
					_db = GetConnection ();
				}
				return _db;
			}
			set {
				_db = value;
			}
		}

//		static SQLiteAsyncConnection _adb;
//		public static SQLiteAsyncConnection ADB 
//		{ 
//			get {
//				if (_adb == null)
//					_adb = GetAsyncConnection ();
//				return _adb;
//			}
//			set {
//				_adb = value;
//			}
//		}

		#if DEBUG
			static string _dbName = "VrWizard.Debug.db3";
		    static bool isProduction = false;
		#else
		    static bool isProduction = true;
			static string _dbName = "VrWizard.db3";
		#endif

		public static string DBName {
			get {return _dbName;}
			set{ _dbName = value;}
		}

		static string _dbpath;
		public  static string DBPath { 
			get {
				if (_dbpath == null) {
					_dbpath = Path.Combine (System.Environment.GetFolderPath (System.Environment.SpecialFolder.MyDocuments), DBName);  // was Personal
				}
				Console.WriteLine ("Production path = {0}", System.Environment.GetFolderPath (System.Environment.SpecialFolder.MyDocuments));
				Console.WriteLine ("Full Path of Database is {0}", _dbpath);
				return _dbpath;
//				var documents = Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments); 
//				var filename = Path.Combine (documents, "db"); 
//				Db = new SQLiteConnection(filename);
			}
			set {
				_dbpath = Path.Combine (value, DBName);
			}
		}

		static string _backupFolder;
		public static string BackupFolder {
			get {
				if(_backupFolder == null){

//					var dir = new File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), "CameraAppDemo");

//					BackupFolder = Environment.  GetExternalStoragePublicDirectory ("VrWizard");
//					BackupFolder = Path.Combine (Android.OS.Environment.ExternalStorageDirectory.Path, "VrWizard")
//					BackupFolder = "/storage/sdcard0/VrWizard";
//					_backupFolder = "/sdcard/VrWizard";
//					if(!Directory.Exists (_backupFolder)){
//						Directory.CreateDirectory (_backupFolder);
//					}
				}
				Console.WriteLine ("Backup Folder = {0}", _backupFolder) ;
				return _backupFolder;
			}
			set{
				_backupFolder = value;
				if(!Directory.Exists (_backupFolder)){
					Directory.CreateDirectory (_backupFolder);
				}
			}
		}
		static string _backupdbpath;
		public  static string BackupDBPath { 
			get {
				if (_backupdbpath == null) {
					_backupdbpath = Path.Combine (BackupFolder, DBName);  // was Personal
				}
//				Console.WriteLine ("Full Backup Path of Database is {0}", _backupdbpath);
				return _backupdbpath;
				//				var documents = Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments); 
				//				var filename = Path.Combine (documents, "db"); 
				//				Db = new SQLiteConnection(filename);
			}
			set {
				_backupdbpath = Path.Combine (value, DBName);
			}
		}

		public static async void SaveDatabase(string key, string filePath)
		{

			var device = Build.Device;
			var id = Settings.Secure.GetString(Application.Context.ContentResolver, Settings.Secure.AndroidId);
			Console.WriteLine ("Start Save String [{0}] - [{1}]", device, id);
			var random = new Random();


			var data = File.ReadAllBytes (filePath);
			var s3 = new S3("AKIAJFLJMODIWM2Y52BA",
				"AciclMYREAejQK8jBxBUnzCYTvZNCClQ0nOjjYCN");

			var bucket = "vrwizard-archive"; // + random.Next();

			try
			{
				//await s3.CreateBucketAsync(bucket);

				await s3.PutObjectAsync(bucket, key, data);

				Console.WriteLine ("---------Database Backed up to {0}", key);


//				await s3.DeleteObjectAsync(bucket, "myobject");
//				Console.WriteLine("------Object myobject deleted");
//
//				await s3.DeleteBucketAsync(bucket);
//				Console.WriteLine ("-----Empty bucket {0} deleted", bucket);


			}
			catch (S3Exception ex)
			{
				Console.WriteLine ("AWS Exception {0}", ex.Message);

			}
		}

		public static async void RestoreDatabase(string key, string filePath)
		{


			var s3 = new S3("AKIAJFLJMODIWM2Y52BA",
				"AciclMYREAejQK8jBxBUnzCYTvZNCClQ0nOjjYCN");

			var bucket = "vrwizard-archive";

			try
			{
				Byte[] data = await s3.GetObjectAsByteArrayAsync(bucket, key);

				File.Delete (filePath);
				File.WriteAllBytes(filePath,data);
				Console.WriteLine ("Reopening DB");
				Repository.Open ();
				Console.WriteLine ("---------DataBase {0} Retrieved", key);
			}
			catch (S3Exception ex)
			{
				Console.WriteLine ("AWS Exception {0}", ex.Message);

			}
		}

		public static bool Backup(string key)
		{
			var backupPath = DBPath + ".bak";
			Repository.Close ();
			File.Copy (DBPath, backupPath, true);
			Repository.Open ();
			Console.WriteLine ("Backing up {0} to {1}", backupPath, key);
			SaveDatabase (key, backupPath);
			Console.WriteLine ("After Backup");
			return true;


//			if (!isProduction)
//				return false;
//			var production = DBPath;
//			var backup = backupFile;
//
//
//
//			if (!File.Exists (production )) {
//				Console.WriteLine ("production [{0}] does not exist yet, skip", production);
//				return false;
//			}
//			Console.WriteLine ("   Backup from {0} to {1}", production, backup);
//			try {
//				Repository.Close ();
//				File.Copy (production, backup, true);
//			}
//			catch(Exception ex) {
//				Console.WriteLine ("Exception = {0}", ex);
//				Repository.Open ();
//				return false;
//			}
//			Repository.Open ();
//			return true;
		}

		public static bool Restore(string key)
		{
			// Need to check for WIFI Only

			var backupPath = DBPath + ".bak";
			if (File.Exists (backupPath))
				File.Delete (backupPath);
			Repository.Close ();
			Console.WriteLine ("Calling Restore");
			RestoreDatabase (key, DBPath);

			return true;

//			if (!isProduction) {
//				Console.WriteLine ("Is not production");
//				return false;
//			}
////			var backup = BackupDBPath;
//			var backup = backupFile;
//			var production = DBPath;
//			Console.WriteLine ("Restore [{0}] to [{1}", backup, production);
//			Console.WriteLine ("Backup exists = {0}", File.Exists (backup));
//			Console.WriteLine ("Production exists = {0}", File.Exists (production));
//			if (!File.Exists (backup )) {
//				Console.WriteLine ("Backup [{0}] does not exist Create one", backup);
//				Backup (backupFile);
//				return false;
//			}
//
//			if (!File.Exists (production)) {
//				Console.WriteLine ("Production created for some reason");
//				Repository.Open ();  // Create it
//			}
//			Console.WriteLine ("   Restoring from {0} to {1}", backup, production);
//			try {
//				Repository.Close();
//				Console.WriteLine ("Copyng now");
//				File.Copy (backup, production, true);
//			}
//			catch(Exception ex) {
//				Console.WriteLine ("Exception = {0}", ex);
//				return false;
//			}
//			try {
//				Repository.Open ();
//			}
//			catch (Exception ex){
//				Console.WriteLine ("OPEN DB EXCEPTION {0}", ex);
//				return false;
//			}
//			Console.WriteLine ("Restore Successful");
//			return true;
		}

		public static void Sync()
		{
//			var backup = BackupDBPath;
//			var production = DBPath;
//
//			Console.WriteLine ("             -----Syncing-----");
//			if (!File.Exists (backup)){
//				Console.WriteLine ("Backup [{0}] does not exist Create one", backup);
//				Backup ();
//				return;
//			}
//
//			var backupLastWriteTime = File.GetLastWriteTimeUtc (backup);
//			var produtionLastWriteTime = File.GetLastWriteTimeUtc (production);
//
//			if (produtionLastWriteTime > backupLastWriteTime ) {
//				Console.WriteLine ("Backup is older than the current, no restore. Backup!");
//				Backup ();
//				return;
//			}
//			if (backupLastWriteTime > produtionLastWriteTime){
//				Console.WriteLine ("Backup is newer than current, Restore!");
//				Restore ();
//				return;
//			}

		}
//		private string ExternalPath()
//		{
//			var sdCardpath = Android.OS.Environment.ExternalStorageDirectory.path;
//			var filePath = System.IO.Path.Combine (sdCardpath, DBName);
//			return filePath;
//		}

		public static SQLiteConnection Open()
		{
			return GetConnection ();
		}

		public static void DropDatabase()
		{

			if (_db != null) {
				_db.Close ();
				_db = null;
			}
			Remove ();

		}

//		public static SQLiteAsyncConnection AsyncOpen()
//		{
//			return GetAsyncConnection ();
//		}

		public static SQLiteConnection GetConnection ()
		{
			if (_db == null) {
				Console.WriteLine ("geting connection for {0}", DBPath);
				_db = new SQLiteConnection (DBPath);
			}

			#if DEBUG 
//				Console.WriteLine (	"Debug Mode Database {0}", DBPath);
//				UpdateDatabaseVersion ();
//				return _db;
			#else
//				Console.WriteLine ("Release Mode Database {0}", DBPath);
			#endif

			UpdateDatabaseVersion ();
			return _db;
		}

//		public static SQLiteAsyncConnection GetAsyncConnection ()
//		{
//			if (DBPath == null)
//				DBPath = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.Personal), DBName);
//			//			Console.WriteLine ("Open the DataBase  {0} -- {1}", DBPath, DBName);
//			if (_db == null) {
//				ADB = new SQLiteAsyncConnection (DBPath);
//				CreateDatabase ();
//			}
//
//			return ADB;
//		}

		private static void UpdateDatabaseVersion()
		{
			try {
				int count = DatabaseVersion.Count;
//				if (count > 0) {
//					Console.WriteLine ("Version exists assume DB exists");
//				}
			}
			catch (SQLiteException ex)
			{
//				Console.WriteLine("No DatabaseVersion found assume not created") ;
				CreateDatabase ();
				return;
//				CreateVersionManager (1);
			}

			var vers = DatabaseVersion.Find (1);

			Console.WriteLine ("------Current Database Version = {0}  New version = {1}", vers.Version, DBVersion);
			do 			
 			{
				//			if (vers.Version < DBVersion)
				Console.WriteLine ("----upgrade db");
				vers.Version = UpgradeDB (vers.Version, DBVersion);
			} while (vers.Version < DBVersion);

			if(vers.Version > DBVersion)
			{
				DowngradeDB (vers.Version, DBVersion);
			}
			//vers.Version = DBVersion;
		
			vers.Save ();

		}

		private static int UpgradeDB(int oldVersion, int newVersion)
		{
			Console.WriteLine ("upgrading DB from {0} to {1}", oldVersion, newVersion);
			if (oldVersion == 1) {
				UpGradeDB_1_2 ();
				oldVersion = 2;
			}
			if (oldVersion == 2 && newVersion == 3) {
				UpGradeDB_2_3 ();
				oldVersion = 3;
			}
			if (oldVersion == 3 && newVersion == 4) {
				UpGradeDB_3_4 ();
				oldVersion = 4;
			}
			if (oldVersion == 4 && newVersion == 5) {
				UpGradeDB_4_5 ();
				oldVersion = 5;
			}
			if (oldVersion == 5 && newVersion == 6) {
				UpGradeDB_5_6 ();
				oldVersion = 6;
			}
			if (oldVersion == 6 && newVersion == 7) {
				UpGradeDB_6_7 ();
				oldVersion = 7;
			}
			if (oldVersion == 7 && newVersion == 8) {
				UpGradeDB_7_8 ();
				oldVersion = 8;
			}
			Console.WriteLine ("Old Version = {0}", oldVersion);
			return oldVersion;
		}


		private static void UpGradeDB_1_2(){
//			Console.WriteLine ("Updating from version 1 to version 2");
			var command = Repository.DB.CreateCommand (
			    "Alter table Inquiry add column State INTEGER");
			command.ExecuteNonQuery ();
			command = Repository.DB.CreateCommand (
				"Alter table Reservation add column State INTEGER");
			command.ExecuteNonQuery ();
		}


		private static void UpGradeDB_2_3()
		{
			//			Console.WriteLine ("Upgrading from version 2 to version 3");
			var command = Repository.DB.CreateCommand (
				"Alter table Template add column Type INTEGER");
			command.ExecuteNonQuery ();
			command = Repository.DB.CreateCommand (
				"Alter table Template add column Format INTEGER");
			command.ExecuteNonQuery ();
			DB.CreateTable<Response> ();
		}

		private static void UpGradeDB_3_4()
		{
			//			Console.WriteLine ("Upgrading from version 2 to version 3");
			var command = Repository.DB.CreateCommand (
				"CREATE INDEX rev_arrival_date ON reservation (ArrivalDate);");
			command.ExecuteNonQuery ();
			command = Repository.DB.CreateCommand (
				"CREATE INDEX rev_departure_date ON reservation (DepartureDate);");
			command.ExecuteNonQuery ();
			DB.CreateTable<Response> ();
		}

		private static void UpGradeDB_4_5()
		{
			Console.WriteLine (	"UpGrading DB from 4 to 5");
			var command = Repository.DB.CreateCommand (
				"Alter table Inquiry add column CurrentReservationState INTEGER");
			command.ExecuteNonQuery ();
			DB.CreateTable<Response> ();
		}

		private static void UpGradeDB_5_6()
		{
			Console.WriteLine (	"UpGrading DB from 5 to 6");
			var command = Repository.DB.CreateCommand (
				"Alter table MailServer add column ApiKey STRING");
			command.ExecuteNonQuery ();
//			DB.CreateTable<Response> ();
		}

		private static void UpGradeDB_6_7()
		{
			Console.WriteLine (	"UpGrading DB from 6 to 7");
			SQLiteCommand command;
			command = Repository.DB.CreateCommand (
				              "Alter table Property add column RemoteID STRING");
			command.ExecuteNonQuery ();

			command = Repository.DB.CreateCommand (
				"Alter table Channel add column RemoteID STRING");
			command.ExecuteNonQuery ();

			command = Repository.DB.CreateCommand (
				"Alter table Deposit add column RemoteID STRING");
			command.ExecuteNonQuery ();

			command = Repository.DB.CreateCommand (
				"Alter table Discount add column RemoteID STRING");
			command.ExecuteNonQuery ();

			command = Repository.DB.CreateCommand (
				"Alter table Fee add column RemoteID STRING");
			command.ExecuteNonQuery ();

			command = Repository.DB.CreateCommand (
				"Alter table Inquiry add column RemoteID STRING");
			command.ExecuteNonQuery ();

			command = Repository.DB.CreateCommand (
				"Alter table Listing add column RemoteID STRING");
			command.ExecuteNonQuery ();

			command = Repository.DB.CreateCommand (
				"Alter table MailServer add column RemoteID STRING");
			command.ExecuteNonQuery ();

			command = Repository.DB.CreateCommand (
				"Alter table Quote add column RemoteID STRING");
			command.ExecuteNonQuery ();

			command = Repository.DB.CreateCommand (
				"Alter table QuoteDetail add column RemoteID STRING");
			command.ExecuteNonQuery ();

			command = Repository.DB.CreateCommand (
				"Alter table Rate add column RemoteID STRING");
			command.ExecuteNonQuery ();

			command = Repository.DB.CreateCommand (
				"Alter table Reservation add column RemoteID STRING");
			command.ExecuteNonQuery ();

			command = Repository.DB.CreateCommand (
				"Alter table ReservedWord add column RemoteID STRING");
			command.ExecuteNonQuery ();

			command = Repository.DB.CreateCommand (
				"Alter table Response add column RemoteID STRING");
			command.ExecuteNonQuery ();


			command = Repository.DB.CreateCommand (
				"Alter table Tax add column RemoteID STRING");
			command.ExecuteNonQuery ();

			command = Repository.DB.CreateCommand (
				"Alter table Template add column RemoteID STRING");
			command.ExecuteNonQuery ();

			command = Repository.DB.CreateCommand (
				"Alter table User add column RemoteID STRING");
			command.ExecuteNonQuery ();

			command = Repository.DB.CreateCommand (
				"Alter table Rate add column DayMap STRING");
			command.ExecuteNonQuery ();



		}

		private static void UpGradeDB_7_8()
		{
			Console.WriteLine (	"UpGrading DB from 7 to 8");
			SQLiteCommand command;
			command = Repository.DB.CreateCommand (
				"Alter table User add column Status STRING");
			command.ExecuteNonQuery ();
		}


		private static void DowngradeDB(int oldVersion, int newVersion)
		{


		}

		private static void CreateVersionManager(int version)
		{
//			Console.WriteLine ("Create Version Manager at version {0}", version);
			DB.CreateTable<DatabaseVersion> ();
			var vers = new DatabaseVersion ();
			vers.Version = version;
			vers.Save ();
		}

		public static void Close()
		{
			if (_db != null) {
				_db.Close ();
			}
			_db = null;
		}

		public static void Remove ()
		{
			Close ();
			if (File.Exists (DBPath)) {
				File.Delete (DBPath);
//				Console.WriteLine ("Remove file exists for {0} = {1}", DBPath, File.Exists (DBPath));
			}

		}

		static void CreateDatabase ()
		{
			DB.CreateTable<User> ();
			DB.CreateTable<Property> ();
			DB.CreateTable<Fee> ();
			DB.CreateTable<Deposit> ();
			DB.CreateTable<ReservedWord> ();
			DB.CreateTable<Tax> ();
			DB.CreateTable<Discount> ();
			DB.CreateTable<Listing> ();
			DB.CreateTable<Rate> ();
			DB.CreateTable<Inquiry> ();
			DB.CreateTable<Quote> ();
			DB.CreateTable<Reservation> ();
			DB.CreateTable<QuoteDetail> ();
			DB.CreateTable<Template> ();
			DB.CreateTable<MailServer> ();
			DB.CreateTable<Channel> ();
			DB.CreateTable<Response> ();

			var prop = Property.DemoProperty ();
			if (Channel.Count == 0) {
				CreateChannels ();
			}
			CreateVersionManager (DBVersion);
//			Console.WriteLine ("CreateDatabase prop.id = {0}", prop.ID);
		}

		private static void CreateChannels()
		{
			var chan = new Channel ();
			chan.Name = "VRBO";
			chan.ChannelCode = "VRBO";
			chan.Save ();;
			chan = new Channel ();
			chan.Name = "HomeAway";
			chan.ChannelCode = "HOMEAWAY";
			chan.Save ();
			chan = new Channel ();
			chan.Name = "FlipKey";
			chan.ChannelCode = "FLIP";
			chan.Save ();
			chan = new Channel ();
			chan.Name = "VacatonRentals";
			chan.ChannelCode = "VRental";
			chan.Save ();
			chan = new Channel ();
			chan.Name = "Villa 4 Vacation";
			chan.ChannelCode = "Villa";
			chan.Save ();
			chan.Name = "Manual";
			chan.ChannelCode = "MANUAL";
			chan.Save ();
		}
	}

}

