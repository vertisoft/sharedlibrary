using System;
using SharedLibrary.DL;
using System.Text.RegularExpressions;
//using Limilabs.Mail;


namespace SharedLibrary.SAL.MailProcessing
{
	public class FlipKey : IInquirySource
	{

		private Inquiry inq;
		private string _text;
		private string listingNumber;
		private VrMail _email;

		public Inquiry Inquiry { 
			get {return inq;}
			set {inq = value;}
		}

//		public string ListingNumber {
//			get {
//				if(_listingNumber == null) {
//					_listingNumber = ListingNum ();
//				}
//				return _listingNumber;
//			}
//			set {_listingNumber = value;}
//		}
//
		public FlipKey (VrMail email)
		{
			_text = email.Text;
			_email = email;
		}

		public static IInquirySource From(VrMail email)
		{
			var text = email.Text;
			string sourceCompany = null;
			Match match;


			match = Regex.Match (text, @"Website: TripAdvisor.com");
			if (match.Success) {
				sourceCompany = "TripAdvisor";
			}
			match = Regex.Match (text, @"Website: FlipKey.com");
			if (match.Success) {
				sourceCompany = "FlipKey";
			}
			if (sourceCompany == null) {
				// Not a flipkey company inquiry
				return null;
			}

			var listingNumber  = ListingNumber (email.Text); 
			var listing = Listing.FindByCodeAndNumber ("FLIP", listingNumber);
			if (listing == null)
				throw new VsListingException ("Property number " + listingNumber + " from FlipKey does not exist");
			Inquiry _inq = Inquiry.FindByMessageID (email.MessageID);
			if (_inq == null){

				_inq = new Inquiry ();
				_inq.PropertyID = listing.PropertyID;
			}
//			else {
//				if (_inq.CurrentState == Inquiry.InquiryState.New) {
//					if (_inq.CurrentQuote != null) {
//						Console.WriteLine ("Updating and removing current quote");
//						_inq.CurrentQuote.Delete ();
//						_inq.CurrentQuote = null;
//						_inq.QuoteID = 0;
//					}
//				}
//				else
//					Console.WriteLine ("Updating existing Inquiry with no existing quote.");
//			}
			IInquirySource source = new FlipKey (email);
			_inq.Channel = "FLIP";
			_inq.ListingNumber = listingNumber;
			_inq.MessageID = email.MessageID;
			source.Inquiry = _inq;


			return source;
		}

		private static string ListingNumber(string text)
		{
			Match md = Regex.Match (text, @"ID #(.*)");
			if (md.Success) 
				return md.Groups [1].Value.TrimEnd (' ', '\r', '\n');
			else
				return null;
		}

		public Inquiry Extract()
		{

			inq.Channel = "FLIP";
			inq.MessageID = _email.MessageID;

			inq.GuestName = GuestName ();
			var dates = RequestedDates ();
			inq.ArrivalDate = dates [0];
			inq.DepartureDate = dates [1];
			inq.Adults = Guests ();
			inq.Children = 0;      // FlipKey only gives total guests
			inq.Email = EMail ();
			inq.ReplyTo = _email.ReplyTo;
			inq.Phone = Phone ();
			inq.Comment = Comments ();
			inq.CurrentState = Inquiry.InquiryState.New;
			inq.InquiryDate = _email.ReceivedDate; //(_email.Date == null ? new DateTime() : (DateTime)_email.Date);
			inq.SourceDocument = _email.BodyAsHTML;
			#if DEBUG
			Console.WriteLine ("\tListing = {0}",inq.ListingNumber);
			Console.WriteLine ("\tProperty Number = {0}", inq.PropertyID);
			Console.WriteLine ("\tGuest = {0}", inq.GuestName);
			Console.WriteLine ("\tEmail = {0}", inq.Email);
			Console.WriteLine ("\tReply-To = {0}", inq.ReplyTo);
			Console.WriteLine ("\tPhone = {0}", inq.Phone);
			Console.WriteLine ("\tAdults = {0}", inq.Adults);
			Console.WriteLine ("\tChildren = {0}", inq.Children);
			Console.WriteLine ("\tDates = {0} - {1}", inq.ArrivalDate.ToShortDateString(), inq.DepartureDate.ToShortDateString ());
			Console.WriteLine ("\tComment: {0}", inq.Comment);
			#endif
			inq.Save ();
			return inq;
		}


		private string GuestName()
		{
			Match md = Regex.Match (_text, @"Guest Name:?\s*(.*)");
			if (md.Success)
				return md.Groups [1].Value.TrimEnd (' ', '\r', '\n');
			else
				return null;
		}

		private DateTime[] RequestedDates()
		{
//Travel Dates: October 10, 2013 - October 18, 2013 (8 nights)
//Travel Dates October 10, 2013–October 17, 2013 (7 nights)
			Match md = Regex.Match (_text, @"Travel Dates:?\s*(.*)\s*[\u2013\-]\s*(.*)\s*\("); // the dash is not a regular dash
			if (md.Success)
			{
				return new [] { DateTime.Parse (md.Groups [1].Value), DateTime.Parse (md.Groups [2].Value) };
			} else {
				return new[] {new DateTime (), new DateTime ()};
			}
		}

		private int Guests()
		{
			Match md = Regex.Match (_text, @"Travelers:?\s*(.*)");
			if (md.Success){
				return Convert.ToInt32 (md.Groups[1].Value.TrimEnd('\r', '\n', ' '));
			}
			return 0;
		}

		private string EMail()
		{
			Match md = Regex.Match (_text, @"Guest Email:?\s*(.*)");
			if (md.Success)
			{
				return md.Groups [1].Value.TrimEnd (' ', '\r', '\n');
			}
			return "";
		}
			

		private string Phone()
		{
			Match md = Regex.Match (_text, @"Guest Phone:?\s*(.*)");
			if (md.Success)
			{
				return md.Groups [1].Value.TrimEnd (' ', '\r', '\n');
			}
			return "";
		}

		string Comments()
		{
			//Comments:	I will take it as long as the sexy owner is there with me.

			Match match = Regex.Match (_text, @"Message\s*(.*?)Next Steps", RegexOptions.Singleline);
			if (match.Success)
				return Regex.Replace (match.Groups [1].Value.TrimEnd ('\r', '\n'), @"> ", "");
			return "";
		}

	}
}

