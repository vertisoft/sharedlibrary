using System;
using SharedLibrary.DL;
using System.Text.RegularExpressions;


namespace SharedLibrary.SAL.MailProcessing
{
	public class VRentals : IInquirySource
	{

		private Inquiry _inq;
		private string _text;
		string _htmlText;
		private VrMail _email;

		public Inquiry Inquiry { 
			get {return _inq;}
			set {_inq = value;}
		}

		public VRentals (VrMail email)
		{

			_htmlText = email.BodyTextFromHTML; //.GetTextFromHtml ();
			_text = email.BodyAsText; //.GetBodyAsText ();
			_email = email;

		}

		public static IInquirySource From(VrMail email)
		{
//			var text1 = email.GetTextFromHtml ();
			var text = email.BodyAsText; //.GetBodyAsText ();
//			Console.WriteLine (" In From:  \n{0}", text);
			Match match = Regex.Match (text, @"www.vacationrentals.com");

			if (match.Success) {
				Console.WriteLine ("Found VRental inquiry");
				IInquirySource source = new VRentals (email);
				source.Inquiry = new Inquiry();
				return source;
			}
			return null;
		}

		public Inquiry Extract()
		{

			Inquiry inq = Inquiry.FindByMessageID (_email.MessageID);
			if (inq != null){
				_inq = inq;
//				Console.WriteLine ("Ignoring existing duplicate inquiry from {0} - {1} ", _inq.Channel, _email.MessageID);
//				return null; // null inquiry indicates found however ignore
			}
			_inq.MessageID = _email.MessageID;
			_inq.Channel = "VRental";
			_inq.ListingNumber = ListingNumber ();

			var listing = Listing.FindByCodeAndNumber (_inq.Channel, _inq.ListingNumber);
			if (listing == null) {
				Console.WriteLine ("Listing {0} for {1}", _inq.Channel, _inq.ListingNumber);
				return null;
			}
			_inq.PropertyID = listing.PropertyID;
			_inq.GuestName = GuestName ();
			_inq.ArrivalDate = ArrivalDate();
			_inq.DepartureDate = DepartureDate ();
			var guests = Guests ();
			_inq.Adults = guests [0];
			_inq.Children = guests [1];
			_inq.Email = EMail ();
			_inq.Phone = Phone ();
			_inq.Comment = Comments ();
			_inq.CurrentState = Inquiry.InquiryState.New;
			_inq.InquiryDate = _email.ReceivedDate; //.Date == null ? new DateTime() : (DateTime)_email.Date;
			_inq.SourceDocument = _email.BodyAsHTML; //.GetBodyAsHtml();
			#if DEBUG
			Console.WriteLine ("\tListing = {0}",_inq.ListingNumber);
			Console.WriteLine ("\tProperty Number = {0}", _inq.PropertyID);
			Console.WriteLine ("\tGuest = {0}", _inq.GuestName);
			Console.WriteLine ("\tEmail = {0}", _inq.Email);
			Console.WriteLine ("\tPhone = {0}", _inq.Phone);
			Console.WriteLine ("\tAdults = {0}", _inq.Adults);
			Console.WriteLine ("\tChildren = {0}", _inq.Children);
			Console.WriteLine ("\tDates = {0} - {1}", _inq.ArrivalDate.ToShortDateString(), _inq.DepartureDate.ToShortDateString ());
			Console.WriteLine ("\tComment: {0}", _inq.Comment);
			#endif
			_inq.Save ();
			return _inq;
		}

		private string ListingNumber()
		{
			Match md = Regex.Match (_text, @"inquiry property #(.*)");
			if (md.Success) 
				return md.Groups [1].Value.TrimEnd (' ', '\r', '\n');
			else
				return null;
		}

		private string GuestName()
		{
			Match md = Regex.Match (_text, @"name:\s*(.*)\s*\r\n");
			if (md.Success) {
//				var value = md.Groups [1].Value;
//
//				Console.WriteLine ("Guest Name = [{0}]",ConvertStringToHex(value.Replace ('\xa0', ' ')));
//				Console.WriteLine ("Guest Name = [{0}]",ConvertStringToHex("Theresa French"));

				return md.Groups [1].Value.Replace ('\xa0', ' ');
			}
			else
				return null;
		}

		private DateTime ArrivalDate()
		{
			Match md = Regex.Match (_text, @"arrival:\s*(.*)"); // the dash is not a regular dash
			if (md.Success)
			{
//				Console.WriteLine ("Arrival = [{0}]", md.Groups[1].Value);
				return DateTime.Parse (md.Groups [1].Value.TrimEnd('\r', '\n', ' '));
			} else {
//				Console.WriteLine ("Arrival Date Not Found");
				return new DateTime ();
			}
		}

		
		private DateTime DepartureDate()
		{
			Match md = Regex.Match (_text, @"departure:\s*(.*)"); // the dash is not a regular dash
			if (md.Success)
			{
//				Console.WriteLine ("Departure = [{0}]", md.Groups[1].Value);
				return DateTime.Parse (md.Groups [1].Value.TrimEnd('\r', '\n', ' '));
			} else {
//				Console.WriteLine ("Departure Date Not Found");
				return new DateTime ();
			}
		}

		private int[] Guests()
		{
			Match md = Regex.Match (_text, @"number of guests:\s*(\d+) [Aa]dults?, (\d+) [Cc]hild");
			if (md.Success){
				return new [] {Convert.ToInt32 (md.Groups[1].Value.TrimEnd('\r', '\n', ' ')), Convert.ToInt32 (md.Groups[2].Value.TrimEnd('\r', '\n', ' '))};
			}
				return new [] {0, 0};
		}

		private string EMail()
		{
			Match md = Regex.Match (_text, @"email address:\s*(.*)");
			if (md.Success)
			{
				return md.Groups [1].Value.TrimEnd (' ', '\r', '\n');
			}
			return "";
		}

		private string Phone()
		{
			Match md = Regex.Match (_text, @"phone number:\s*(.*)");
			if (md.Success)
			{
				return md.Groups [1].Value.TrimEnd (' ', '\r', '\n');
			}
			return "";
		}

		string Comments()
		{
			//Comments:	I will take it as long as the sexy owner is there with me.

			Match match = Regex.Match (_text, @"comments:\s*\n(.*?)---------", RegexOptions.Singleline);
			if (match.Success)
				return Regex.Replace (match.Groups [1].Value.TrimEnd ('\r', '\n'), @"> ", "");
			return "";
		}


		public static string ConvertStringToHex(string asciiString)
		{
			string hex = "";
			foreach (char c in asciiString)
			{
				int tmp = c;
				hex += String.Format("{0:x2}", (uint)System.Convert.ToUInt32(tmp.ToString()));
			}
			return hex;
		}

	}
}



