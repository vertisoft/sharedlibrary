using System;
using SharedLibrary.DL;
using System.Text.RegularExpressions;
using System.IO;




namespace SharedLibrary.SAL.MailProcessing
{
	public class VrWizard : IInquirySource
	{

		private string _text;
		private VrMail _email;
		private string _subject;
		private string _templateInfo;
		private Inquiry inq;
		private Property property;


		private string pattern;

		public Inquiry Inquiry { 
			get {return inq;}
			set {inq = value;}
		}

		public VrWizard (VrMail email, string info)
		{
			_text = email.Text;
//			Console.WriteLine ("Text = {0}", _text);
			_email = email;
			_subject = email.Subject;
			_templateInfo = info;
		}


		public static IInquirySource Subject(VrMail email)
		{

			var subject = email.Subject;
//			Console.WriteLine ("Subject = {0}", subject);
			var match = Regex.Match (subject, @"VrWizard:Template:\s*(.*)");
			if (match.Success) {
				var templateInfo = (match.Groups [1].Value);
//				Console.WriteLine ("Found Template Name = {0} - {1}", templateInfo, match.Groups[1].Value);

				IInquirySource source = new VrWizard(email, templateInfo);
				return source;
			}
			return null;
		}


		public Inquiry Extract ()
		{
//			Console.WriteLine ("Extracting Template from VrWizard ");
			property = Property.CurrentProperty;

			var match = Regex.Match (_templateInfo, @"(.*):\s(.*)");
			if (match != null) {
				Template template = property.TemplateByName (match.Groups [1].Value);
				if (template == null) {
					template = new Template (property.ID);
					template.Name = match.Groups [1].Value;
//					Console.WriteLine ("Creating new template {0}", template.Name);
				}
				else {
//					Console.WriteLine ("Deleted Status = {0} - {1}", template.Deleted, template.DeleteDate.ToShortDateString());
					template.Deleted = false;
					template.DeleteDate = DateTime.MinValue;
//					Console.WriteLine (	"Updateing existing template {0}", template.Name);
				}
				template.Subject = match.Groups [2].Value;
				template.TemplateType = "TXT";
				template.Content = _text;
				template.Save ();
//				Console.WriteLine ("Template ID = {0} propertyid = {1}", template.ID, template.PropertyID);
				inq = new Inquiry ();   // just to indicate succcess. 
				return inq;
			}
			return null;
		}
	}
}
