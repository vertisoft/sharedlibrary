using System;
using SharedLibrary.DL;
using System.Text.RegularExpressions;
using System.IO;
//using Limilabs.Mail;
using System.Collections.Generic;



namespace SharedLibrary.SAL.MailProcessing
{
	public class VRBO : IInquirySource
	{
		private Inquiry inq;
		private string _text;
		private VrMail _email;
		private string _travelerSource;
		private string _listingSource;
		private string _listing;
		private string pattern;

		public Inquiry Inquiry { 
			get {return inq;}
			set {inq = value;}
		}

		public VRBO (VrMail email)
		{
			_text = email.Text;
			_email = email;
		}

		public static IInquirySource From(VrMail email)
		{
			string _travelerSource = "";
			string _listingSource = "";
			string _listing = "";
			Console.WriteLine ("Looking at Vrbo/Homeaway");
//			Rebex.Mime.Headers.HeaderValueCollection;
//			var headers = email.Headers;

			_listingSource = email.ListingSource; //.Document.Root.Headers["x-inquiry-listingsource"];
			if (String.IsNullOrWhiteSpace(_listingSource)){
//				Console.WriteLine ("Not Vrbo/Homeaway null/white");
				return null;
			}
			if (_listingSource == "") {
				#if DEBUG
				Console.WriteLine ("Not VRBO/Homeaway empty");
				#endif
				return null;  // Not from Homeaway
			}
			_travelerSource = email.TravelerSource; //.email.Document.Root.Headers["X-Inquiry-TravelerSource"];
			_listing = email.Listing; //email.Document.Root.Headers["x-inquiry-listing"];
			;
			if(String.IsNullOrEmpty (_listing)){
				Match match = Regex.Match (email.Subject, @"Listing #:?\s*(.*)");
				if (match.Success) {
					_listing = match.Groups [1].Value.TrimEnd (' ', '\r', '\n');
				} else {
					_listing = "unknown";
				}
				Console.WriteLine ("Subject = [{0}]", email.Subject);
			}


			string text = email.Text;
			Console.WriteLine (" Html_to_text = {0}", email.BodyTextFromHTML);
//			#if DEBUG
			Console.WriteLine (" \nIn From:  {0}\n", email.Text);
			Console.WriteLine ("Source = {0}", _travelerSource);
			Console.WriteLine ("listing Source = {0}", _listingSource);
			Console.WriteLine ("Listing = {0}", _listing);
			_listing = Regex.Replace (_listing, @"trip-", "");
			_listing = Regex.Replace (_listing, @"vrbo-", "");
			Console.WriteLine ("Final listing = {0}", _listing);
//			#endif

			Inquiry inq;
			IInquirySource source;

			if (_listingSource == "") {
//				Console.WriteLine ("Not VRBO/Homeaway");
				return null;  // Not from Homeaway
			}
			var listings = Listing.ListingsByProperty (Property.CurrentProperty.ID);
//			Console.WriteLine ("Current available listings");
//			foreach(Listing l in listings)
//			{
//				Console.WriteLine ("   {0}  -  {1}", l.ChannelCode, l.ListingNumber);
//			}
			var _listingNumber = ListingNumber (_listing);
//			#if DEBUG
			Console.WriteLine ("looking for vrbo/homeaway = [{0}]/[{1}]", _listingSource.ToUpper () ,_listingNumber );
//			#endif
			var listing = Listing.FindByCodeAndNumber (_listingSource.ToUpper () ,_listingNumber);
			if (listing == null) {
//				#if DEBUG
				var lst = Listing.All();
				foreach(Listing l in lst){
					Console.WriteLine ("Listing {0}/{1}", l.ChannelName, l.ListingNumber);
				}

				Console.WriteLine("Property number [" + _listingNumber + "] from " + _listingSource.ToUpper () + " does not exist"); ;
//				#endif
				//				throw new VsListingException ("Property number [" + _listingNumber + "] from " + _listingSource.ToUpper () + " does not exist");
				return null;
			}

			inq = Inquiry.FindByMessageID (email.MessageID.ToString ());
			if (inq != null) {
				source = new VRBO(email);
				Console.WriteLine ("Skip as duplicate");
				return source;  // Skip as duplicate
			}
			inq = new Inquiry ();
			inq.PropertyID = listing.PropertyID;

			source = new VRBO(email);
			source.Inquiry = inq;
			inq.Channel = _listingSource.ToUpper ();
			inq = source.Inquiry;
			inq.Channel = _listingSource.ToUpper ();
			inq.ListingNumber = _listingNumber;
			inq.MessageID = email.MessageID.ToString ();
			inq.ReplyTo = email.ReplyTo; //.Document.Root.Headers["Reply-To"];
			inq.SourceDocument = email.BodyAsHTML; //.GetBodyAsHtml();
			#if DEBUG
			Console.WriteLine ("It is a VRBO Inquiry {0} - {1}", inq.ListingNumber, inq.ReplyTo.Trim ());
			#endif
			return source;
		}

		private static string ListingNumber(string listing)
		{
			Match match = Regex.Match (listing, @"\D*(\d+)");
			if (match.Success) {
				return match.Groups [1].Value.TrimEnd (' ', '\r', '\n');
			} else {
				return null;
			}
		}
//
//

//
		public Inquiry Extract ()
		{


			inq.GuestName = GuestName ();
	
			var dates = RequestedDates ();
			inq.ArrivalDate = dates [0];
			inq.DepartureDate = dates [1];

			var guests =  Guests();
			inq.Adults = guests [0];
			inq.Children = guests [1];
			inq.Phone = Phone ();
			inq.Email = EMail ();
			inq.Comment = Comments ();

			inq.CurrentState = Inquiry.InquiryState.New;
			inq.MessageID = _email.MessageID;

			inq.InquiryDate = _email.ReceivedDate; //(_email.Date == null) ? new DateTime () : (DateTime)_email.Date;

//			#if DEBUG
			Console.WriteLine ("\tListing = {0}",inq.ListingNumber);
			Console.WriteLine ("\tProperty Number = {0}", inq.PropertyID);
			Console.WriteLine ("\tGuest = {0}", inq.GuestName);
			Console.WriteLine ("\tEmail = {0}", inq.Email);
			Console.WriteLine ("\tPhone = {0}", inq.Phone);
			Console.WriteLine ("\tAdults = {0}", inq.Adults);
			Console.WriteLine ("\tChildren = {0}", inq.Children);
			Console.WriteLine ("\tDates = {0} - {1}", inq.ArrivalDate.ToShortDateString(), inq.DepartureDate.ToShortDateString ());
			Console.WriteLine ("\tComment: {0}", inq.Comment);
			Console.WriteLine ("\tMessagaeID: {0}", inq.MessageID);
//			#endif
////
			inq.Save ();
			return inq;
//		
		}
//
////		//Listing #:	209114
//		string ListingNumber()
//		{
//			Console.WriteLine ("ListingNumber _listing = {0} - {1}", _listing, inq.ListingNumber);
//			Match match = Regex.Match (inq.ListingNumber, @"(\d+)");
////			Match match = Regex.Match (_text, @"Rental Inquiry- Property(.*)", RegexOptions.None);
//			if (match.Success) {
//				Console.WriteLine ("Found Listing = [{0}]", match.Groups [1].Value);
//				return match.Groups [1].Value.TrimEnd (' ', '\r', '\n');
//			}
//			else
//				return null;
//		}

	
		string GuestName()
		{
		//Name:		Theresa French
			Match match = Regex.Match (_text, @"Name:?\s*(.*)");
			if (match.Success) 
				return match.Groups [1].Value.TrimEnd (' ', '\r', '\n');
			else
				return "";
		}


//		Arrival date: Oct 10, 2013
//		Departure date: Oct 17, 2013
		DateTime[] RequestedDates()
		{
			DateTime arrival = DateTime.MinValue;
			DateTime departure = DateTime.MinValue;
			Console.WriteLine ("IN Dates {0}\n", _text);
			Match match;
			match = Regex.Match (_text, @"Arrival date:\s*(.*)");
			Console.WriteLine ("arrival success = {0}",match.Success);
			if (match.Success) {

				arrival = DateTime.Parse (match.Groups [1].Value);
			}
			match = Regex.Match (_text, @"Departure date:\s*(.*)"); //\r?\n");
			if (match.Success) {
				departure = DateTime.Parse (match.Groups [1].Value.Trim ());
			}

//			Console.WriteLine ("Arrival = {0}  departure = {1}", arrival.ToShortDateString (), departure.ToShortDateString ());
			return new [] { arrival, departure};
		}

//		Total # in party: 3 (including children)
//		No of children: 1
		int[] Guests()
		{
			int total = 0;
			int children = 0;
			Match match;

			match = Regex.Match (_text, @"Total # in party?:\s*(.*) \(") ;
			if (match.Success) {
				total = Convert.ToInt32 (match.Groups [1].Value);
				match = Regex.Match (_text, @"No of children:\s*(.*)");
				if (match.Success)
					children = Convert.ToInt32 (match.Groups [1].Value);
				return new [] { total - children, children };
			}

//			# in Party:	3 (3 adults, 0 children)
			match = Regex.Match (_text, @"\((\d) adults,\s(\d) ch*\r?\n");
			if (match.Success){
				var adults = Convert.ToInt32 (match.Groups [1].Value);
				children = Convert.ToInt32 (match.Groups [2].Value);
				return new [] {Convert.ToInt32 (match.Groups [1].Value), children = Convert.ToInt32 (match.Groups [2].Value)};
			}
			return new[] { 0, 0 };
		}

		string Phone()
		{
			//Phone:		702-526-2470
			RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.Multiline;
			Match match = Regex.Match (_text, @"Phone #:?(.*)\r?\n", options);
			if (match.Success) {
				var phone = match.Groups [1].Value.Trim ();
//				match = Regex.Match (phone, "Total");
//				if (match.Success)
//					phone = "";
				return phone;
			}	
			return "";
		}

		string EMail()
		{
			//Email:		dhf0820@gmail.com
			Match match = Regex.Match (_text, @"Email:?(.*)\r?\n");
			if (match.Success)
//				return match.Groups [1].Value.TrimEnd ('\r', '\n');
				return match.Groups [1].Value.TrimEnd ();
			else
				return "";
		}

		string Comments()
		{
			pattern = String.Format (@"Further info:\s*(.*is.*in your calendar)", Inquiry.ArrivalDate.ToString ("MMM d"));

			Match match = Regex.Match (_text, pattern, RegexOptions.Singleline);
//			Match match = Regex.Match (_text, @"^Inquiry detail$(.*)Name", RegexOptions.Singleline);
			Console.WriteLine ("Comment Match = {0}", match.Success);
			if (match.Success) {

				var comment = Regex.Replace (match.Groups [1].Value.TrimEnd ('\r', '\n'), @"> ", "");
//				Console.WriteLine ("Looked for {0}", pattern);
				return comment;
			}

			match = Regex.Match (_text, @"Comments:\s*(.*)", RegexOptions.Singleline);
			if (match.Success) {
				var comment = Regex.Replace (match.Groups [1].Value.TrimEnd ('\r', '\n'), @"				> ", "");
				return comment;
			}
//			Console.WriteLine ("Looked for {0}", pattern);
//			Console.WriteLine ("Comment not found");
			return "";
		}

	}
}


//		Inquiry for property #292391 [http://www.vrbo.com/209114?
//		                              utm_source=NL&utm_medium=email&utm_term=20131007&utm_content=propertyid_text_o_loth&utm_campaign=HAUSOwnerInquiry
//		                              ]
//		Available
//			Oct 10, 2013 - Oct 17, 2013 (7 nights)
//				Reply [https://www.homeaway.com/haoi/121.292391.3076026/emailReplyAvailable.html?inquiryId=5cd920f87fb144a482b4d533aeffbed8&cid=E_OwnerInquiry_NL_O_20131007_RAA_button_LOTH]
//				       Inquiry Detail
//				       Inquiry from vrbo-209114
//				       Name Theresa French
//				       Dates Oct 10, 2013 - Oct 17, 2013
//				       Guests 2 adults, 1 children
//				       Phone 7035039665
//				       Email dhf0820@gmail.com
//				       Inquiry from VRBO.com
