using System;
using SharedLibrary.DL;
using System.Text.RegularExpressions;



namespace SharedLibrary.SAL.MailProcessing
{
	public class Villas : IInquirySource
	{
		private Inquiry _inq;
		private string _text;
		private VrMail _email;
		public Inquiry Inquiry { 
			get {return _inq;}
			set {_inq = value;}
		}

		public Villas (VrMail email)
		{

			_text = email.BodyAsText; //.GetBodyAsText ();
			_email = email;
		}

		public static IInquirySource From(VrMail email)
		{
			var text = email.BodyAsText; // .GetBodyAsText ();
//			Console.WriteLine (" Villa:  {0}", text);
			Inquiry inq;
//			Console.WriteLine ("Looking for Villas4Vacations");
			Match match = Regex.Match (text, @"property listing on Villa4vacation");
			if (match.Success) {
				Console.WriteLine ("Success it is a villas");
				IInquirySource source = new Villas (email);
				source.Inquiry = inq = new Inquiry();
				inq.Channel = "Villa";
				inq.ReceivedMethod = "D";
				return source;
			}

			return null;
		}

		public Inquiry Extract()
		{

			if( Inquiry.FindByMessageID (_email.MessageID) != null)
			{
				return null;
			}
//			foreach(Listing l in Listing.ListingsByProperty(Property.CurrentProperty.ID))
//			{
//				Console.WriteLine ("Channel = {0} =  {1}", l.ChannelCode, l.ListingNumber);
//			}
			_inq.MessageID = _email.MessageID;
			_inq.ListingNumber = ListingNumber ();
			var listing = Listing.FindByCodeAndNumber (_inq.Channel ,_inq.ListingNumber);

			if (listing == null) {
				Console.WriteLine ("Listing {0} for {1}", _inq.Channel, _inq.ListingNumber);
				return null;
			}
			_inq.PropertyID = listing.PropertyID;
			_inq.ArrivalDate = ArrivalDate ();
			_inq.DepartureDate = DepartureDate ();
			_inq.GuestName = GuestName ();
			_inq.Adults = Guests ();
			_inq.Children = 0;

			_inq.Email = EMail ();
			_inq.Phone = Phone ();
			_inq.Comment = Comments ();
			_inq.CurrentState = Inquiry.InquiryState.New;
			_inq.InquiryDate = _email.ReceivedDate; //.Date == null ? new DateTime() : (DateTime)_email.Date;
			_inq.SourceDocument = _email.BodyAsHTML; //.GetBodyAsHtml();
			#if DEBUG
			Console.WriteLine ("\tListing = {0}",_inq.ListingNumber);
			Console.WriteLine ("\tProperty Number = {0}", _inq.PropertyID);
			Console.WriteLine ("\tGuest = {0}", _inq.GuestName);
			Console.WriteLine ("\tEmail = [{0}]", _inq.Email);
			Console.WriteLine ("\tPhone = {0}", _inq.Phone);
			Console.WriteLine ("\tAdults = {0}", _inq.Adults);
			Console.WriteLine ("\tChildren = {0}", _inq.Children);
			Console.WriteLine ("\tDates = {0} - {1}", _inq.ArrivalDate.ToShortDateString(), _inq.DepartureDate.ToShortDateString ());
			Console.WriteLine ("\tComment: {0}", _inq.Comment);
			#endif

			_inq.Save ();
			return _inq;
		}

		//Listing #:	209114
		string ListingNumber()
		{
			Match match = Regex.Match (_email.Subject, @"Inquiry for Listing (.*)", RegexOptions.None);
			if (match.Success) 
				return match.Groups [1].Value.TrimEnd (' ', '\r', '\n');
			else
				return null;
		}

		DateTime ArrivalDate()
		{
			Match md = Regex.Match (_text, @"Check-in date\s*(.*)");
			if (md.Success)
				return DateTime.Parse (md.Groups [1].Value.TrimEnd (' ', '\r', '\n'));
			else
				return  new DateTime ();
		}

		DateTime DepartureDate()
		{
			Match md = Regex.Match (_text, @"Check-out date\s*(.*)");
			if (md.Success)
				return DateTime.Parse (md.Groups [1].Value.TrimEnd (' ', '\r', '\n'));			
			else
				return  new DateTime ();
		}


		int Guests()
		{
			//> Guests	 2 adults, 1 children=0D
			Match match = Regex.Match (_text, @"Party size:\s*(\d+)") ;
			if (match.Success){
				return Convert.ToInt32 (match.Groups[1].Value);
			}
			else {
//				Console.WriteLine ("Guests not found");
				return 0;
			}
		}

		string GuestName()
		{
			//Name:		Theresa French
			Match match = Regex.Match (_text, @"Customer name:\s*(.*)");
			if (match.Success) 
				return match.Groups [1].Value.TrimEnd (' ', '\r', '\n');
			else
				return "";
		}

		string EMail()
		{
			//Email:		dhf0820@gmail.com
			Match match = Regex.Match (_text, @"E-mail:\s*(.*)");
			if (match.Success)
				return match.Groups [1].Value.TrimEnd ('\r', '\n',' ');
			else
				return "";
		}

		string Phone()
		{
			//Phone:		702-526-2470
			Match match = Regex.Match (_text, @"Phone:\s*(.*)");
			if (match.Success)
				return match.Groups [1].Value.TrimEnd ('\r', '\n', ' ');
			else
				return "";
		}

		string Comments()
		{
			//Comments:	I will take it as long as the sexy owner is there with me.

			Match match = Regex.Match (_text, @"Comments:\s*(.*?)-----", RegexOptions.Singleline);
			if (match.Success)
				return Regex.Replace (match.Groups [1].Value.TrimEnd ('\r', '\n'), @"> ", " ");
			return "";
		}
		//		Inquiry for property #292391 [http://www.vrbo.com/209114?
		//		                              utm_source=NL&utm_medium=email&utm_term=20131007&utm_content=propertyid_text_o_loth&utm_campaign=HAUSOwnerInquiry
		//		                              ]
		//		Available
		//			Oct 10, 2013 - Oct 17, 2013 (7 nights)
		//				Reply [https://www.homeaway.com/haoi/121.292391.3076026/emailReplyAvailable.html?inquiryId=5cd920f87fb144a482b4d533aeffbed8&cid=E_OwnerInquiry_NL_O_20131007_RAA_button_LOTH]
		//				       Inquiry Detail
		//				       Inquiry from vrbo-209114
		//				       Name Theresa French
		//				       Dates Oct 10, 2013 - Oct 17, 2013
		//				       Guests 2 adults, 1 children
		//				       Phone 7035039665
		//				       Email dhf0820@gmail.com
		//				       Inquiry from VRBO.com
	}
}



