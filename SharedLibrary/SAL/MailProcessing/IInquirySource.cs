using System;
using SharedLibrary.DL;

namespace SharedLibrary.SAL.MailProcessing
{
	public interface IInquirySource
	{
//		IInquirySource IsFrom(string rawEmail, string textEmail);
		Inquiry Extract ();
		Inquiry Inquiry{ get; set;}

	}
}

