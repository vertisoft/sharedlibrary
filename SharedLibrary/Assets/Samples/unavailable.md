{{today}}



Aloha {{guest_first_name}},

I am sorry our vacation condo is currently unavailable for arrival {{arrival_date}} and departing {{departure_date}}.

I do keep my calendar upto date and if there are others dates you might be interested in, please let me know.

We provide a mobile cooler and beach chairs with an umbrella and beach towels. Just bring a swim suit and a limited number of clothes and you will be set. Quality body boards and snorkel equipment is readily available for rental close by. The companies that rent them are professionals and make sure the equipment is in top condition to avoid injury.

A split AC system is very much like a central air conditioner. It is much quieter and provides a better comfort level than window units (the normal in Kihei). You can set the temperature just like a normal AC system.

Maui Vista is very conveniently located with grocery stores, restaurants and other activities within walking distance.

Your condo is on the first floor so it has a large dining lanai leading on to a grassy open area and since it is in Building 2, it is away from the noise of S. Kihei Rd. That street is busy and noisy. There is nothing like this condo in Maui Vista. One of our previous guests came with friends who had reserved another unit, when they saw ours, they were very unhappy. They did pay a little less but got the run of the mill basic Maui Vista condo with limited amenities. They booked their next stay with us. Many of these "low cost" units have not been remodeled or updated since they were built in the 80's. Most units do not offer king beds and almost no other offers top quality memory foam mattress. The others 'a0do not offer state of the art electronics or fully equipped kitchens either. In those you are lucky to get a semi working 20" TV. We provide a 47" LCD HDTV in the living room and a 37" HDTV in the bedroom. We also provide a Wii game console and two iPod stereos. Just plug in you iPod and listen to your music. We also provide both wireless and wired fire-walled access. Just bring your notebook.

You will love Maui. We have many first timers and old time Maui visitors stay with us and just love it. Quiet but central to everything. Many of our guests who have previously stayed in other units have commented that they could not believe how our unit was equipped. We provide a kitchen with everything you would have at home. It can get expensive eating out all the time on Maui. Having a good kitchen with more than a couple of pans can make the difference.

Most vacation condos on Maui are low cost queen beds, however I have put in a high quality memory foam king bed. Several of my guests have asked for information on the bed as they wanted to buy one for their homes. Also, earlier while it was vacant, there was a guest next door (some other owner) that saw our unit because I had the drapes open and he wanted to change. Of course he couldn't because we are independent, but he said, "next year we are staying with you".


I know you will find no other unit in the south Kihei area "tricked" out like this one.

If you would like additional information or like to reserve another date, please call me at 702-843-0810.

Mahalo

Don French
