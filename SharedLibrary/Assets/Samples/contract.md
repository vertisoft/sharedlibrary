SHORT TERM RENTAL AGREEMENT

This agreement made this {{day}} day of {{month}}, {{year}}  between {{guest_name}} Phone:  {{guest_phone}} (hereinafter called the Tenants) and Luxury Maui Condos LLC (hereinafter called the Landlord) concerning the short term rental of the property located at {{property_complex_name}}, 2191 South Kihei Rd Unit 2117, Kihei HI 96753
Total people in renting party: {{total_guests}}, comprising {{guests}}.
Rental period begins at 4 pm on the {{arrival_date}} and ends at 11 am on the {{departure_date}}. 
Total Rental Amount: {{total_charge}}.
Damage Deposit: Guest is required to purchase a insurance policy that will provide a minimum of $1500.00 of coverage in the amount of {{damage_insurance}}. This policy is available to purchase at time of payment, and is non-refundable.
Payments: {{payment_1}} {{payment_2}} {{payment_3}}
Terms of the Agreement:
1. The Landlord have the right to inspect the premises without prior notice at any time to enforce the terms of this agreement. Should the Tenants violate any of the terms of this agreement, the rental period shall be terminated immediately. The Tenants waive all rights to process if they fail to vacate the premises upon termination of the rental period. The Tenants shall vacate the premises at the expiration time and date of this agreement.
The Tenants shall maintain the premises in a good, clean, and ready to rent condition, and use the premises only in a careful and lawful manner. The tenants shall leave the premises in a clean condition at the expiration of the rental agreement, defined by the Landlord as trash being removed, dishes in the dishwasher, and generally a ready to rent condition with normal cleaning. Tenants shall pay for maintenance and repairs should the premises be left in a lesser condition. The tenants agree that the Landlord shall deduct costs of said services from the damage/security deposit prior to refund if tenants cause damage to the premises or its furnishings.
The Tenants shall dispose of all waste material generated during the rental period in a lawful manner and put the trash in the dumpsters provided by the complex..
The Tenants shall pay for any damage done to the premises over and above normal wear and tear not covered by insurance.
The Tenants shall not sublet the property.
There are no pets allowed on the property at any time.
The Tenants shall have no more than four (4) persons reside or sleep on the premises unless prior written approval is provided by the Landlord. 
The Tenants shall behave in a civilized manner and shall be good neighbors respecting the rights of the surrounding property owners. The Tenants shall not create noise or disturbances likely to disturb or annoy the surrounding property owners. Creating a disturbance of the above nature shall be grounds for immediate termination of this agreement and Tenants shall then immediately vacate the premises. Quiet hour starts at 10 PM and Pool and outdoor noise should be kept to a minimum. 
There shall be no smoking inside the premises, on the lanai or on the Maui Vista complex except in approved smoking areas. There will be a $300 fine for violating this clause.
Landlord shall provide towels, linens, cups, knives, forks, spoons, dishes, pots, pans and other non consumable items as commonly used by the Landlord’s family. Toilet paper, soap, dish detergent, laundry soap, shampoos, and other consumables are to be purchased by the Tenant. No reimbursement will be made for unused consumables left at the premises. If consumables exist at the premises when the Tenant arrives the Tenant is free to use them.
The Tenants and Tenants' Guests shall hereby indemnify and hold harmless the Landlord against any and all claims of personal injury or property damage or loss arising from use of the premises regardless of the nature of the accident, injury or loss. Tenants expressly recognize that any insurance for property damage or loss which the Landlord may maintain on the property does not cover the personal property of Tenants, and that Tenants should purchase their own insurance for Tenants and Guests if such coverage is desired.
All rental payments are non refundable unless the Landlord can rent the Property for the same period and amount. Otherwise all payments made, become liquidated damages.
Tenants agree to pay all reasonable costs, attorney's fees and expenses that shall be made or incurred by Landlord enforcing this agreement.
Tenants expressly acknowledge and agree that this Agreement is for transient occupancy of the Property, and that Tenants do not intend to make the property a residence or household.
We occasionally experience outages that are beyond our control.  We report outages as each occurs.  No refunds or compensation will be given for any outages.
There shall be no refunds of rents or deposits already paid due to shortened stays or ruined expectations because of weather conditions. The Landlord recommends the Tenant purchase Travel Insurance.
There shall be no refunds of rents or deposits already paid because of changes in plans, shortened stays or ruined expectations due to work and family emergencies or other commitments. The landlord recommends the Tenant purchase Travel Insurance.
It is the tenant’s responsibility to learn about safety precautions, warning signs of water conditions, and safety procedures concerning swimming in or being around the pool. Tenant agrees to have a responsible adult supervising minors while they swim in the pool. Tenant is hereby notified that the pool can be dangerous and tenant accepts fully the risks involved. 
Tenant agrees that Fireworks and other hazardous materials shall not be used in or around the property.
Tenant shall use the property for legal purposes only and other use, such as but not limited to, illegal drug use, abuse of any person, harboring fugitives, etc. ; shall cause termination of this agreement with no refund of rents or deposits.
The property has a fire extinguisher installed near the kitchen area. The fire extinguisher was fully charged at last inspection. It is the duty of the tenant to inform management immediately should the fire extinguisher become less than fully charged. Tenant agrees to use the fire extinguisher only for true emergencies.
The property has fire alarms installed and they are believed to function properly at the time of rental. Tenant will notify management without delay if a fire alarm “chirps” or has a low battery condition.
Tenant shall see to their own security while in the property by locking doors, windows, etc. when it’s prudent to do so.
Valuable items left behind by tenant will be held for the tenant and every reasonable effort will be made to contact the tenant for return. If items are not claimed for longer than 3 months they shall become the property of the Landlord. The Landlord shall not be help liable for condition of said items.
Cable TV is provided and service level has been chosen by the Landlord. No refund of rents shall be given for outages, content, lack of content, or personal preferences with regard to cable TV service.
High speed wireless internet is provided as a connivence only and is not integral to the agreement. No refund of rents shall be given for outages, content, lack of content, speed, access problems, lack of knowledge of use, or personal preferences with regard to internet service. 
Tenant agrees that Air conditioning shall not be set below 76 degrees. Doors and windows shall be closed when the air conditioning is in operation.
Tenant agrees to follow all additional rules and regulations  shown in Exhibit A.
HURRICANE OR STORM POLICY - No refunds will be given for hurricanes or storms. Hawaii is in a tropical area and has received Hurricanes in the past. We highly recommend all guests purchase travel insurance.  If you wish to purchase travel insurance, go to www.InsureMyTrip.com for details and to purchase.
Please fill out and sign this Agreement and mail , fax or scan and e0mail to Landlord. One executed original will be sent back to you.
Landlord address: Luxury Maui Condos LLC., 8162 Misty Sage St. Las Vegas NV 89139,  Fax number is (702) 924-0718, E-mail address is info@luxurymauivista.com.
(I/We) agree to abide by the above conditions and hereby swear that the information provided above is true:

Tenant ______________________________________________________________________ Date ______________


EXHIBIT A - RENTAL RULES AND REGULATIONS

1. CHECK-IN TIME IS AFTER 4 P.M. HST AND CHECK-OUT IS BY 11 A.M. HST. 
2. This is a NON SMOKING unit. This includes NO SMOKING on the lanai.
3. Pets are not permitted in rental units or at the complex under any conditions.
4. We will not rent to vacationing students or singles under 25 years of age unless accompanied by an adult guardian or parent.
DAMAGE NOT COVERED BY INSURANCE -  Damage insurance is required, as show in your rental confirmation (Exhibit A), is required.  This insurance covers accidental damage to the property during your stay beyond normal wear and tear. It does not cover damage caused by illegal activities, misuse, or vandalism. Please report any damage immediately.
Upon departure the following is required:
a.  All debris, rubbish and discards are placed in dumpster, and soiled dishes are placed in the dishwasher and cleaned.
b.  All keys are left on the kitchen table and unit is left locked.
c.  All charges accrued during the stay are paid prior to departure.
SMOKING - No Smoking in the unit or on the Lanai. There is a $300.00 fine for smoking inside the unit or on the lanai.
6.   PAYMENT - An advance payment equal to 50% of the rental rate is required within 10 days after reservation confirmation. The advance payment will be applied toward the room rent. Please make payments in the form of Credit Card or personal checks payable to Luxury Maui Condos. The advance payment is not a damage deposit. The BALANCE OF RENT is due sixty (60) days prior to your arrival date.
  CANCELLATIONS -   Cancellations or changes that result in no stay or a shortened stay, forfeit the full advance payment unless the Condo can be re-rented for the same period and price. Cancellation or early departure does not warrant any refund of rent or deposit.
8.  MAXIMUM OCCUPANCY- The maximum number of guests per condominium is limited to four (4) persons. An additional charge of $10.00 per person per night for each guests in addition to two (2) will be assessed regardless of the age of the additional guests, unless otherwise stated in reservation (Exhibit A).
9.  CLEANING FEES - Each reservation will incur a cleaning fee as shown on the reservation (Exhibit A).
10. INCLUSIVE FEES - Rates include a initial one-time linen-towel setup. Maui Vista amenity and parking fees are included in the rental rate.
11. NO DAILY MAID SERVICE - While linens, bath and beach towels are included in the unit, daily maid service is not included in the rental rate. We do not permit bath towels or linens to be taken from the units, but we provide extra beach towels for your external use.  Additional cleaning service is available for an additional $85.00 per time.
13. RATE CHANGES - Rates subject to change without notice until initial payment is received.
14. FALSIFIED RESERVATIONS - Any reservation obtained under false pretense will be subject to forfeiture of advance payment, deposit and/or rental money, and the party will not be permitted to check-in.
15. WRITTEN EXCEPTIONS - Any exceptions to the above mentioned policies must be approved in writing in advance.
16. CHECKING IN. Upon arrival or a soon there after as possible, you MUST check in with the Maui Vista office in Building One to receive your parking pass. This is a state law, the same as every hotel that all guests must be registered to legally stay in a condo.
PARKING PASSES - Parking passes are received from the front desk. Guests MUST display the parking pass as instructed by the front desk at all times. Failure to display may result in towing of vehicle at tenant's expense.
Follow all additional rules provided at checkin by Maui Vista.
No Business activity permitted in condo. Doing so will result in eviction and loss of all payments.
HURRICANE OR STORM POLICY - No refunds will be given for hurricanes or storms. Hawaii is in a tropical area and has received Hurricanes in the past. We highly recommend all guests purchase travel insurance.  If you wish to purchase travel insurance, go to www.InsureMyTrip.com for details and to purchase.

This unit is privately owned; the owners are not responsible for any accidents, injuries or illness that occurs while on the premises or its facilities. The owners are not responsible for the loss of personal belongings or valuables of the guest. By accepting this reservation, it is agreed that all guests are expressly assuming the risk of any harm arising from their use of the premises or others whom they invite to use the premise.

