using System;
using System.Text.RegularExpressions;
using System.Globalization;
//using Android.Util;


namespace SharedLibrary
{
	public partial class Utils
	{


		public static bool ToBool(int value){
			if (value != 0)
				return true;
			return false;
		}

		public static string NightsString(double nights)
		{
			var nightsString = "";
			if (nights == 1)
				nightsString = String.Format ("{0} night", nights);
			else
				nightsString = String.Format ("{0} nights", nights);
			return nightsString;
		}

		public static decimal Parse(string value)
		{
			if (IsNumeric (value)) {
				decimal decval;
				bool convt = decimal.TryParse (value, NumberStyles.Currency,
					             CultureInfo.CurrentCulture.NumberFormat, out decval);
				if (convt)
					return decval;
			}
			return 0.00m;
//			decimal _amount = Decimal.Parse (value, NumberStyles.Currency);
//			return _amount;
		}

		public static int ParseInt(string value)
		{
			int intval;
			if (IsNumeric (value)) {
				bool convt = Int32.TryParse (value, NumberStyles.Integer,
					             CultureInfo.CurrentCulture.NumberFormat, out intval);
				if (convt)
					return intval;
			}
			return 0;

		}

		public static DateTime ParseDate(string value)
		{

			DateTime dtvalue;
			if (String.IsNullOrWhiteSpace (value))
				return DateTime.MinValue;

			bool convt = DateTime.TryParse (value, out dtvalue);
			if (convt)
				return dtvalue;
			throw new VsException ("Invalid Date");
		}

		public static string AmountToString(decimal value)
		{
			return value.ToString ("F");
		}

		public static decimal AmountToDecimal(string value)
		{
//			Log.Debug ("VrWizard", String.Format ("Amount to decimal = {0}", value));
			if (IsNumeric (value))
				return Decimal.Parse (value, NumberStyles.Currency);
			else
				return 0.00m;
		}

		public static string PercentageToString(decimal value)
		{
			return (value * 100).ToString ("F");
		}

		public static decimal PercentageToDecimal(string value)
		{
			if (IsNumeric (value))
				return Decimal.Parse(value, NumberStyles.Currency) / 100;
			else
				return (Decimal) 0.0;
		}

		public static decimal CalculateTax(decimal taxableAmount, decimal taxRate)
		{
			return Math.Round(taxRate * taxableAmount, 2);
		}

		public static string ReservationType(string t)
		{
			string resvType;

			switch(t)
			{
			case "R":
				resvType = "Reserved";
				break;
			case "T":
				resvType = "Tentatatively reserved";
				break;
			case "B":
				resvType = "Blocked";
				break;
			case "A":
				resvType = "Available";
				break;
			default:
				resvType = "Unknown";
				break;
			}
			return resvType;
		}

		public static bool IsNumeric(string value)
		{
			value = value.Trim ();
			double Num;
			bool isNum = double.TryParse(value, out Num);
			if (isNum)
				return true;
			else
				return false;
		}
	}


}

