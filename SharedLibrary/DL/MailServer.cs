using System;
using SQLite;
using SharedLibrary.BL;
using SharedLibrary.DAL;
using SharedLibrary.LIB;
//using Javax.Crypto;

namespace SharedLibrary.DL
{
	public class MailServer : BL.Contracts.BusinessEntityBase
	{
		[Indexed]
		public int		PropertyID{ get; set;}
		public string	UserLogin{ get; set;}
		public string	UserPassword{ get; set;}
		public string	ServerName{ get; set; }
		public int		Port{ get; set;}
		public string	InBox{ get; set;}
		public string	Archive{ get; set;}
		public bool		UseSSL{ get; set;}
		public string	ApiKey{ get; set;}
		//		public bool		DirectEmail{ get; set;}  //directly receive email
		[Indexed]
		public string 	RemoteID{ get; set;}

		private VrWizardServer _vrserver;

		public MailServer ()
		{
			UserLogin = "";
			ServerName = "";
			InBox = "InBox";
			UserPassword = "";
			UseSSL = true;
			Port = 993;
			ApiKey = "";
			//DirectEmail = false;
			//_vrserver = new VrWizardServer ();

		}

		public MailServer(int propertyID)
		{
			PropertyID = propertyID;
			UserLogin = "";
			ServerName = "";
			InBox = "InBox";
			Archive = "Archive/VrWizard";
			UserPassword = "";
			UseSSL = true;
			Port = 993;
			ApiKey = "";
			//DirectEmail = false;
			//_vrserver = new VrWizardServer ();

		}

		public void Save()
		{

			if (PropertyID == 0) {
				throw new Exception ("PropertyID must be set to a valid property.id");
			}

			if (ID == 0) {
//				Console.WriteLine ("MS Saving new ");
//				_vrserver.CreateServerAccount (this);
				Repository.DB.Insert (this);
			}
			else {
//				Console.WriteLine ("MS Update");
//				_vrserver.UpdateServerAccount (this);
				Repository.DB.Update (this);
			}
		}

		public static void Delete(int id) {
			if (id != 0) {
				Repository.DB.Delete<MailServer> (id);
			}
		}

		public void Delete() {
			if (ID != 0) {
				Repository.DB.Delete (this);
			}
		}

		public void DeleteAll(int propertyID)
		{
			var ms = Find (propertyID);
			if (ms != null)
				ms.Delete ();
		}



		public static int Count
		{
			get {
				var command = Repository.DB.CreateCommand("select count(*) from MailServer");
				var count = command.ExecuteScalar<int>();
				return count;
			}
		}


		public static MailServer Find(int id )
		{
			var record = Repository.DB.Find<MailServer> (id);
			if (record == null)
				throw new ArgumentException ("MailServer ID " + id +" is invalid");
			return record;
		}
	}
}


