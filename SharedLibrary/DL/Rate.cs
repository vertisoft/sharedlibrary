using System;
using System.Collections.Generic;
using SQLite;
using SharedLibrary.DAL;
using SharedLibrary.DL;
using System.Globalization;

namespace SharedLibrary.DL
{
	public class Rate
	{

		[PrimaryKey, AutoIncrement]
		public int 		ID { get; set; }

		[Indexed(Name = "RateIndex", Order = 2, Unique = true)]
		public string	Name { get; set; }
		public string 	Description { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public string   WeekendDays { get; set; }   //  Fixed to Fri/Sat PRO ONLY = "DDDDDWW" Weekend Days are returned.
		public decimal  Weekend { get; set; }
		public decimal  Weekday { get; set; }
		public decimal  Weekly { get; set; }
		public decimal  Monthly { get; set; }
		public decimal  ExtraGuestRate { get; set; }
		public int      RatePeople { get; set; }
		public int      MinNights { get; set; }
		public string	RateType { get; set; }   // D = Default  N = Normal Seasonal   PRO ONLY S = Special by date P = Promotional (select Only)  
		[Indexed(Name = "RateIndex", Order = 1, Unique = true)]
		public int		PropertyID { get; set; }
		[Indexed]
		public string   RemoteID{ get; set;}



		// Used in calculated the rate to use in a quote
		[Ignore]
		public decimal	DaysInRate{ get; set;}
		[Ignore]
		public DateTime RateStart{ get; set;}
		[Ignore]
		public DateTime RateEnd{ get; set;}
		public int DayMap {get; set;}  // bitmap of what are weekdays and what are weekend hi = sunday

		public Rate(){
		}

		public Rate(int id)
		{
			PropertyID = id;
		}

		public void Save()
		{
			if (PropertyID == 0) {
				throw new Exception ("PropertyID must be set to a valid property.id");
			}

			if (ID == 0)
				Repository.DB.Insert (this);
			else
				Repository.DB.Update (this);
		}

		public static void Delete(int id) {
			if (id != 0) {
				Repository.DB.Delete<Rate> (id);
			}

		}

		public void Delete() {
			if (ID != 0) {
				Repository.DB.Delete (this);
			}
		}

		public static void DeleteAll(int propertyID)
		{
			var records = RatesByProperty (propertyID);
			if (records == null)
				return;
			foreach (Rate r in records)
			{
				r.Delete ();
			}
		}


		public static List<Rate> RatesByProperty(int propertyID)
		{
			return Repository.DB.Query<Rate> ("Select * from Rate where PropertyID = ?", propertyID);
		}

		public  int Count
		{
			get {
				var command = Repository.DB.CreateCommand("select count(*) from Rate where PropertyID = ?", PropertyID);
				var count = command.ExecuteScalar<int>();
//				Console.WriteLine ("Rate.Count returning {0}", count);
				return count;
			}
		}

		public static Rate Find(int id )
		{
			var record = Repository.DB.Find<Rate> (id);
			if (record == null)
				throw new ArgumentException ("Rate ID " + id +" is invalid");
			return record;
		}
		



		public static List<Rate> Rates(int propertyID, string rateType)
		{
			var rates = Repository.DB.Query<Rate> ("Select * from Rate where PropertyID = ? and RateType = ? order by StartDate", 
			                                       propertyID, rateType);
			return rates;
		}

		public static List<Rate> Find(int id, string name)
		{
			var rates = Repository.DB.Query<Rate> ("select * from Rate where PropertyID = ? and Name = ?", id, name);
			return rates;
		}

		public static Rate WhichSpecialRate(Property prop, DateTime arrivalDate, DateTime departureDate)
		{

			Rate special = null;

			var specials = Repository.DB.Query<Rate> ("select * from Rate where PropertyID = ? and RateType = ?", prop.ID, "SPL");

			if (specials.Count == 0) return null;
			foreach(Rate r in specials)
			{
				#if DEBUG
				Console.WriteLine ("Which Rate Specials ");
				Console.WriteLine ("    Base at arrivalDate {0} departureDate {1}",  arrivalDate, departureDate);
				Console.WriteLine ("    Looking at StartDate {0}  EndDate = {1}", r.StartDate, r.EndDate);

				Console.WriteLine ("    Start to arrival = {0}", r.StartDate.CompareTo(arrivalDate));
				Console.WriteLine ("    Start to departure = {0}", r.StartDate.CompareTo(departureDate));
				Console.WriteLine ("    End to arrival = {0}", r.EndDate.CompareTo(arrivalDate));
				Console.WriteLine ("    End to departure = {0}", r.EndDate.CompareTo(departureDate));
				#endif
				if(r.StartDate.CompareTo(departureDate) < 0 && r.EndDate.CompareTo(arrivalDate) > 0)
//					if (arrivalDate.CompareTo(r.StartDate) >= 0 &&  departureDate.CompareTo(r.StartDate) >= 0)
				{
//					Console.WriteLine ("Found Special {0}", r.Name);
					special = r;
//					Console.WriteLine ("Special {0}", special.Name);
					break;
				}
			}
//			Console.WriteLine ("Returning special {0}", special);
			return special;

		}

		public static List<Rate> WhichRate(Property property, DateTime arrivalDate, DateTime departureDate)
		{
			var validRates = new List<Rate> ();
			var startRate = -1;
			var endRate = -1;
			List<Rate> rateTable = RateTable (property);
			Rate validRate;
			foreach (Rate r in rateTable) {
				validRate = null;
				if (arrivalDate >= r.StartDate && arrivalDate <= r.EndDate && departureDate <= r.EndDate) {

					// all in one rate
					r.DaysInRate = (departureDate - arrivalDate).Days;
//					Console.WriteLine ("Arrival and Departure are in {0} for {1} days ({2} - {3})",
//						r.Name, r.DaysInRate, arrivalDate, departureDate);
					validRates.Add (r);
					break;
				}
				if (arrivalDate >= r.StartDate && arrivalDate <= r.EndDate) {
					// arrival is in this rate
					r.DaysInRate = (r.EndDate - arrivalDate).Days + 1;
//					Console.WriteLine ("      Arrival is in {0} for {1} days ({2} - {3})", 
//						r.Name, r.DaysInRate, arrivalDate.ToShortDateString (), r.EndDate.ToShortDateString ());
					validRates.Add (r);
					continue;
				}
				if (arrivalDate < r.StartDate && departureDate > r.EndDate) {
					// entire rate period is included in the quote
					r.DaysInRate = (r.EndDate - r.StartDate).Days + 1;
//					Console.WriteLine ("      Entire Rate  {0} is included in quote for {1} days ({2} - {3})", 
//						r.Name, r.DaysInRate, r.StartDate.ToShortDateString (), r.EndDate.ToShortDateString ());
					validRates.Add (r);
					continue;
				}

				if (departureDate >= r.StartDate && departureDate <= r.EndDate) {
					r.DaysInRate = (departureDate - r.StartDate).Days;
//					Console.WriteLine ("      Departure is in {0} for {1} days ({2} - {3})", 
//						r.Name, r.DaysInRate, r.StartDate.ToShortDateString (), departureDate.ToShortDateString ());
					validRates.Add (r);
					break;  // found all rates
				}
			}
			return validRates;
		}

		static List<Rate> RateTable(Property property)
		{

			var storedRates = Rates (property.ID, "SEA" );
			var allRates = new List<Rate> ();
			var lastEndDate = DateTime.MinValue;
			foreach(Rate r in storedRates)
			{
				if (r.StartDate > lastEndDate) {
					var defRate = AddDefaultRate (property, lastEndDate, r.StartDate.AddDays(-1));
					allRates.Add (defRate);
//					lastEndDate = defRate.EndDate;
				}
				lastEndDate = r.EndDate.AddDays (1);
				allRates.Add (r);
			}
			allRates.Add (AddDefaultRate (property, lastEndDate, DateTime.MaxValue)); //lastEndDate.AddYears (2)));
//			foreach(Rate r in allRates) 
//			{
//				Console.WriteLine ("All Name = {0}, Start = {1}, End = {2}, Daily = {3}", r.Name, r.StartDate, r.EndDate, r.Weekday);
//			}

			return allRates;
		}

		static Rate AddDefaultRate(Property property, DateTime startDate, DateTime endDate)
		{
			var defaultRate = new Rate ();
			defaultRate.Name = "Default";
			defaultRate.StartDate = startDate;
			defaultRate.EndDate = endDate;
			defaultRate.Weekend = property.Weekend;
			defaultRate.Weekday = property.Weekday;
			defaultRate.WeekendDays = property.WeekendDays;
			defaultRate.Weekly = property.Weekly;
			defaultRate.Monthly = property.Monthly;
			defaultRate.ExtraGuestRate = property.ExtraGuestRate;
			defaultRate.RatePeople = property.RatePeople;
			defaultRate.MinNights = property.MinNights;
			return defaultRate;
		}

		[Ignore]
		public string WeekendString{
			get {return Weekend.ToString ("F");}
			set {Weekend = Utils.Parse (value);}
		}
		
		[Ignore]
		public string WeekdayString{
			get {return Weekday.ToString ("F");}
			set {Weekday = Utils.Parse (value);}
		}
		
		[Ignore]
		public string WeeklyString{
			get {return Weekly.ToString ("F");}
			set {Weekly = Utils.Parse (value);}
		}
		
		[Ignore]
		public string MonthlyString{
			get {return Monthly.ToString ("F");}
			set {Monthly = Utils.Parse (value);}
		}

		[Ignore]
		public string AdditionapPeopleString{
			get {return ExtraGuestRate.ToString ("F");}
			set {ExtraGuestRate = Utils.Parse (value);}
		}

		[Ignore]
		public string RatePeopleString{
			get {return RatePeople.ToString ();}
			set {RatePeople = Utils.ParseInt (value);}
		}
		
		[Ignore]
		public string MinNightsString{
			get {return MinNights.ToString ();}
			set {MinNights = Utils.ParseInt (value);}
		}

	}
}

