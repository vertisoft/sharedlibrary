using System;
using SQLite;
using SharedLibrary.DAL;
using SharedLibrary.DL;
using SharedLibrary.BL.Contracts;
using System.Globalization;
using System.Collections.Generic;

namespace SharedLibrary.DL
{
	public class Discount : IDetail
	{
		[PrimaryKey, AutoIncrement]
		public int		ID {get; set;}

		[Indexed(Name = "DiscountIndex", Order = 2, Unique = true)]
		public string  Name { get; set; }
		public string  Description { get; set; }
		public decimal Percentage { get; set; }
		public decimal Amount { get; set; }

		[Indexed(Name = "DiscountIndex", Order =1, Unique = true)]
		public int	   PropertyID { get; set; }

		[Indexed]
		public string RemoteID { get; set;}

		[Ignore]
		public bool    Required { 
			get{return false;}
			set{ }
		}
		[Ignore]
		public decimal    PerNightAmount { 
			get{return 0.00m;}
			set{}
		}

		[Ignore]
		public bool    Refundable { 
			get{return false;} 
			set{ } 
		}
		[Ignore]
		public bool    Taxable { 
			get{return false;} 
			set{ } 
		}

		public Discount(){}


		public Discount(int id)
		{
			PropertyID = id;
		}

		public void Save()
		{
			if (PropertyID == 0) 
				throw new VsPropertyIDMisingException ("PropertyID must be set to a valid property.id");

			if (ID == 0)
				Repository.DB.Insert (this);
			else 
				Repository.DB.Update (this);
		}


		public void Delete(int id) {
			if (id != 0) {
				Repository.DB.Delete<Discount> (id);
			}
		}

		public void Delete() {
			if (this.ID != 0) {
				Repository.DB.Delete (this);
			}
		}

		public static void DeleteAll(int propertyID)
		{
			var discounts = Discount.Discounts (propertyID);
			if (discounts == null)
				return;
			foreach(Discount d in discounts){
				d.Delete ();
			}
		}

		public static Discount Find(int id )
		{

			var record = Repository.DB.Find<Discount> (id);
			if (record == null)
				throw new ArgumentException ("Discount ID " + id +" is invalid");
			return record;
		}

		public static Discount FindByName(int propertyID, string name)
		{
			var discs = Repository.DB.Query<Discount> ("select * from Discount where PropertyID = ? and name = ?", propertyID, name);
			if (discs.Count == 0)
				return null;
			return discs [0];
		}

		public static List<Discount> Discounts(int propertyID)
		{
			return Repository.DB.Query<Discount> ("select * from Discount where PropertyID = ?", propertyID);

		}
		
		[Ignore]
		public string PercentageString {
			get { return (Percentage * 100).ToString ("F");}
			set { Percentage = Decimal.Parse(value, NumberStyles.Currency) / 100;}
		}
		
		[Ignore]
		public string PerNightAmountString {
			get {return PerNightAmount.ToString ("F");}
			set {PerNightAmount = Decimal.Parse(value, NumberStyles.Currency); }
		}
		
		[Ignore]
		public string AmountString {
			get { return Amount.ToString ("F"); }
			set { Amount = Decimal.Parse (value, NumberStyles.Currency); }
		}

	}
}

