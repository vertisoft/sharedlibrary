using System;
using SQLite;
using SharedLibrary;
using SharedLibrary.DAL;

namespace SharedLibrary.DL
{
	// Pro version
	public class Contract : BL.Contracts.BusinessEntityBase
	{
		[Indexed]
		public int PropertyID { get; set;}

		[Indexed]
		public int InquiryID { get; set;}
		public decimal Payment1 { get; set;}
		public decimal Payment2 { get; set;}
		public decimal Payment3 { get; set;}
		public DateTime? Payment1Due { get; set;}
		public DateTime? Payment2Due { get; set;}
		public DateTime? Payment3Due { get; set;}

		public string Document { get; set;}


		public Contract ()
		{
			

		}
	}
}

