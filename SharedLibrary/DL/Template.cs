using System;
using SharedLibrary;
using SQLite;
using SharedLibrary.DAL;
using System.Collections.Generic;

namespace SharedLibrary.DL
{

	public interface ITemplate : BL.Contracts.IBusinessEntity
	{
		string		Name { get; set;}  // Limit too 20 Characters
		string		Description{ get; set;}
		string		TemplateType{ get; set;} 	// PDF, TXT
		int			TypeTemplate{ get; set;}
		int			Format{ get; set;}
		string		Contents{ get; set;}
		string		DefaultSubject{ get; set;}
		string		Attachments{ get; set; }    // Colon seperated list of all attachments that are automatically attached
		int			PropertyID{ get; set;}
	}

	public class Template : BL.Contracts.BusinessEntityBase
	{
		[Indexed(Name = "TemplateIndex", Order = 2, Unique = true)]
		public string		Name { get; set;}
		public string		Description{ get; set;}
		public string		TemplateType{ get; set;} 	// 
		public int			Type{ get; set;}  // One of Types
		public int			Format{ get; set;}
		public string		Content{ get; set;}
		public string		Subject{ get; set;}
		public string		Attachments{ get; set; }    // Colon seperated list of all attachments that are automatically attached
		[Indexed(Name = "TemplateIndex", Order = 1, Unique = true)]
		public int			PropertyID{ get; set;}
		public bool			Deleted{ get; set;}
		public DateTime		DeleteDate{ get; set;}
		[Indexed]
		public string		RemoteID {get; set;}

		public enum Types
		{
			Miscelaneous,
			Quote,
			Contract,
			Welcome
		}

		public enum Formats
		{
			PDF,
			TEXT
		}

		public Template ()
		{
		}

		public Template(int propertyID)
		{
			PropertyID = propertyID;
		}

		public void Save()
		{
			if (PropertyID == 0)
				throw new VsIdMisingException ("Property ID Must be included");

			if(ID == 0)
				Repository.DB.Insert (this);
			else
				Repository.DB.Update (this);

		}

		public void Delete()
		{
			if (ID > 0){
				this.Deleted = true;
				this.DeleteDate = DateTime.Today;
				this.Save ();
			}
		}


		public static void DeleteAll(int propertyID)
		{
			var records = TemplatesByProperty (propertyID);
			if (records == null)
				return;
			foreach (Template r in records)
			{
				r.Delete ();
			}
		}

		public static List<Template> TemplatesByProperty(int propertyID)
		{
			return Repository.DB.Query<Template> ("Select * from Template where PropertyID = ?", propertyID);
		}

		public void Remove()
		{
			if (ID > 0 && Deleted)
				Repository.DB.Delete (this);
		}
		
		public static Template Find(int id )
		{

			var record = Repository.DB.Find<Template> (id);
			if (record == null)
				throw new ArgumentException ("ID " + id +" is invalid for a Template");
			return record;
		}
		[Ignore]
		public Types TypeTemplate
		{
			get{ 
				return (Types)Type;
			}
			set{
				Type = (int)value;

			}
		}
		[Ignore]
		public Formats FormatType
		{
			get{
				return (Formats)Format;
			}
			set{
				Format = (int)value;
			}
		}


	}
}


