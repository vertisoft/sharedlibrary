using System;
using System.Collections.Generic;
using SharedLibrary;
using SharedLibrary.DAL;
using SQLite;
using Android.Provider;
using System.Net.NetworkInformation;


namespace SharedLibrary.DL
{
	public class User
	{
		[PrimaryKey, AutoIncrement]
		public int    ID { get; set; }

		public string Name {get; set;}
		public string Token { get; set; }  // Remote access token
		public string Email { get; set;}
		public string Password { get; set;}

		public string Pin { get; set;}
		public bool   PinRequired { get; set; }
//		public int 	  DemoId { get; set; }
		public bool   IsDemo{ get; set; }
		[Indexed]
		public string RemoteID{ get; set;}

		public string Status { get; set;}

//		private static int _demoPropertyID = 0;

//		public static int    DemoPropertyId { 
//			get{
//				if (Current.DemoId == 0) {
//
//					var prop = Property.Demo ();
//					_current.DemoId = prop.ID;
//					_current.Save ();
//				}
//				return Current.DemoId;
//			}
//			set {
//				Current.DemoId = value;
//				Current.Save ();
//			}
//		}

		private static User _current = null;
		public static User Current{ 
			get{
				if (_current == null)
					_current = FindUser ();
				return _current;
			}
			set { _current = value;}
		}


		public int Save()
		{
			if (ID == 0)
				return Repository.DB.Insert (this);
			else
				return Repository.DB.Update (this);
		}

		public static User FindUser()
		{
			User user;
			var users = Repository.DB.Query<User> ("select * from User");
			if (users.Count == 0) {
				user = null;
			} else {
				user = users [0];
			}
			_current = user;
//			Property.Demo ();
			return user;
		}

//		public static User CreateDemoUser()
//		{
//
//			var user = new User ();
//			user.IsDemo = true;
//			user.Name = "Demo User";
//			user.Email = "demo.user@vrwizard.com";
//			user.Save ();
//
//			return (user);
//		}

		public static User Find(int id )
		{
			var record = Repository.DB.Find<User> (id);
//			if (record == null)
//				throw new ArgumentException ("User ID " + id +" is invalid");
			return record;
		}

		public static User FindByPin(string pin)
		{
			var users = Repository.DB.Query<User> ("select * from User where Pin = ?", pin);
			if (users.Count != 0)
				return users [0];
			else
				return null;
		}

		public static List<User> Users()
		{
			var users = Repository.DB.Query<User> ("select * from User");
			return users;
		}


		public static User FindByEmail(string email)
		{
			var users = Repository.DB.Query<User> ("select * from User where email = ?", email);
			if (users.Count != 0)
				return users [0];
			else
				return null;
		}

		public static User FindByRemoteId(string id)
		{
			var users = Repository.DB.Query<User> ("select * from User where remoteid = ?", id);
			if (users.Count != 0)
				return users [0];
			else
				return null;
		}

		public static void DropUserTable()
		{
			Console.WriteLine ("Dropping User");
			Repository.DB.DropTable<User> ();
		}

		public static int Count
		{
			get {
				var command = Repository.DB.CreateCommand("select count(*) from User");
				var count = command.ExecuteScalar<int>();
//				Console.WriteLine ("Count returning {0}", count);
				return count;
			}
		}

	}
}

