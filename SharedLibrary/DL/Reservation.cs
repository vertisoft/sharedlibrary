using System;
using System.Collections.Generic;
using SharedLibrary.DAL;
using SQLite;
using System.Net.NetworkInformation;




namespace SharedLibrary.DL
{
	public class Reservation : BL.Contracts.BusinessEntityBase
	{
		public string	GuestName{ get; set;}  // Just to save having to look up the name from the inquiry or Adhoc
		[Indexed]
		public DateTime	ArrivalDate { get; set;}  // Allow for changes after confirmed
		[Indexed]
		public DateTime DepartureDate {get; set;}
		public string 	Type { get; set;}  //R = Reserved, T = Tentative, B = Block ( just not available for some reason)
		public int		State { get; set;}
		public string 	Comment { get; set;}

		[Indexed]
		public int 		InquiryID { get; set;}
		public int		PropertyID { get; set;}

		private Inquiry _inquiry = null;
		[Indexed]
		public string   RemoteID { get; set;}

		[Ignore]
		public Inquiry  ReservationInquiry {
			get {
				if (_inquiry == null) {
					_inquiry = Inquiry.Find (InquiryID);
				} else if (_inquiry.ID != InquiryID)
					_inquiry = Inquiry.Find (InquiryID);
				return _inquiry;
			}
		}

		private Property _property = null;
		[Ignore]
		public Property ReservationProperty{
			get {
				if (_property == null)
					_property = Property.Find (PropertyID);
				return _property;
			}
		}

		public enum Availability {
			None,
			Available,
			Reserved,
			Tentative,
			Blocked,
			Paid,
			Deposit,
			Canceled
		}

		public Reservation ()
		{
		}

		public Reservation(Inquiry inq, Availability typeReservation)
		{
			if (ConflictsWithOthers (inq))
				throw new VsReservationConflictException ("This new reservations conflicts with an existing one.");

			InquiryID = inq.ID;
			GuestName = inq.GuestName;
			PropertyID = inq.PropertyID;
			ArrivalDate = inq.ArrivalDate;
			DepartureDate = inq.DepartureDate;
			CurrentState = typeReservation;
		}

		public Reservation(int propID)
		{
			ID = propID;	
		}



//		public static Reservation BlockDates(int propID, DateTime firstDate, DateTime lastDate, string comment = "")
		public Reservation(int propID, DateTime firstDate, DateTime lastDate, Availability type, string name = "", string comment = "")
		{
			if ((firstDate == DateTime.MinValue) || (lastDate == DateTime.MinValue))
				throw new VsException ("Creating a Blocked Reservation requires valid dates");

			PropertyID = propID;
			CurrentState = type;
//			State = (int)type;
//			Type = Reservation.NewToOldState (type);
			var prop = Property.Find (propID);
			var inq = prop.NewInquiry ();
			inq.GuestName = name;
			GuestName = name;
			Comment = comment;
			ArrivalDate = firstDate;
			DepartureDate = lastDate;
			inq.ArrivalDate = firstDate;
			inq.DepartureDate = lastDate;
			inq.Comment = comment;
			inq.Save ();
			InquiryID = inq.ID;

		}

		public static bool ConflictsWithOthers( Inquiry inq)
		{
			bool conflict = false;
			var conflicts = Conflicts (inq.PropertyID, inq.ArrivalDate, inq.DepartureDate);
			if (conflicts == null)
				return false;
			foreach(Reservation r in conflicts){
				if (r.InquiryID != inq.ID)
					return true;  // Someone else
			}
			return false;
		}
 
		public static List<Reservation> Conflicts( int propID, DateTime requestedArrive, DateTime requestedDepart)
		{
			if ((requestedArrive == DateTime.MinValue) || (requestedDepart == DateTime.MinValue))
				return null;
			//                                                                   
			//                                                        A                      D  A        D
			// Current Reservation                                    D======================D  D========D
			// Arrive before A Departs after A              rA        rA        rD           rD    rD      NO A >= rA and A <= rD
//			Console.WriteLine ("Arrives before A Departs after A ");
			var revs = Repository.DB.Query<Reservation> ("select * from Reservation where PropertyID = ? and ArrivalDate >= ? and ArrivalDate < ?", propID, requestedArrive, requestedDepart);
			if (revs.Count > 0){
//				Console.WriteLine ("     Found Reservation Type = {0}", revs [0].Type);
				return revs;
			}
			//                                                        A                      D
			// Current Reservation                                    D======================D
			// Arrive after A & Arrive before D depart any            rA  rA       rD        rD    rD      NO A <= rA and D > rA 
//			Console.WriteLine ("Arrive after A & Arrive before D depart any");
			revs = Repository.DB.Query<Reservation> ("select * from Reservation where PropertyID = ? and ArrivalDate <= ? and DepartureDate > ?", propID, requestedArrive, requestedArrive);
			if (revs.Count > 0) {
//				Console.WriteLine ("    Found Reservations");
				return revs;
			}
			else
			 	return null;
		}

		public static int FutureCount(int propID){
			var command = Repository.DB.CreateCommand("select count(*) from Reservation where PropertyID = ? and DepartureDate > ?", propID, DateTime.Now);
			var count = command.ExecuteScalar<int>();
			return count;
		}

		public static List<Reservation> FutureReservations(int propID)
		{
			var revs = Repository.DB.Query<Reservation> ("select * from Reservation where PropertyID = ? and  DepartureDate > ? order by date(ArrivalDate)", propID,  DateTime.Now);
			if (revs.Count > 0) {
				//				Console.WriteLine ("    Found Reservations");
				return revs;
			} else
				return null;
		}

		public static decimal OccupancyRate(int year)
		{
//			select * from Reservation where ArrivalDate < '2014-01-01' and DepartureDate >= '2014-01-01' and type = 'R'
//			select * from Reservation where ArrivalDate >= '2014-01-01' and DepartureDate <= '2015-01-01' and type = 'R'

			//			var yearstart = 
//			var revs = Repository.DB.Query<Reservation> ("select * from Reservation where PropertyID = ? and ")
			return new decimal(0.0);
		}

		public static Reservation CheckAvailability( int propID, DateTime requestedArrive, DateTime requestedDepart)
		{
//			Console.WriteLine ("Checking Availability for Arrive = {0} depart - {1}", requestedArrive.ToShortDateString (), requestedDepart.ToShortDateString ());
			if ((requestedArrive == DateTime.MinValue) || (requestedDepart == DateTime.MinValue)) {
//				Console.WriteLine ("Checking availability for a missing Arrival/departure inquiry");
				return null;
			}
			var resvs = Reservation.Conflicts (propID, requestedArrive, requestedDepart);
			return resvs == null ? null : resvs[resvs.Count - 1];
		}

		public static Availability ReservationStateForDate(int propID, DateTime arriveDate, DateTime departDate)
		{
//			Console.WriteLine ("In ReservationState for Arrive {0}, Depart {1}", arriveDate.ToShortDateString (), departDate.ToShortDateString ());
			if ((arriveDate == DateTime.MinValue) || (departDate == DateTime.MinValue)) {
//				Console.WriteLine ("Missing Arrival or departure date");
				return Availability.Available;
			}
			var rev = Reservation.CheckAvailability (propID, arriveDate, departDate);
//			if (rev == null) {
//				Console.WriteLine ("Check Avaiability {0} - {1}  {2}", arriveDate, departDate, "Available");
//				return Availability.Available;
//			}
//			else {
//				Console.WriteLine ("Check Avaiability {0} - {1}  {2}", arriveDate, departDate, rev.State.ToString ());
//				return rev.State;
//			}
			return rev == null ? Availability.Available : (Availability)rev.State ; 
		}

//		public Availability State(DateTime date)
//		{
//
//		}

		public static Reservation CurrentGuest(int propID)
		{
			return Reservation.CheckAvailability (propID, DateTime.Now, DateTime.Now);
		}


		public static string ReservationType(int propID, DateTime arriveDate, DateTime departDate)
		{
//			Console.WriteLine ("In ReservationType for Arrive {0}, Depart {1}", arriveDate.ToShortDateString (), departDate.ToShortDateString ());
			if ((arriveDate == DateTime.MinValue) || (departDate == DateTime.MinValue)) {
//				Console.WriteLine ("Missing Arrival or departure date");
				return "A";
			}
			var rev = Reservation.CheckAvailability (propID, arriveDate, departDate);
			if (rev == null) {
//				Console.WriteLine ("Check Avaiability {0} - {1}  {2}", arriveDate, departDate, "A");
				return "A";
			}
			else {
//				Console.WriteLine ("Check Avaiability {0} - {1}  {2}", arriveDate, departDate, rev.Type);
				return rev.Type;
			}
			return rev == null ? "A" : rev.Type ; // No Conflicts

		}

		public void Delete() {
//			Console.WriteLine ("Deleting Reservation {0}", ID);
			if (ID != 0) {
				ReservationInquiry.SetReservationStateByReservationType (Availability.Canceled);
				ReservationInquiry.Save ();
				Repository.DB.Delete<Reservation> (ID);
			}
		}

		public static void DeleteAll(int propertyID)
		{
			var records = ReservationsByProperty (propertyID);
			if (records == null)
				return;
			foreach (Reservation r in records)
			{
				r.Delete ();
			}
		}


		public static List<Reservation> ReservationsByProperty(int propertyID)
		{
			return Repository.DB.Query<Reservation> ("Select * from Reservation where PropertyID = ?", propertyID);
		}

		public bool Save()
		{
			if (PropertyID == 0) {
				throw new Exception ("Reservation must have a PropertyID must be associated with a property.");
			}

			if (InquiryID == 0)
				throw new VsException ("Reservation must have a valid Inquiry Associated with it.");

			if (Reservation.ConflictsWithOthers (ReservationInquiry))
				return false;

			if (ID > 0) {
				Repository.DB.Update (this);
			}
			else {
				var conf = Conflicts (PropertyID, ArrivalDate, DepartureDate);
				if (conf != null){// There are existing reservations for this nquiry need to delete them
					foreach(Reservation r in conf){
						r.Delete ();
					}

				}
//		 		if (Reservation.ConflictType (this.PropertyID, this.ArrivalDate, this.DepartureDate) != "A")
//					throw new ApplicationException ("There is a conflict with this reservation");
				Repository.DB.Insert (this);
			}
			var inq = Inquiry.Find (InquiryID);
			inq.ReservationState = CurrentState;	// Make sure the inquiry is uptodate

			inq.ReservationID = this.ID;	
			inq.Save ();
			return true;
		}

		public static Reservation FindByInquiry(int id)
		{
			var resv = Repository.DB.Query<Reservation> ("Select * from Reservation where InquiryID = ?", id);
			if (resv.Count == 0)
				return null;
			return resv [0];
		}

		
		public static Reservation Find(int id )
		{

			var record = Repository.DB.Find<Reservation> (id);
			if (record == null)
				throw new ArgumentException ("Reservation ID " + id +" is invalid");
			return record;
		}

//		[Ignore]
//		public int ReservedNightsBetween(DateTime startDate, DateTime endData)


		[Ignore]
		public Availability ReservationState
		{
			get {
				return (Availability)State;
			}
			set {
				State = (int)value;
				Type = NewToOldState (value);
			}
		}
		[Ignore]
		public Availability CurrentState{
			get{
//				if (State == (int)Availability.Unknown)
//					State = (int) OldToNewState (Type);

				return (Availability)State;
			}
			set {
				State = (int)value;
				Type = NewToOldState (value);
			}
		}

		public Availability OldToNewState(string old)
		{
			switch (old[0])
			{
			case 'R':
				return Availability.Reserved;
			case 'T':
				return Availability.Tentative;
			case 'B':
				return Availability.Blocked;
			}
			return Availability.Available;
		}

		public static string NewToOldState(Availability value)
		{
			var v =  value.ToString ()[0].ToString ();
			return v;
		
		}

	}
}

