using System;
using SQLite;
using SharedLibrary.DAL;
using SharedLibrary.BL.Contracts;
using System.Globalization;
using System.Collections.Generic;


namespace SharedLibrary.DL
{
	public class Fee : IDetail
	{
		[PrimaryKey, AutoIncrement]
		public int ID{ get; set;}

		[Indexed(Name = "FeeIndex", Order = 2, Unique = true)]
		public string Name { get; set;}
		public string Description { get; set;}
		public bool Required { get; set;}
		public bool Taxable { get; set;}
		public decimal Amount { get; set;}
		public decimal PerNightAmount { get; set;}
		public decimal Percentage { get; set;}
//		public bool PerNight{ get; set;}

		[Indexed(Name = "FeeIndex", Order = 1, Unique = true)]
		public int PropertyID { get; set;}

		[Indexed]
		public string RemoteID { get; set;}

		[Ignore]
		public bool Refundable {
			get{ return false;}
			set{ }
		}


		public Fee ()
		{
		}

		public Fee(int id)
		{
			PropertyID = id;
		}

		public void Save()
		{
			if (PropertyID == 0) {
				throw new VsPropertyIDMisingException ("PropertyID must be set to a valid property");
//				throw new System.Exception ("PropertyID must be set to a valid property");
			}

			if (ID == 0)
				Repository.DB.Insert (this);
			else
				Repository.DB.Update (this);
		}

		public void Update(){
			if (PropertyID == 0) {
				throw new VsPropertyIDMisingException ("PropertyID must be set to a valid property");
			}

			Repository.DB.Update (this);
		}

		public void Delete() {
			if (ID != 0) {
				Repository.DB.Delete<Fee> (ID);
			}
		}

		public static void DeleteAll(int propertyID)
		{
			var records = Fee.Fees (propertyID);
			if (records == null)
				return;
			foreach(Fee r in records){
				r.Delete ();
			}
		}

		public static int Count
		{
			get {
				var command = Repository.DB.CreateCommand("select count(*) from Fee");
				var count = command.ExecuteScalar<int>();
//				Console.WriteLine ("Count returning {0}", count);
				return count;
			}
		}

		public static Fee Find(int id )
		{
			var record = Repository.DB.Find<Fee> (id);
			if (record == null)
				throw new ArgumentException ("Fee ID " + id +" is invalid");
			return record;
		}

		public static List<Fee> Fees(int propID)
		{
			var fees = Repository.DB.Query<Fee> ("select * from fee where PropertyID = ?", propID);
			return fees;
		}

		
		[Ignore]
		public string PercentageString {
			get { return (Percentage * 100).ToString ("F");}
			set { Percentage = Decimal.Parse(value, NumberStyles.Currency) / 100;}
		}
		
		[Ignore]
		public string PerNightAmountString {
			get {return PerNightAmount.ToString ("F");}
			set {PerNightAmount = Decimal.Parse(value, NumberStyles.Currency); }
		}
		
		[Ignore]
		public string AmountString{
			get {return Amount.ToString ("F");}
			set {Amount = Decimal.Parse (value, NumberStyles.Currency);}
		}


	}
}

