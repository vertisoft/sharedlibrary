using System;
using SQLite;
using SharedLibrary.DAL;
using SharedLibrary.BL.Contracts;
using System.Globalization;
using System.Collections.Generic;



namespace SharedLibrary.DL
{
	public class Tax : IDetail
	{

		[PrimaryKey, AutoIncrement]
		public int		ID { get; set; }

		[Indexed(Name = "TaxIndex", Order = 2, Unique = true)]
		public string Name { get; set; }
		public string Description { get; set; }
		public decimal Percentage { get; set; }
		public bool    Required { get; set; }
		[Indexed(Name = "TaxIndex", Order = 1, Unique = true)]
		public int     PropertyID { get; set; }
		[Indexed]
		public string	RemoteID { get; set;}

		[Ignore]
		public decimal Amount { 
			get{ return 0.00m;}
			set{ }
		}

		[Ignore]
		public decimal PerNightAmount { 
			get { return 0.00m; }
			set{ }
		}

		[Ignore]
		public bool Refundable { 
			get { return false; }
			set{ }
		}

		[Ignore]
		public bool Taxable {
			get {return false;}
			set{ }
		}
		public Tax(){}

		public Tax(int propertyID)
		{
			PropertyID = propertyID;
		}
		public void Save()
		{
			if (PropertyID == 0) {
				throw new VsPropertyIDMisingException ("PropertyID must be set to a valid property");
			}

			if (ID == 0)
				Repository.DB.Insert (this);
			else
				Repository.DB.Update (this);
		}

//		public void Update(){
//			if (PropertyID == 0) {
//				throw new VsPropertyIDMisingException ("PropertyID must be set to a valid property");
//			}
//
//			Repository.DB.Update (this);
//		}

		public void Delete(int id) {
			if (id != 0) {
				Repository.DB.Delete<Tax> (id);
			}
		}

		public void Delete() {
			if (this.ID != 0) {
				Repository.DB.Delete (this);
			}
		}


		public static void DeleteAll(int propertyID)
		{
			var records = Taxes (propertyID);
			foreach (Tax r in records)
			{
				r.Delete ();
			}
		}


		public static Tax Find(int id )
		{

			var record = Repository.DB.Find<Tax> (id);
			if (record == null)
				throw new ArgumentException ("ID " + id +" is invalid for a Tax");
			return record;
		}

		public static Tax FindByName(int propertyID, string name)
		{
			var taxes = Repository.DB.Query<Tax> ("select * from Tax where PropertyID = ? and name = ?", propertyID, name);
//			Console.WriteLine ("Found {0} taxes first id = {1} - {2}", taxes.Count, taxes [0].ID, taxes[0].PropertyID);
			return (taxes.Count == 0) ? null : taxes [taxes.Count - 1];
		}

		public static List<Tax> Taxes(int propertyID)
		{
			return Repository.DB.Query<Tax> ("select * from Tax where PropertyID = ?", propertyID);
		}

		public  static int Count(int propertyID)
		{
			var command = Repository.DB.CreateCommand("select count(*) from Tax where PropertyID = ?", propertyID);
			var count = command.ExecuteScalar<int>();
//				Console.WriteLine ("Tax.Count returning {0}", count);
			return count;
		}
		
		[Ignore]
		public string PercentageString {
			get { return (Percentage * 100).ToString ("F");}
			set { Percentage = Decimal.Parse(value, NumberStyles.Currency) / 100;}
		}
		
		[Ignore]
		public string PerNightAmountString {
			get {return PerNightAmount.ToString ("F");}
			set {PerNightAmount = Decimal.Parse(value, NumberStyles.Currency); }
		}

		[Ignore]
		public string AmountString {
			get { return Amount.ToString ("F"); }
			set { Amount = Decimal.Parse (value, NumberStyles.Currency); }
		}

		public decimal Calculate(decimal taxableAmount)
		{
			return Math.Round(Percentage * taxableAmount, 2);
		}


	}
}

