using System;
using SQLite;
using SharedLibrary.BL.Contracts;
using SharedLibrary.DAL;



namespace SharedLibrary
{
	public class DatabaseVersion
	{
		[PrimaryKey, AutoIncrement]
		public int ID{ get; set;}
		public int Version { get; set;}

		public DatabaseVersion ()
		{

		}
		public static int Count
		{
			get {
				var command = Repository.DB.CreateCommand("select count(*) from DatabaseVersion");
				var count = command.ExecuteScalar<int>();
//				Console.WriteLine ("Count returning {0}", count);
				return count;
			}
		}

		public static DatabaseVersion Find(int id)
		{
			var record = Repository.DB.Find<DatabaseVersion> (id);
			if (record == null)
				throw new ArgumentException ("Database Manager record does not exist");
			return record; 
		}

		public void Save()
		{
			if (ID == 0)
				Repository.DB.Insert (this);
			else
				Repository.DB.Update (this);
		}

	}
}

