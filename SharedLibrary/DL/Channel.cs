using System;
using System.Collections.Generic;
using SharedLibrary.DAL;
using SQLite;

namespace SharedLibrary.DL
{

	public class Channel
	{
		[PrimaryKey, AutoIncrement]
		public int      ID { get; set; }

		public string   Name { get; set; }
		public string	Phone { get; set; }
		public string	Url { get; set; }
		public string	AdminUrl { get; set; }

		[Unique]
		public string	ChannelCode { get; set; }

		[Indexed]
		public string	RemoteID { get; set;}

		public static Channel Find(int id)
		{
			var chan = Repository.DB.Find<Channel> (id);
			return chan;
		}

		public static Channel FindByChannel(string channelCode)
		{
			var result = Repository.DB.Query<Channel> ("select * from Channel where ChannelCode = ?", channelCode);
			if (result.Count > 0) {
				return result [0];
			}
			return null;
		}

		public int Save()
		{
			return Repository.DB.Insert (this);
		}

		public void Update()
		{
			Repository.DB.Update (this);
		}

		public void Delete(int id) {
			if (id != 0) {
				Repository.DB.Delete<Channel> (id);
			}
		}

		public void Delete() {
			if (this.ID != 0) {
				Repository.DB.Delete (this);
			}
		}

		public static int Count
		{
			get {
				var command = Repository.DB.CreateCommand("select count(*) from Channel");
				var count = command.ExecuteScalar<int>();
//				Console.WriteLine ("There are {0} Channels", count);
				return count;
			}
		}

		public static List<Channel> Channels()
		{
			return Repository.DB.Query<Channel> ("select * from Channel");
		}



	}

}


