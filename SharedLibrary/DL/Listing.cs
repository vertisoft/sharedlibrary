using System;
using System.Collections.Generic;
using SharedLibrary.DAL;
using SQLite;

namespace SharedLibrary.DL
{
	public class Listing
	{
		[PrimaryKey, AutoIncrement]
		public int 		ID { get; set;}

		[Indexed(Name = "ListingID", Order = 3, Unique = true)]
		public string 	ListingNumber { get; set; }
		[Indexed(Name = "ListingID", Order = 2, Unique = true)]
		public string 	ChannelCode	{ get; set; }
		public string   ChannelName { get; set;}
		public string	UserName{ get; set; } // not used in basic version
		public string	Password{ get; set; } // not used in basic version
		[Indexed(Name = "ListingID", Order = 1, Unique = true)]
		public int		PropertyID{ get; set; }
		[Indexed]
		public string	RemoteID { get; set;}

		public Listing(){}

		public Listing(int propertyID)
		{
			PropertyID = propertyID;
		}

		/// <summary>
		///	Save() saves the current Listing. PropertyID, ChannelCode, and ListingNumber together must be unique.
		/// </summary>
		/// 
	    /// <exception cref="ArgumentException">
		/// Throws ArgumentException if a listing already exists with the same combined PropertyID, ChannelCode and ListingNumber.
		/// </exception>
		public void Save()
		{
			if (PropertyID == 0) {
				throw new Exception ("PropertyID must be set to a valid property.id");
			}

			if (ID > 0) 
				Repository.DB.Update (this);
			else
				Repository.DB.Insert (this);

		}

		
		public static Listing Find(int id )
		{
			var record = Repository.DB.Find<Listing> (id);
			if (record == null)
				throw new ArgumentException ("Listing ID " + id +" is invalid");
			return record;
		}


		/// <summary>
		///	Update() Called by Save to update the current Listing verifying that ChannelCode, and ListingNumber together are unique. This is called if the ID has a value other than 0. We use the combination of the 
		/// ListingNumber and ChannelCode to determine the property the inquiry is for.
		/// </summary>
		/// 
		/// <exception cref="ArgumentException">
		/// Throws ArgumentException if a listing already exists with the same combined ChannelCode and ListingNumber.
		/// </exception>
//		void Update(){

//			var listings = Repository.DB.Query<Listing> ("select * from Listing where ChannelCode = ? and ListingNumber = ?",  ChannelCode, ListingNumber);
//			if (listings.Count == 1 && listings [0].ID != ID ) { // There is a listing with the same information. If same ID, the record is being updated with no change to the ChannelCode or ListingNumber
//				throw new ArgumentException ("Channel Code and Listing Number already exists");
//			}
//
//			Repository.DB.Update (this);
//		}

		/// <summary>
		///	Deletes the listing specified by the ID.
		/// </summary>
		/// 
		/// /// <param name="id">The specific listing ID that is to be deleted.</param>
		public static void Delete(int id) {
			if (id != 0) {
				Repository.DB.Delete<Listing> (id);
			}
		}

		public void Delete() {
			if (this.ID != 0) {
				Repository.DB.Delete (this);
			}
		}


		public static void DeleteAll(int propertyID)
		{
			var records = ListingsByProperty (propertyID);
			if (records == null)
				return;
			foreach (Listing r in records)
			{
				r.Delete ();
			}
		}

		public static List<Listing> ListingsByProperty(int propertyID)
		{
			return Repository.DB.Query<Listing> ("Select * from Listing where PropertyID = ?", propertyID);
		}


		public static int Count
		{
			get {
				var command = Repository.DB.CreateCommand("select count(*) from Listing");
				var count = command.ExecuteScalar<int>();
//				Console.WriteLine ("Listing.Count returning {0}", count);
				return count;
			}
		}

		public static List<Listing> All()
		{
			return Repository.DB.Query<Listing> ("Select * from Listing");
		}

		public static Listing FindByCodeAndNumber (string channelCode, string number) 
		{
//			Console.WriteLine ("Looking for [{0}/{1}]", channelCode, number);
			var listings =  Repository.DB.Query<Listing> ("select * from Listing where ChannelCode = ? and ListingNumber = ?", channelCode, number);
			if(listings.Count == 1)
			{
				return listings [0];
			}
			return null;

		}

		private bool AlreadyExists(string channelCode, string listingNumber) 
		{
			var command = Repository.DB.CreateCommand("select Count(*) from Listing where ChannelCode = ? and ListingNumber = ?", channelCode, listingNumber);
			var count = command.ExecuteScalar<int>();
//			Console.WriteLine ("Listing for {0}/{1}", count, channelCode, listingNumber);
			if (count > 0) {
				return true;
			}
			return false;
		}
	}
}
// Change to           1/V/5     1/V/6      1/H/2     1/H/4    1/V/4
// Existing 1/V/4     T/T/F/U   F/T/T/X	   T/F/F/U   T/F/T/X  T/T/T/U
// Other1   1/V/5
// Other2   2/V/6
// Other3   2/H/4

