using System;
using System.Collections.Generic;
using SharedLibrary.DAL;
using SharedLibrary.LIB;
using SharedLibrary.DL;

using SQLite;





namespace SharedLibrary.DL
{
	/// <summary>
	///	A Quote is requested by the user. Usually a standard quote is generated first. There can be only one active quote
	/// related to an inquiry. If a Quote has been Delivered to the requestor, it may not be deleted.
	/// 
	/// Requestor information is in the inquiry. An inquiry, email or manual is required by the system.
	/// 
	/// </summary>
	/// 
	public class Quote : BL.Contracts.BusinessEntityBase
	{

		[Indexed]
		public int		 InquiryID { get; set;}

		[Indexed]
		public int 		 PropertyID { get; set;}

		public decimal	 BaseRental { get; set;}
		public decimal	 ExtraGuestsCharge { get; set; }
		public decimal	 Taxable { get; set;}
		public decimal 	 NonTaxable { get; set; }
		public decimal	 TotalTaxes { get; set;}
		public decimal	 TotalFees { get; set;}
		public decimal 	 TotalDiscounts { get; set;}
		public decimal 	 TotalDeposits { get; set;}
		public decimal 	 TotalCharge { get; set;}
		public string 	 CommentResponse { get; set;}
		public bool		 Delivered { get; set;}
		public string	 CurrentTemplate{ get; set;}
		public int		 MinNights{ get; set;}


		public decimal 	 Payment1 { get; set;}			//Pro
		public decimal 	 Payment2 { get; set;}			//Pro
		public decimal 	 Payment3 { get; set;}			//Pro
		public DateTime? Payment1Due { get; set;}		//Pro
		public DateTime? Payment2Due { get; set;}		//Pro
		public DateTime? Payment3Due { get; set;}		//Pro
		public string	 MyComments { get; set;}        //Pro
		[Indexed]
		public string	 RemoteID { get; set;}

		private Inquiry inquiry { get; set;}
		private Property _property;
		private Property property { 
			get { 
				if (_property == null)
					_property = Property.Find (PropertyID);
				return _property;
			} 
		}

		private Template _template;
		public Template Template{

			get{

				if (_template == null ) {
//
//					var templates = property.Templates ();
//					Console.WriteLine ("{0} templates", templates.Count);
//					Console.WriteLine ("Template {0} found = {1}", templates [0].Name, templates [0].Content);
					_template = property.TemplateByName (CurrentTemplate);
					if (_template == null)
						return new Template(property.ID);
					return _template;
				}
				if(_template.Name != CurrentTemplate) {
					_template = property.TemplateByName (CurrentTemplate);
					return _template;
				}
				return _template;
			}
		}

		[Ignore]
		public decimal DiscountedRental {
			get {return (BaseRental + ExtraGuestsCharge - TotalDiscounts);}
		}


		public Quote()
		{

		}

		public Quote(Inquiry inq)
		{
			inquiry = inq;
			InquiryID = inq.ID;
			PropertyID = inq.PropertyID;
			_property = Property.CurrentProperty;

//			Console.WriteLine ("PropertyID = {0}", PropertyID);
		}

		public List<QuoteDetail> Details ()
		{
			return QuoteDetail.All (ID);
		}

		public List<QuoteDetail> DetailsByType (string detailType)
		{
			return QuoteDetail.DetailsByType (ID, detailType);
		}

		public QuoteDetail NewDetail(string detailType)
		{
			var detail = new QuoteDetail ();
			detail.QuoteID = ID;
			detail.DetailType = detailType;
			return detail;
		}


		public void DeleteAllDetail()
		{
			var details = Details ();
			if (details == null)
				return;
			foreach(QuoteDetail q in details)
			{
				DeleteDetail (q);
			}
		}


		public void DeleteDetail(Object quoteDetail)
		{
			Repository.DB.Delete (quoteDetail);
		}

		public void Delete()
		{
			DeleteAllDetail ();
			Repository.DB.Delete (this);
		}

		public static void DeleteAll(int propertyID)
		{
			var quotes = Quotes (propertyID);
			if (quotes == null)
				return;
			foreach (Quote q in quotes)
			{
				q.DeleteAllDetail ();
				q.Delete ();
			}
		}

		public static List<Quote> Quotes(int propertyID)
		{
			return Repository.DB.Query<Quote> ("Select * from Inquiry where PropertyID = ?", propertyID);
		}

		public void Save()
		{
			if (InquiryID == 0)
				throw new VsIdMisingException ("InquiryID is required");

			if (ID == 0)
				Repository.DB.Insert (this);
			else
				Repository.DB.Update (this);
		}
		
		public static Quote StandardQuote(Inquiry inq)
		{

			var calculator = new QuoteCalculator (inq);

			var quote = calculator.Standard ();
			return quote;
		}


		public Quote Recalculate(bool includeBase = false)
		{
			if (inquiry == null) 
				inquiry = Inquiry.Find (InquiryID);
			var qc = new QuoteCalculator (this, inquiry);

			qc.Recalculate (includeBase);
			return this;
		}

		public static void Delete(int id){

			Repository.DB.Delete<Quote> (id);
		}


		public string DefaultTemplate(string name)
		{
			string templateName;

			var prop = inquiry.Property;

			if (name != null)
				templateName = name;
			else {
				var calcNights = (inquiry.DepartureDate - inquiry.ArrivalDate).TotalDays;

				if (MinNights > (inquiry.DepartureDate - inquiry.ArrivalDate).TotalDays)
					templateName = prop.MinimumNightsDocument;
				else if (inquiry.Availability == "A")
					templateName = prop.AvailableDocument;
				else
					templateName = prop.NotAvailableDocument;
			}
			return templateName;
		}



		public string ResponseContent()
		{
//			Console.WriteLine ("In ResponseContent = {0}", inquiry.CurrentQuote.TotalTaxes);
			var tc = new TemplateConverter(inquiry, null);
//			Template template = tc.FindTemplate (templateName);
			if (String.IsNullOrWhiteSpace (Template.Content))
			{
				return "";
			}

			return tc.Convert (Template.Content);
		}

		public string ResponseConvert(string text)
		{
			var tc = new TemplateConverter(inquiry, null);
			return tc.Convert (text);
		}

		public string ResponseSubject()
		{
			var tc = new TemplateConverter(inquiry, null);
			return tc.Convert (Template.Subject);
		}


		
		public static Quote Find(int id )
		{
			var record = Repository.DB.Find<Quote> (id);
			if (record == null)
				throw new ArgumentException ("Quote with ID " + id +" is invalid");
			record.inquiry = Inquiry.Find (record.InquiryID);
			return record;
		}

	}

	
}



