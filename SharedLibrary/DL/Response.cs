using System;
using SQLite;
using SharedLibrary;
using SharedLibrary.DAL;

using Mono.Security;

using System.Collections.Generic;


namespace SharedLibrary.DL
{
	public class Response: BL.Contracts.BusinessEntityBase
	{
		[Indexed]
		public int 		PropertyID{ get; set; }
		[Indexed]
		public int		InquiryID{ get; set;}
		public int		Type{ get; set;}
		public DateTime	DateSent{ get; set;}
		public string	SentTo{ get; set;}
		public string	Subject{ get; set;}
		public string	Body{ get; set;}
		[Indexed]
		public string   RemoteID{ get; set;}


		public Response ()
		{


		}

		public Response(int propertyID)
		{
			PropertyID = propertyID;
		}

		public void Save()
		{
			if (PropertyID == 0) {
				throw new Exception ("PropertyID must be set to a valid property.id");
			}

			if (ID != 0)
				Repository.DB.Update (this);
			else {

				Repository.DB.Insert (this);
			}
		}

		public static void Delete(int id) {
			if (id != 0) {
				Repository.DB.Delete<Response> (id);
			}
		}

		public void Delete() {
			if (ID != 0) {
				Repository.DB.Delete (this);
			}
		}

		public static void DeleteAll(int propertyID)
		{
			var records = Inquiry.Inquiries (propertyID);
			if (records == null)
				return;
			foreach(Inquiry r in records){
				r.Delete ();
			}
		}

		public static int Count(int propertyID)
		{
			var command = Repository.DB.CreateCommand("select count(*) from Response where ID = ?", propertyID);
			var count = command.ExecuteScalar<int>();
			return count;
		}


		public static Response Find(int id )
		{
			var record = Repository.DB.Find<Response> (id);
			if (record == null)
				throw new ArgumentException ("Response ID " + id +" is invalid");
			return record;
		}


		public static List<Response> Responses(int inquiryID)
		{
			return Repository.DB.Query<Response> ("Select * from Response where InquiryID = ?", inquiryID);
		}


		public static List<Response> AllResponses()
		{
			return Repository.DB.Query<Response> ("Select * from Response");
		}

		public Template.Types ResponseType
		{
			get {
				return (Template.Types)Type;
			}
			set {
				Type = (int)value;
			}
		}
	}
}


