using System;
using SQLite;
using SharedLibrary.DAL;
using System.Collections.Generic;
using SharedLibrary;
using System.Security.Permissions;
using System.Text.RegularExpressions;
using System.Resources;


namespace SharedLibrary.DL
{
	/// <summary>
	///	An Inquiry is received from a Listing Service or manually added and saved. If there is a quote attached, the inquiry may not
	/// be deleted, only archived. If nothing has been sent to the requester delete is fine. Suggest just ignoring it to 
	/// save the contact informtion
	/// </summary>
	/// 
	public class Inquiry : BL.Contracts.BusinessEntityBase
	{

		public enum InquiryState
		{
			New,
			Reserved,
			Responded,
			Tentative,
			Blocked,
			Ignored,
			Archived,
			Deleted,
			Canceled,
			Deposit,
			None,
			Unknown
		}




		public string 	GuestName { get; set;}
		public int 		Adults { get; set;}
		public int		Children { get; set;}

		[Indexed]
		public DateTime	ArrivalDate { get; set;}
		public DateTime DepartureDate { get; set;}
		public string	Comment	{ get; set;}
		public string	Phone { get; set;}
		public string	ListingNumber { get; set;}
		public string	Channel { get; set;}
		public string	ReceivedMethod { get; set;} //Direct or Forwarded. Not sure if needed
	//	public string	ListingID { get; set;}  // Not sure why this is needed
		public string	Email { get; set;}
		public string	ReplyTo{ get; set;}
		[Indexed]
		public string   MessageID{ get; set;}

		[Indexed]
		public DateTime	InquiryDate { get; set;}
		public DateTime ResponseTime { get; set;}

		[Indexed]

		public string	Status { get; set; }        // N = NEW, RES = Reserved R = RESPONDED, IGN = IGNORED, TEN = Tentative
		                                            // ARC = ARCHIVED, DEL =  Deleted, CR = Cancelled Reservation
		public int		State { get; set;}			// Current State of the inquiry Uses InquiryStatus and replaces Status
		public int		CurrentReservationState { get; set;}  // Current reservation status
		public string 	SourceDocument { get; set;} // The actual email inquiry Removed in Archive
		public string 	MyComments { get; set;}		// Pro
		public string	EventComments { get; set;}  // Pro Any special event being celebrated on this stay.
		public string	StayReason { get; set;}     // Pro WEDDING, ANNIV, GRAD, RETIRE, BIRTH, HONEY, VACAT
		public int		ReservationID{ get; set;}	// 0 if no reservation or reservation cancelled
		[Indexed]
		public int 		PropertyID{ get; set;}


		public int 		QuoteID { get; set;}  // The quote for this inquiry if there is one.  There may be one or none.

		[Indexed]
		public string RemoteID { get; set;}

		public Inquiry()
		{
			Status = "NEW";
			CurrentState = Inquiry.InquiryState.New;
			InquiryDate = DateTime.Now;
			ArrivalDate = DateTime.Now;
			DepartureDate = DateTime.Now;
			Channel = "Manual";
		}

		public Inquiry(int propertyID)
		{
			Status = "NEW";
			CurrentState = Inquiry.InquiryState.New;
			PropertyID = propertyID;
			InquiryDate = DateTime.Now;
			ArrivalDate = DateTime.Now;
			DepartureDate = DateTime.Now;
			Channel = "Manual";

		}

		private static Inquiry _currentInquiry;
		public static Inquiry CurrentInquiry{
			get { return _currentInquiry;}
			set { _currentInquiry = value;}
		}

		private  Quote _currentQuote;
		[Ignore]
		public  Quote CurrentQuote {
			get{
				if (_currentQuote == null) {
					if (ArrivalDate == DateTime.MinValue || DepartureDate == DateTime.MinValue) {
						return null;
					}
					if (QuoteID == 0) {
						StandardQuote ();  // Sets _currentQuote
					} else {
						try
						{
							_currentQuote = Quote.Find (QuoteID);
						}
						catch(ArgumentException ex)
						{
							_currentQuote = Quote.StandardQuote (this);
							QuoteID = _currentQuote.ID;
							this.Save ();
						}
//					Console.WriteLine ("Using an existing quote {0} -  {1}", _currentQuote.ID, _currentQuote.TotalFees);
					}
				}

				return _currentQuote;
			}
			set{_currentQuote = value;}
		}



		[Ignore]
		public string UseQuote
		{
			get{
				string avail = Availability;
				if (TotalNights < Property.CurrentProperty.MinNights)
					return Property.CurrentProperty.MinimumNightsDocument;
				if (avail == "T")
					return Property.CurrentProperty.TentativeDocument;
				if (avail == "R")
					return Property.CurrentProperty.NotAvailableDocument;
				if (avail == "B")
					return Property.CurrentProperty.AvailableDocument;
				return Property.CurrentProperty.AvailableDocument;
			}
		}

		private Property _property{ get; set; }

		[Ignore]
		public Property Property
		{
			get{
				if (_property == null)
					_property = Property.Find (PropertyID);
				return _property;}
		}

		[Ignore]
		public string Guests
		{
			get {
				string results = "";
				if (Adults == 1)
					results = "1 Adult";
				else
					results = String.Format ("{0} Adults", Adults);
				if (Children == 0)
					results += " and No Children";
				else if (Children == 1)
					results += " and 1 Child";
				else {
					results += String.Format (" and {0} Children", Children);
				}
				return results;
			}
		}


		public List<Response> Responses
		{
			get{
				return Response.Responses (this.ID);
			}
		}

		public string NightsString(bool full)
		{
			var nights = TotalNights;
			string nightsString = "";
			if(full) {   // Return with proper singular/plural nights
				nightsString = Utils.NightsString (nights);
			}
			else {
				nightsString = nights.ToString ();
			}
			return nightsString;
		}


		
		public void Save()
		{
			if (PropertyID == 0) {
				throw new Exception ("PropertyID must be set to a valid property.id");
			}

			if (ID != 0)
				Repository.DB.Update (this);
			else {

				Repository.DB.Insert (this);
				Console.WriteLine ("saved inquiry {0}", ID);
			}
		}

		public static void Delete(int id) {
			if (id != 0) {
				Inquiry inq = Inquiry.Find (id);
				inq.Delete ();
			}
		}

		public void Delete() {
			if (ID != 0) {
				DeleteRelatedInfo (this);
				Repository.DB.Delete (this);
			}
		}

		public static void DeleteAll(int propertyID)
		{
			var records = Inquiry.Inquiries (propertyID);
			if (records == null)
				return;
			foreach(Inquiry r in records){
				r.DeleteRelatedInfo (r);
				r.Delete ();
			}
		}

		public static int Count
		{
			get {
				var command = Repository.DB.CreateCommand("select count(*) from Inquiry");
				var count = command.ExecuteScalar<int>();
				return count;
			}
		}

		
		public static Inquiry Find(int id )
		{
			var record = Repository.DB.Find<Inquiry> (id);
//			if (record == null)
//				throw new ArgumentException ("Inquiry ID " + id +" is invalid");
			return record;
		}

		public static Inquiry FindByMessageID(string id)
		{
			var inq = Repository.DB.Query<Inquiry> ("Select * from Inquiry where MessageID = ?", id);
			if (inq.Count == 0)
				return null;
			return inq [0];
		}

		public static List<Inquiry> InquiryFindByStatus(int id, InquiryState status ) // N = NEW, R = RESPONDED, I =IGNORED, DELETED (Possible)
		{

			return Repository.DB.Query<Inquiry> ("select * from Inquiry where PropertyId = ? and State = ?  order by datetime(inquirydate) desc", 
				id, (int) status);
		}

		public static List<Inquiry> InquiryFindByStatusSince(int id, List<InquiryState> status, int daysBack ) // N = NEW, R = RESPONDED, I =IGNORED, DELETED (Possible)
		{
			var startDate = DateTime.Now.AddDays (daysBack * -1);
			string since = String.Format("{0}-{1}-{2}", startDate.Year, startDate.Month.ToString ("D2"), startDate.Day.ToString ("D2"));
			string wanted = "";
			foreach (InquiryState s in status)
			{ 
				if (String.IsNullOrEmpty (wanted))
					wanted = String.Format ("{0}", (int)s);
				else
					wanted += String.Format (", {0}", (int)s);

			}

			var query = "select * from Inquiry where PropertyId = ? and State in (" + wanted + ") and inquirydate > ?  order by datetime(inquirydate) desc";
			return Repository.DB.Query<Inquiry> (query, id, since);
		}

		public static List<Inquiry> Inquiries(int propertyID)
		{
			return Repository.DB.Query<Inquiry> ("Select * from Inquiry where PropertyID = ?", propertyID);
		}

		public bool IsValid()
		{
			if (ArrivalDate == DateTime.MinValue || DepartureDate == DateTime.MinValue)
				return false;
			return true;
		}

		public Reservation Reserve(Reservation.Availability type)
		{
			if (this.ID == 0) {
				this.Save ();
			}
			this.SetReservationStateByReservationType ( type);	
			var reserv = new Reservation(this, type);
			reserv.Save ();
			this.ReservationID = reserv.ID;
			this.Save ();
			return reserv;
		}

		public Quote StandardQuote()
		{
			if (QuoteID > 0)
				Quote.Delete (QuoteID);
			var quote = Quote.StandardQuote(this);
			QuoteID = quote.ID;
			_currentQuote = quote;
			Save();
			return quote;
		}



		public Quote SaveNewQuote(Quote q)
		{
			if (q.ID == 0)
				q.Save ();

			if (QuoteID > 0)
				Quote.Delete (QuoteID);
			return q;
		}

//TODO Is this required All quotes start as standard quote
		public Quote NewQuote()
		{
			var quote = new Quote (this);

			return quote;
		}

		[Ignore]
		public double  TotalNights {
			get {
				if (ArrivalDate == DateTime.MinValue || DepartureDate == DateTime.MinValue)
					return 0;
				return DepartureDate.Subtract (ArrivalDate).TotalDays;
			}
			//set;
		}

		[Ignore]
		public string Availability { // A = AVAILABLE, T = TENTATIVE, R = RESERVED, B = BLOCKED
			get {
				string rev;
				var resv = Reservation.CheckAvailability (PropertyID, ArrivalDate, DepartureDate);
				if (resv == null)
					rev = "A";
				else
					rev = resv.Type;
				return rev;
			}
		}
		[Ignore]
		public Reservation.Availability AvailabileStatus { // The new InquiryState enum
			get {
				Reservation.Availability rev;
				var resv = Reservation.CheckAvailability (PropertyID, ArrivalDate, DepartureDate);
				if (resv == null)
					rev = Reservation.Availability.Available;
				else
					rev = resv.CurrentState;
				return rev;
			}
		}

		public static List<Inquiry> InquiryByName(int propertyID, string value)
		{
			if (value == null)
				return null;
			if (value == "*")
				return Repository.DB.Query<Inquiry> ("Select * from Inquiry where PropertyID = ?", propertyID);

			string name = "%" + Regex.Replace (value, @"\(\'", "") + "%";

			return Repository.DB.Query<Inquiry> ("Select * from Inquiry where PropertyID = ? and guestname like ?", propertyID, name);
		}

		[Ignore]
		public InquiryState CurrentState
		{
			get{
				if (State == null)
					State = (int)OldToNewState (Status);
				return (InquiryState)State;
			}
			set{
				State = (int)value;
				Status = NewToOldState (value);
			}

		} 

		public void SetCurrentStateByReservationType(Reservation.Availability value)
		{
			switch(value){
			case Reservation.Availability.Blocked:
				CurrentState = InquiryState.Blocked;
				break;
			case Reservation.Availability.Reserved:
				CurrentState = InquiryState.Reserved;
				break;
			case Reservation.Availability.Tentative:
				CurrentState = InquiryState.Tentative;
				break;
			default:
				CurrentState = InquiryState.Canceled;
				break;
			}
		}
		public void SetReservationStateByReservationType(Reservation.Availability value)
		{
			ReservationState = value;
//			switch(value){
//			case Reservation.Availability.Available:
//				ReservationState = Inquiry.InquiryState.None;
//				break;
//			case Reservation.Availability.Blocked:
//				ReservationState = InquiryState.Blocked;
//				break;
//			case Reservation.Availability.Reserved:
//				ReservationState = InquiryState.Reserved;
//				break;
//			case Reservation.Availability.Tentative:
//				ReservationState = InquiryState.Tentative;
//				break;
//			default:
//				ReservationState = InquiryState.Canceled;
//				break;
//			}
		}

		private string NewToOldState(InquiryState value)
		{
			string state = "N";
			switch(value) 
			{
			case InquiryState.New:
				Status = "N";
				break;
			case InquiryState.Reserved:
				Status = "RES";
				break;
			case InquiryState.Responded:
				Status = "R";
				break;
			case InquiryState.Ignored:
				Status = "IGN";
				break;
			case InquiryState.Tentative:
				Status = "TEN";
				break;
			case InquiryState.Canceled:
				Status = "Canceled";
				break;
			}
			return state;
		}

		private InquiryState OldToNewState(string value)
		{
			InquiryState state = InquiryState.Unknown;
			switch(Status) 
			{
				case "N":
					state = InquiryState.New;
					break;
				case "RES":
					state = InquiryState.Reserved;
					break;
				case "R":
					state = InquiryState.Responded;
					break;
				case "IGN":
					state = InquiryState.Ignored;
					break;
				case "TEN":
					state = InquiryState.Tentative;
					break;
				case "Canceled":
					state = InquiryState.Canceled;
					break;
			}
			return state;
		}


		public string InquiryStateString {

			get {
				return CurrentState.ToString ();
			}
		}

		[Ignore]
		public string AdultsString{
			get { return Adults.ToString();}
			set {Adults = Utils.ParseInt(value);}
		}

		[Ignore]
		public string ChildrenString {
			get {return Children.ToString();}
			set {Children = Utils.ParseInt(value);}
		}

		[Ignore]
		public static List<InquiryState> RespondedStates{
			get {
				var list = new List<InquiryState> ();
				list.Add (InquiryState.Reserved);
				list.Add (InquiryState.Responded);
				list.Add (InquiryState.Tentative);
				list.Add (InquiryState.Canceled);

				return list;

			}
		}

		[Ignore]
		public Reservation.Availability ReservationState
		{
			get{
				return (Reservation.Availability)CurrentReservationState;
			}
			set{
				CurrentReservationState = (int)value;
			}
		}


		private void DeleteRelatedInfo(Inquiry inq){
			var rev = Reservation.FindByInquiry (inq.ID);
			if (rev != null){
				rev.Delete ();
			}
			if (inq.CurrentQuote != null)
				inq.CurrentQuote.Delete();
			var resps = Response.Responses (inq.ID);
			if(resps != null){
				foreach (Response r in resps)
					r.Delete ();
			}
		}

	}
}


