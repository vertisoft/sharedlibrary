using System;
using SharedLibrary.DAL;
using SQLite;

namespace SharedLibrary.DL
{
	public class Config
	{
		[PrimaryKey]
		public int 		ID { get; set; }

		public int 		DemoPropertyID { get; set; }	// Either a valid property ID or 0 if Demo turned off
		public string 	Pin { get; set; }
		public bool 	PinRequired { get; set; }
		public string   Name { get; set; }
		public string	VrWizardName{ get; set;}		// VrWizard Website login
		public string	VrWizardPin{ get; set; }		// VrWizard WebSite PIN
		public string	AppDBVersion{ get; set;}		// keep track of current version of the db

		public Config()
		{

			AppDBVersion = "1.0";

		}
		public static Config Get()
		{
			var config = Repository.DB.Find<Config> (1);
			if(config == null){
				config = new Config ();
			}
			return config;
		}

		public void Save()
		{

			if (ID == 0)
				Repository.DB.Insert (this);
			else 
				Repository.DB.Update (this);
		}
	}
}

