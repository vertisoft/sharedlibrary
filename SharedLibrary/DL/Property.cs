using System;
using System.Collections.Generic;
using SharedLibrary;
using SharedLibrary.DAL;
using SharedLibrary.LIB;

//using RestSharp;
//using RestSharp.Deserializers;
using SQLite;
//using System.Runtime.InteropServices.ComTypes;

namespace SharedLibrary.DL
{
		
	public class Property
	{
		[PrimaryKey, AutoIncrement]
		public int     ID { get; set; }
		public string  ServerID { get; set;} // VrWizard server id for inquiries

// General information
		public string  Name { get; set; }
		public string  ShortName { get; set; }
		public string  Address1 { get; set; }
		public string  Address2 { get; set; }
		public string  City { get; set; }
		public string  State { get; set; }
		public string  Zip { get; set; }
		public string  Country { get; set; }
		public string  About { get; set; }   // Description to avoid Apple reserved word
		public string  ShortDescription { get; set; }
		public string  ComplexName { get; set; }
		public string  Summary { get; set; }
//		public string  Area { get; set; }
		public int     Sleeps { get; set; }
		public bool    IsDemo { get; set; }
		public bool	   IsActive { get; set;} // show property on main screen and combined inquiry list

// Emails/ contact
		public string  ResponseEmail { get; set; }
		public string  RejectEmail { get; set; }
		public string  CopyEmail { get; set; }
		public string  BCCEmail { get; set; }
		public string  Voice { get; set; }
		public string  Fax { get; set; }

//Default Prices
		public string  WeekendDays { get; set; }   // "MON,TUE, WED, THU, FRI, SAT, SUN" Weekend Days are returned.
		public decimal Weekend { get; set; }
		public decimal Weekday { get; set; }
		public decimal Weekly { get; set; }
		public decimal Monthly { get; set; }
		public decimal ExtraGuestRate { get; set; }
		public int     RatePeople { get; set; }
		public int     MinNights { get; set; }

//Default Documents
		public string  AvailableDocument { get; set; }
		public string  TentativeDocument{ get; set;}
		public string  NotAvailableDocument { get; set; }
		public string  MinimumNightsDocument{ get; set;}
		public string  WelcomeDocument { get; set; }
		public string  ThankYouDocument{ get; set;}
		public string  ContractDocument { get; set; }
		public string  NoticeToCleanDocument{ get; set;}
		public string  ReservationNoticeDocument{ get; set; } // document to sendto cleaner and others regarding a new reservton

// Security Codes to open doors gates
		public string  SecurityCode1 { get; set; }
		public string  SecurityCode2 { get; set; }
		public string  SecurityCode3 { get; set; }

// Urls of property sites
		public string  Url1 { get; set; }
		public string  Url2 { get; set; }

// Server uri for property
		public string  Uri { get; set; }  // Used in pro for auto response Maybe
		[Indexed]
		public string	RemoteID{ get; set;}

		public Property()
		{
			IsDemo = true;
		}



		private static Property _currentProperty;

		public static Property CurrentProperty{
			get {return _currentProperty;}
			set {_currentProperty = value;}
		}

		public static int CurrentPropertyID{
			get {return _currentProperty.ID;}
			set {_currentProperty = Find(value);}
		}

		public static bool ValidCurrentProperty{
			get { 
				if (_currentProperty == null || _currentProperty.ID == 0)
					return false;
				else
					return true;
			}
		}


		private List<Word> _reservedWords;

		[Ignore]
		public List<Word> ReservedWords
		{
			get{ 
				if(_reservedWords == null)
					_reservedWords = ReservedWord.ReservedWordsSorted(ID);
				return _reservedWords;
			}
			set {
				_reservedWords = null;  //force the regeneration on next request
			}
		}
	                                  

		public static Property DemoProperty()
		{

			var props = Repository.DB.Query<Property> ("select * from property where IsDemo = 1");
			Property prop = null;

			if (props.Count > 0) {
//				Console.WriteLine ("Demo Property exists");
				prop = props [0];
			} else {
//				Console.WriteLine ("No Demo Property");
				_currentProperty = prop;
			}
			return prop;
		}



		public static Property ReCreateDemoProperty()
		{
//			Console.WriteLine ("Recreate Demo Property");
			var props = Repository.DB.Query<Property> ("select * from property where IsDemo =  1");
			if (props.Count > 0)
				foreach (Property p in props)
					p.Delete ();
			var prop = CreateDemo ();
			_currentProperty = prop;
			return prop;
		}

		// 		Creates a new Demo Property
		static Property CreateDemo()
		{
			return DemoCreator.Create ();
		}

		public static void Delete(int id)
		{
			// Delete each of the linked elements;
			var prop = Property.Find (id);
			prop.DeleteAllSupporting (id);
			Repository.DB.Delete<Property> (id);
		}

		public void Delete()
		{
			if (ID != 0) {
				DeleteAllSupporting (ID);
				Repository.DB.Delete (this);
			}
		}

		private void DeleteAllSupporting(int propertyID)
		{
			Fee.DeleteAll (propertyID);
			Deposit.DeleteAll (propertyID);
			Discount.DeleteAll (propertyID);
			Inquiry.DeleteAll (propertyID);
			Listing.DeleteAll (propertyID);
			try {
				MailServer.DeleteAll (propertyID);
			}
			catch (ArgumentException ex){
//				Console.WriteLine ("Ignoring the MailServer not set up.");
			}
			Quote.DeleteAll (propertyID);
			Rate.DeleteAll (propertyID);
			Reservation.DeleteAll (propertyID);
			ReservedWord.DeleteAll (propertyID);
//			Response.DeleteAll (propertyID);
//			Task.DeleteAll (propertyID);
			Tax.DeleteAll (propertyID);
			Template.DeleteAll (propertyID);
		}

		public static Property Find(int id )
		{

			var prop = Repository.DB.Find<Property> (id);
			if (prop == null)
				throw new ArgumentException ("ID " + id +" is invalid");

//			Console.WriteLine ("Find = {0} - {1}", id, prop.ID);
			_currentProperty = prop;
			return prop;
		}

		// Inorder to set the CurrentProperty a find must be done on the desired property

		public static List<Property> Properties()
		{
//			DemoProperty ();
//			return Repository.DB.Query<Property> ("select * from Property where IsDemo != ?",1);
			return Repository.DB.Query<Property> ("select * from Property");
		}

		public Property Save()
		{
			if (ID == 0)
				Repository.DB.Insert (this);
			else
				Repository.DB.Update (this);
			_currentProperty = this; // In case the property has never been saved it becomes the current property
//
//			if (_reservedWords == null)
//				ReservedWord.ReservedWords (ID);
			return this;
		}

		public static Property RealProperty
		{
			get {
				var properties = Repository.DB.Query<Property>("select * from Property where IsDemo = ?", 0);
				if (properties.Count == 0)
					return null;
				Property.CurrentProperty = properties [0];
				return CurrentProperty;
			}
		}

		public static int Count
		{
			get {
				var command = Repository.DB.CreateCommand("select count(*) from Property", 0);
				var count = command.ExecuteScalar<int>();
//				Console.WriteLine ("Count says there are {0} Total Properties", count);
				command = Repository.DB.CreateCommand("select count(*) from Property where IsDemo = ?", 0);
				count = command.ExecuteScalar<int>();
//				Console.WriteLine ("Count says there are {0} Real Properties", count);
				return count;
			}
		}

		public static int DemoCount
		{
			get {
				var command = Repository.DB.CreateCommand("select count(*) from Property where IsDemo = ?", 1 );
				var count = command.ExecuteScalar<int>();
//				Console.WriteLine ("DemoCount says there are {0} Demo Properties", count);
				return count;

			}
		}

		// Fee Handlers
		public List<Fee> Fees()
		{
			object id = ID;
			var fees = Repository.DB.Query<Fee> ("select * from fee where PropertyID = ?", id);
			return fees;
		}

		public Fee NewFee()
		{
			var fee = new Fee (ID);
			return fee;
		}

		[Ignore]
		public int FeeCount
		{
			get {
				var command = Repository.DB.CreateCommand("select count(*) from Fee where PropertyId = ?", ID );
				var count = command.ExecuteScalar<int>();
//				Console.WriteLine ("Property {0} has {1} Fees", ID, count);
				return count;
			}
		}

		// Deposit Handlers 

		public Deposit NewDeposit()
		{
			var dep = new Deposit (ID);
			return dep;
		}
		
		public List<Deposit> Deposits()
		{
			object id = ID;
			var deposits = Repository.DB.Query<Deposit> ("select * from deposit where PropertyID = ?", id);
			return deposits;
		}

		[Ignore]
		public int DepositCount
		{
			get {
				var command = Repository.DB.CreateCommand("select count(*) from Deposit where PropertyId = ?", ID );
				var count = command.ExecuteScalar<int>();
//				Console.WriteLine ("Property {0} has {1} Deposits", ID, count);
				return count;
			}
		}

		// ReservedWord Handlers

		public ReservedWord NewReservedWord()
		{
			var rw = new ReservedWord (ID);
			rw.WordType = "C";  // default to Custom
			return rw;
		}

		public List<ReservedWord> CustomReservedWords()
		{
			object id = ID;
			var reservedWords = Repository.DB.Query<ReservedWord> ("select * from reservedword where PropertyID = ? and WordType = 'C'", id);
			return reservedWords;
		}

		[Ignore]
		public int ReservedWordCount
		{
			get {
				var command = Repository.DB.CreateCommand("select count(*) from ReservedWord where PropertyId = ?", ID );
				var count = command.ExecuteScalar<int>();
//				Console.WriteLine ("Property {0} has {1} ReservedWords", ID, count);
				return count;
			}
		}

		// Tax Handlers

		public Tax NewTax()
		{
			var tax = new Tax (ID);	
			return tax;
		}

		public List<Tax> Taxes()
		{
			object id = ID;
			var taxes = Repository.DB.Query<Tax> ("select * from tax where PropertyID = ?", id);
			return taxes;
		}

		[Ignore]
		public int TaxCount
		{
			get {
				var command = Repository.DB.CreateCommand("select count(*) from Tax where PropertyId = ?", ID );
				var count = command.ExecuteScalar<int>();
//				Console.WriteLine ("Property {0} has {1} taxes", ID, count);
				return count;
			}
		}

		//Discount Handlers

		public Discount NewDiscount()
		{
			var disc = new Discount (ID);	
			return disc;
		}

		public List<Discount> Discounts()
		{
			object id = ID;
			return Repository.DB.Query<Discount> ("select * from Discount where PropertyID = ?", id);
		}


		public int DiscountCount
		{
			get {
				var command = Repository.DB.CreateCommand("select count(*) from Discount where PropertyId = ?", ID );
				var count = command.ExecuteScalar<int>();
//				Console.WriteLine ("Property {0} has {1} Discounts", ID, count);
				return count;
			}
		}

		// Listing Handlers
				
		public Listing NewListing()
		{
			var list = new Listing (ID);	
			return list;
		}

		public List<Listing> Listings()
		{
			return Repository.DB.Query<Listing> ("select * from Listing where PropertyID = ?", ID);
		}

		public Listing ListingForChannel(string code)
		{
			var listings = Repository.DB.Query<Listing> ("select * from Listing where PropertyID = ? and ChannelCode = ?", ID, code);
			if (listings.Count == 0)
				return null;
			else
				return listings [0];
		}


		[Ignore]
		public int ListingCount
		{
			get {
				var command = Repository.DB.CreateCommand("select count(*) from Listing where PropertyId = ?", ID );
				var count = command.ExecuteScalar<int>();
//				Console.WriteLine ("Property {0} has {1} Listings", ID, count);
				return count;
			}
		}

		// Rate Handlers

		public Rate NewRate()
		{
			var rate = new Rate (ID);	
			return rate;
		}

		public List<Rate> Rates()
		{
			return Repository.DB.Query<Rate> ("select * from Rate where PropertyID = ?", ID);
		}
		
		public List<Rate> RatesByStartDate()
		{
			return Repository.DB.Query<Rate> ("select * from Rate  where PropertyID = ? order by DateTime(StartDate)", ID);
		}

		[Ignore]
		public int RateCount
		{
			get {
				var command = Repository.DB.CreateCommand("select count(*) from Rate where PropertyId = ?", ID );
				var count = command.ExecuteScalar<int>();
//				Console.WriteLine ("Property {0} has {1} Rates", ID, count);
				return count;
			}
		}

		public int RateTypeCount(string rateType)
		{
			var command = Repository.DB.CreateCommand("select count(*) from Rate where PropertyId = ? and RateType = ?", ID, rateType);
			var count = command.ExecuteScalar<int>();
			//				Console.WriteLine ("Property {0} has {1} Rates", ID, count);
			return count;
		}

		// Inquiry Handlers

		public Inquiry NewInquiry()
		{
			return new Inquiry (ID);
		}

		public List<Inquiry> Inquiries()
		{
			return Repository.DB.Query<Inquiry> ("select * from Inquiry where PropertyID = ? order by datetime(inquirydate) desc", ID);
		}

		public List<Inquiry> InquiriesByName(string name)
		{
			return Inquiry.InquiryByName (ID, name);
		}

		[Ignore]
		public int InquiryCount
		{
			get {
				var command = Repository.DB.CreateCommand("select count(*) from Inquiry where PropertyId = ?", ID );
				var count = command.ExecuteScalar<int>();
				return count;
			}
		}

		public List<Inquiry> InquirySqlQuery(string sql)
		{
			return Repository.DB.Query<Inquiry> (sql);
		}

		public List<Inquiry> InquiryFindByStatus(Inquiry.InquiryState status) // N = NEW, R = RESPONDED, I =IGNORED, DELETED (Possible)
		{
			return Inquiry.InquiryFindByStatus (ID, status);
//			return Repository.DB.Query<Inquiry> ("select * from Inquiry where PropertyId = ? and Status = ?  order by datetime(inquirydate) desc", 
//												ID, status);
		}


		public int InquiryCountByStatus(string status)
		{
				var command = Repository.DB.CreateCommand("select count(*) from Inquiry where PropertyId = ? and Status = ?", 
												ID, status );
				var count = command.ExecuteScalar<int>();
				return count;
		}

		// Reservation Handlers

		public Reservation NewReservation()
		{
			return new Reservation (ID);
		}

		public List<Reservation> Reservations()
		{
			return Repository.DB.Query<Reservation> ("select * from Reservation where PropertyID = ?", ID);
		}

		[Ignore]
		public int ReservationCount
		{
			get {
				var command = Repository.DB.CreateCommand("select count(*) from Reservation where PropertyId = ?", ID );
				var count = command.ExecuteScalar<int>();
				return count;
			}
		}

		// Template Handler
		public Template NewTemplate()
		{
			return new Template (ID);
		}


		public List<Template> Templates()
		{
			return Repository.DB.Query<Template> ("select * from Template where PropertyID = ? and Deleted = ?", ID, false);
		}

		public Template TemplateByName(string name)
		{
			var templates = Repository.DB.Query<Template> ("select * from Template where PropertyID = ? and Name = ?", ID, name);
			if (templates.Count == 0)
				return null;
			else
				return templates[0];   // only return the first as there should only be one
		}

		[Ignore]
		public int TemplateCount
		{
			get {
				var command = Repository.DB.CreateCommand("select count(*) from Template where PropertyId = ?", ID );
				var count = command.ExecuteScalar<int>();
				return count;
			}
		}

		// MailServer Handler
		public MailServer NewMailServer()
		{
			return new MailServer (ID);
		}


		[Ignore]
		public MailServer MailServer
		{
			get {
//				var count = MailServer.CountByProperty(ID);
//				Console.WriteLine ("there are {0} mail servers",count);
//				Console.WriteLine ("MailServer for property {0}, {1}", ID, Name);
				var records = Repository.DB.Query<MailServer> ("select * from MailServer where PropertyID = ?", ID);
				if (records.Count == 0)
					throw new ArgumentException ("No Mail Server set up!");
				return records [0];
			}
		}

	}
}




