using System;
using System.Collections.Generic;
using SQLite;
using SharedLibrary.BL;
using SharedLibrary.DAL;
using System.Linq;
using System.Reflection;
using System.Globalization;





namespace SharedLibrary.DL
{
	public class QuoteDetail : BL.Contracts.BusinessEntityBase
	{
		[Indexed]
		public int     	QuoteID { get; set;}
		public int		DetailID{ get; set;}		// if the id is present, it is a standard detail item, otherwise it is an adhoc item
		public string	DetailType{ get; set;}		// FEE, DIS, TAX, DEP
		public string	DetailName{ get; set;}
		public string	DetailDescription{ get; set;}  // Used in response text
		public decimal	DetailPercentage{ get; set;}
		public decimal	DetailAmount{ get; set;}
		public decimal	DetailPerNightAmount{ get; set;}
		public decimal	DetailCalculatedAmount{ get; set;}
		public bool		DetailTaxable{ get; set;}
		public bool		DetailRequired{ get; set;}
		public bool		DetailRefundable{ get; set;}
//		public bool		DetailPerNight{ get; set;}
		[Indexed]
		public string	RemoteID { get; set;}

		public QuoteDetail ()
		{
		}

		public QuoteDetail(int quoteID)
		{
			QuoteID = quoteID;
		}

		public void Save()
		{
//			Console.WriteLine ("-----------");
//			Console.WriteLine ("Save QuoteDetail {0} type {1}", this.DetailName, DetailType);
			if (QuoteID == 0)
				throw new VsIdMisingException ("  QuoteID is required");

			string[] validTypes = { "FEE", "DIS", "DEP", "TAX" };

			if (!validTypes.Contains (DetailType))
				throw new VsException ("  QuoteDetail must be of type 'FEE', 'DIS', 'DEP', or 'TAX' it is " + DetailType);

			if (ID == 0) {
//				Console.WriteLine ("  Insert QuoteDetail for quote {0} Amount =  {1}  Calculated = {2}", this.DetailName, this.DetailAmount, this.DetailCalculatedAmount);
				Repository.DB.Insert (this);
			}
			else {
//				Console.WriteLine ("  Update QuoteDetail for {0} Amount =  {1}  Calculated = {2}", this.DetailName, this.DetailAmount, this.DetailCalculatedAmount);
				Repository.DB.Update (this);
			}
//			Console.WriteLine ("  End Save {0} id = {1} for QuoteID = {2} Amount = {3}  Percentage = {4}  perNight  {5} Calculated = {6}", 
//			                   this.DetailName, ID, QuoteID, this.DetailAmount, this.DetailPercentage, this.DetailPerNightAmount, this.DetailCalculatedAmount);
//			Console.WriteLine ("-----------");
		}

		public void Delete() {
			Repository.DB.Delete (this);
			ID = 0;
		}

		public static List<QuoteDetail> All(int quoteID)
		{
			return Repository.DB.Query<QuoteDetail> ("select * from QuoteDetail where QuoteID = ?", quoteID);
		}

		public static List<QuoteDetail> DetailsByType(int quoteID, string detailType)
		{
			return Repository.DB.Query<QuoteDetail> ("select * from QuoteDetail where QuoteID = ? and DetailType = ?", quoteID, detailType);
		}

		public static QuoteDetail Find(int id )
		{

			var record = Repository.DB.Find<QuoteDetail> (id);
			if (record == null)
				throw new ArgumentException ("QuoteDetail ID " + id +" is invalid");
			return record;
		}

		
		[Ignore]
		public string DetailPercentageString {
			get { return (DetailPercentage * 100).ToString ("F");}
			set { DetailPercentage = Utils.Parse(value) / 100;}
		}
		
		[Ignore]
		public string DetailAmountString {
			get { return DetailAmount.ToString ("F");}
			set { DetailAmount = Utils.Parse (value);}
		}
		
		[Ignore]
		public string DetailPerNightAmountString {
			get { return DetailPerNightAmount.ToString ("F");}
			set { DetailPerNightAmount = Utils.Parse(value);}
		}

		
		[Ignore]
		public string DetailCalculatedAmountString {
			get { return DetailCalculatedAmount.ToString ("F");}
			set { DetailCalculatedAmount = Utils.Parse(value);}
		}
	}
}

