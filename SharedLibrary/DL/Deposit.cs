using System;
using SQLite;
using SharedLibrary.DAL;
using SharedLibrary.BL;
using SharedLibrary.BL.Contracts;
using System.Collections.Generic;
using System.Globalization;

namespace SharedLibrary.DL
{
	public class Deposit : IDetail
	{
		[PrimaryKey, AutoIncrement]
		public int 	    ID { get; set;}

		[Indexed(Name = "DepositIndex", Order = 2, Unique = true)]
		public string   Name { get; set; }
		public string 	Description { get; set; }
		public decimal	Amount { get; set; }
		public decimal	Percentage { get; set; }
		public bool		Required { get; set; }
		public bool		Refundable{ get; set;}
		[Indexed(Name = "DepositIndex", Order = 1, Unique = true)]
		public int 		PropertyID { get; set; }

		[Indexed]
		public string	RemoteID { get; set;}

		[Ignore]
		public bool		Taxable{ 
			get{return false;} 
			set { }
		}

		[Ignore]
		public decimal 	PerNightAmount{ 
			get{return 0.00m;} 
			set{ }
		}



		public Deposit(){

		}


		public Deposit(int propertyID)
		{
			PropertyID = propertyID;
		}

		public void Save()
		{
			if (PropertyID == 0) {
				throw new System.Exception ("PropertyID must be set to a valid property");
			}

			if (ID == 0)
				Repository.DB.Insert (this);
			else
				Repository.DB.Update (this);
		}

		public void Update(){
			if (PropertyID == 0) {
				throw new System.Exception ("PropertyID must be set to a valid property");
			}

			Repository.DB.Update (this);
		}

		public void Delete() {
			if (ID != 0) {
				Repository.DB.Delete<Deposit> (ID);
			}
		}

		public static void Delete(int propertyID){
			Repository.DB.Delete<Deposit> (propertyID);
		}

		public static void DeleteAll(int propertyID)
		{
			var deposits = Deposit.FindByProperty (propertyID);
			if (deposits == null)
				return;
			foreach(Deposit d in deposits){
				d.Delete ();
			}
		}

		public static int Count
		{
			get {
				var command = Repository.DB.CreateCommand("select count(*) from Deposit");
				var count = command.ExecuteScalar<int>();
//				Console.WriteLine ("Tax.Count returning {0}", count);
				return count;
			}
		}

		public static Deposit Find(int id )
		{

			var record = Repository.DB.Find<Deposit> (id);
			if (record == null)
				throw new ArgumentException ("Deposit ID " + id +" is invalid");
			return record;
		}

		public static List<Deposit> FindByProperty(int propertyID)
		{
			List<Deposit> deps = Repository.DB.Query<Deposit> ("select * from Deposit where PropertyID = ?", propertyID);
			if (deps.Count == 0)
				return null;
			return deps;
		}

		public static  List<Deposit> Deposits(int propID)
		{
			var deposits = Repository.DB.Query<Deposit> ("select * from deposit where PropertyID = ?", propID);
			return deposits;
		}

		public string PercentageString {
			get { return (Percentage * 100).ToString ("F");}
			set { Percentage = Decimal.Parse(value, NumberStyles.Currency) / 100;}
		}

		public string PerNightAmountString {
			get {return PerNightAmount.ToString ("F");}
			set {PerNightAmount = Decimal.Parse(value, NumberStyles.Currency); }
		}

		public string AmountString {
			get { return Amount.ToString ("F"); }
			set { Amount = Decimal.Parse (value, NumberStyles.Currency); }
		}

	}
}


	
