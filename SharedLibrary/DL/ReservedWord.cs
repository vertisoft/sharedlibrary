using System;
using SharedLibrary.DAL;

using SQLite;
using System.Collections.Generic;
using SharedLibrary.BL;
using SharedLibrary.DL;
using SharedLibrary;
//using SharedLibrary.BL;
//using System.ComponentModel;
using System.IO;
using SharedLibrary.LIB;


namespace SharedLibrary.DL
{
	public class ReservedWord  : BL.Contracts.BusinessEntityBase
	{

		[Indexed(Name = "RWIndex", Order = 2, Unique = true)]
		public string	Name { get; set; }

		public string	Description { get; set; }
		public string	Text { get; set; }			// if there is text it is a Custom word
		public string	WordType { get; set; }			// C = Custom, I = Internal V=Variable ( based upon info from fees, ...
		public int 		DataType{ get; set;}
		public string	TableName{ get; set; }    	// Table name it came from
		public int		TableID { get; set; }		// The id in the above table the word came from

		[Indexed(Name = "RWIndex", Order = 1, Unique = true)]
		public int		PropertyID { get; set; }    // Property the reserved word belongs to

		[Indexed]
		public string   RemoteID { get; set;}


		public ReservedWord()
		{
		}

		public ReservedWord(int propertyID)
		{
			PropertyID = propertyID;
		}

		public void Save()
		{
			if (PropertyID == 0) {
				throw new VsIdMisingException ("PropertyID must be set to a valid property");
			}

			if (ID == 0)
				Repository.DB.Insert (this);
			else
				Repository.DB.Update (this);
		}


		public void Delete() {
			if (ID != 0) {
				Repository.DB.Delete<ReservedWord> (ID);
			}
		}

		public static void DeleteAll(int propertyID)
		{
			var records = ReservedWordsByProperty (propertyID);
			if (records == null)
				return;
			foreach (ReservedWord r in records)
			{
				r.Delete ();
			}
		}

		public static List<ReservedWord> ReservedWordsByProperty(int propertyID)
		{
			return Repository.DB.Query<ReservedWord> ("Select * from ReservedWord where PropertyID = ?", propertyID);
		}


		public static ReservedWord Find( int id, string word)
		{
			var rwords = Repository.DB.Query<ReservedWord> ("Select * from ReservedWord where PropertyID = ? and Name = ?", id, word);
			if (rwords.Count == 0)
				return null;
			return(rwords [0]); // better only be one
		}

		public static int Count
		{
			get {
				var command = Repository.DB.CreateCommand("select count(*) from ReservedWord");
				var count = command.ExecuteScalar<int>();
//				Console.WriteLine ("Count returning {0}", count);
				return count;
			}
		}

		public static List<ReservedWord> CustomReservedWords(int id)
		{
			var reservedWords = Repository.DB.Query<ReservedWord> ("select * from reservedword where PropertyID = ? and WordType = 'C'", id);
			return reservedWords;
		}

		public static List<Word> ReservedWordsSorted(int propID)
		{
			List<Word> list = new List<Word>();
			list = BaseWordList (list);
			list = CustomWords (propID, list);
			list = DiscountWords (propID, list);
			list = FeeWords (propID, list);
			list = DepositWords (propID, list);
			list = TaxWords (propID, list);
//			Console.WriteLine ("ReservedWord.ReservedWords Words in list = {0}", list.Count);


			return list;
		}

		public static List<Word> ReservedWords(int propID)
		{
			List<Word> list = new List<Word>();
			list = BaseWordList (list);
			list = CustomWords (propID, list);
			list = FeeWords (propID, list);
			list = DiscountWords (propID, list);
			list = DepositWords (propID, list);
			list = TaxWords (propID, list);
//			Console.WriteLine ("ReservedWord.ReservedWords Words in list = {0}", list.Count);
			return list;
		}


		private static List<Word> CustomWords(int propID, List<Word> list)
		{
			var rWords = ReservedWord.CustomReservedWords (propID);
			foreach(ReservedWord rw in rWords)
			{
				Word word = new Word ();
				word.KeyWord = rw.Name;
				word.WordType = "C";
				word.StringValue = rw.Text;
				list.Add (word);
			}
			return list;
		}

		private static List<Word> DepositWords(int propID, List<Word> list)
		{
			var deposits = Deposit.Deposits (propID);
			foreach(Deposit d in deposits)
			{
//				Word word = new Word ();
//				word.KeyWord = d.Name;
//				word.WordType = "V";
//				list.Add (word);
				list = DetailWords (d.Name, list);
			}
			return list;
		}

		private static List<Word> DetailWords(string value, List<Word> list)
		{
			list.Add (CreateReservedWord (value));
			list.Add (CreateReservedWord (value + "_Percentage"));
//			list.Add (CreateReservedWord (value + "_PerNight")); 
			return list;
		}

		private static Word CreateReservedWord(string value)
		{
			var newWord = value.Replace (" ", "_");
			Word word = new Word ();
			word.KeyWord = newWord;
			word.WordType = "V";
			return word;
		}
		
		private static List<Word> DiscountWords(int propID, List<Word> list)
		{
			var discounts = Discount.Discounts (propID);
			foreach(Discount d in discounts)
			{
				list = DetailWords (d.Name, list);
//				Word word = new Word ();
//				word.KeyWord = d.Name;
//				word.WordType = "V";
//				list.Add (word);
			}
			return list;
		}
		
		private static List<Word> FeeWords(int propID, List<Word> list)
		{
			var fees = Fee.Fees (propID);
			foreach(Fee d in fees)
			{
				list  = DetailWords (d.Name, list);
//				Console.WriteLine ("Add {0} to list of reserved words", d.Name);
//				Word word = new Word ();
//				word.KeyWord = d.Name;
//				word.WordType = "V";
//				list.Add (word);
			}
			return list;
		}

		private static List<Word> TaxWords(int propID, List<Word> list)
		{
			var taxes = Tax.Taxes (propID);
			foreach(Tax d in taxes)
			{
				list = DetailWords (d.Name, list);
//				Word word = new Word ();
//				word.KeyWord = d.Name;
//				word.WordType = "V";
//				list.Add (word);
			}
			return list;
		}


		private static Word  NewWord(string value)  // Createsa new internal word
		{
			Word word = new Word ();
			word.KeyWord =  value.Replace (" ", "_");
			word.WordType = "I";
			return word;
		}
		private static List<Word> BaseWordList(List<Word> list)
		{   list.Add (NewWord (""));
			list.Add (NewWord ("Adults"));
			list.Add (NewWord ("ArrivalDate"));
			list.Add (NewWord ("ArrivalDateFull"));
			list.Add (NewWord ("ArrivalDateShort"));
			list.Add (NewWord ("BaseRental"));
			list.Add (NewWord ("ChannelCode"));
			list.Add (NewWord ("ChannelName"));
			list.Add (NewWord ("Children"));
			list.Add (NewWord ("Comment"));
			list.Add (NewWord ("CommentResponse"));
			list.Add (NewWord ("ComplexName"));
			list.Add (NewWord ("DepartureDate"));
			list.Add (NewWord ("DepartureDateFull"));
			list.Add (NewWord ("DepartureDateShort"));
			list.Add (NewWord ("DiscountedRental"));
			list.Add (NewWord ("ExtraGuestFee") ); 
			list.Add (NewWord ("Fax"));
			list.Add (NewWord ("FirstName"));
			list.Add (NewWord ("GuestEmail"));
			list.Add (NewWord ("GuestName"));
			list.Add (NewWord ("GuestPhone"));
			list.Add (NewWord ("Guests"));
			list.Add (NewWord ("LastName"));
			list.Add (NewWord ("ListingNumber"));
			list.Add (NewWord ("MinumumNights"));
			list.Add (NewWord ("Nights")); 
			list.Add (NewWord ("NightsFull")); 
			list.Add (NewWord ("NonTaxable"));
			list.Add (NewWord ("PropertyAddress1"));
			list.Add (NewWord ("PropertyAddress2"));
			list.Add (NewWord ("PropertyCity")); 
			list.Add (NewWord ("PropertyDescription"));
			list.Add (NewWord ("PropertyShortName"));
			list.Add (NewWord ("PropertyName"));
			list.Add (NewWord ("PropertyState"));
			list.Add (NewWord ("PropertyZip"));
			list.Add (NewWord ("ResponseEmail"));
			list.Add (NewWord ("SecurityCode"));
			list.Add (NewWord ("Sleeps"));
			list.Add (NewWord ("Taxable"));
			list.Add (NewWord ("Today"));
			list.Add (NewWord ("TotalCharge"));
			list.Add (NewWord ("TotalDeposits"));
			list.Add (NewWord ("TotalDiscounts"));
			list.Add (NewWord ("TotalFees"));
			list.Add (NewWord ("TotalTaxes"));
			list.Add (NewWord ("VoicePhone"));
	        list.Add (NewWord ("URL"));

			return list;
		}


	}
}



