using System;

namespace SharedLibrary
{
	public class VsInvalidDateException : Exception
	{
		public VsInvalidDateException ()
		{
		}

		public VsInvalidDateException(string message) : base(message)
		{
		}

		public VsInvalidDateException(string message, Exception inner)
			: base(message, inner)
		{}
	}

	public class VsPropertyIDMisingException : Exception
	{
		public VsPropertyIDMisingException(string message)
			: base(message)
		{}

	}

	public class VsIdMisingException : Exception
	{
		public VsIdMisingException(string message)
			: base(message)
		{}

	}

	public class VsException : Exception
	{
		public VsException(string message)
			: base(message)
		{}

	}

	public class VsListingException : Exception
	{
		public VsListingException(string message)
			: base(message)
		{}

	}

	public class VsReservationConflictException : Exception{
		public VsReservationConflictException(string message)
			:base(message)
		{}
	}

}

