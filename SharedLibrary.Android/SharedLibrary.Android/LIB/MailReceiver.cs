﻿// This needs to be created for each OS platform since the library is specific. It should be the same code for all if using limlab

using System;

using System.Collections.Generic;
using SharedLibrary.SAL.MailProcessing;
using SharedLibrary.DL;
using System.Text.RegularExpressions;
using System.IO;
using Limilabs.Mail;
using Limilabs.Client.IMAP;
using Limilabs.Client;
using Limilabs.Mail.Licensing;
using Limilabs.Mail.Fluent;
//using System.Runtime.Remoting.Channels;
//using System.Runtime.InteropServices;
//using Java.Lang;
using System.Threading;
using Java.Util;







namespace SharedLibrary.LIB
{
	public class MailReceiver
	{
		private string _hostName;
		private int _port;
		private bool _useSSL = true;
		private string _userName;
		private string _password;
		private string _inbox;
		private string _archive;
		private string _apiKey;
		//		private SslMode _security;
		private string _userToken;
		private Inquiry inq;

		public Imap _client;

		MailServer ms;

		SharedLibrary.DL.Property _property;
		private VrWizardServer _server;
		private bool _idleRunning = false;
		private bool _supportsIdle =  false;
		private bool _archiveInquiries = false;

		public MailReceiver (Property property, bool archiveInquiries, string userToken)
		{
			_userToken = userToken;
			_apiKey = _userToken;
//			Console.WriteLine ("User Token = {0}", _userToken);
			_property = property;
			_archiveInquiries = _archiveInquiries;

			var licenseStatus = LicenseHelper.Load();
//			Console.WriteLine ("\nLicense Status = {0}\n", licenseStatus.ToString ());

			try {
//				Console.WriteLine ("Calling property MailServer for {0}", property.Name);
				ms = _property.MailServer;
//			    Console.WriteLine ("We have a mail server");
//				Console.WriteLine ("MailReceiver - Servername = {0}", ms.ServerName);
//				Console.WriteLine ("MailReceiver - UserName = {0}", ms.UserLogin);
//				Console.WriteLine ("Password = {0}", ms.UserPassword);
				_port = ms.Port;  //143 if none
				_hostName = ms.ServerName;

				_userName = ms.UserLogin;
				_password = ms.UserPassword;
				_inbox = ms.InBox;
				_archive = ms.Archive;
				_useSSL = ms.UseSSL;
				Console.WriteLine ("MailReciever: {0} - {1} - {2}, {3}, {4}", 
					_hostName, _userName, _inbox, _archive, _port);

			} catch (Exception ex) {
				//Console.WriteLine ("Mail Server does not exist in MailReceiver {0}", ex);
				return;
				// throws exception if no mailserver setup
			}

			_server =  new VrWizardServer ();
			_apiKey = _userToken;
			//Console.WriteLine ("Connectiong to imap server");
			_client = new Imap ();


			System.Timers.Timer tmImapConnect = new System.Timers.Timer ();
			tmImapConnect.Interval = (1000*60)*20;
			tmImapConnect.Start ();
			tmImapConnect.Elapsed += (object sender, System.Timers.ElapsedEventArgs e) => {

				if (_useSSL == true)
					_client.ConnectSSL (_hostName);
				else
					_client.Connect (_hostName);
				//Console.WriteLine ("Connected to Gmail");
				try {
					_client.Login (_userName, _password);
//				_client.Login ("luxurymauivista@gmail.com", "kqsz pepe cgap cdig");
					//Console.WriteLine ("Logged In");
					_idleRunning = true;
				} catch (Exception ex) {
					Console.WriteLine ("Exception {0}", ex.Message);
					_idleRunning = false;
					_client.Dispose ();
					return;
				}
				_supportsIdle = _client.SupportedExtensions ()
				.Contains (ImapExtension.Idle);
			};
			//Console.WriteLine ("MailReceiver Supports IDLE: {0}", _supportsIdle);
			//Console.WriteLine ("MailReceiver Client = {0}", _client);
		}

//		public static void ValidMail()
//		{
//			var licenseStatus = LicenseHelper.Load ();
//			var c = new Imap ();
//			if (_useSSL == true)
//				_client.ConnectSSL (_hostName);
//			else
//				_client.Connect (_hostName);
//			c.Login (_userName, _password);
//			Console.WriteLine ("Logged In with ValidMail");
//
//
//		}

		public void StopImapIdle()
		{
			Console.WriteLine ("MailReceiver stoped idle");
			_idleRunning = false;
		}

		public void InitReceiveMail()
		{
//			Console.WriteLine ("InitReceiveMail IdleRunning = {0}", _idleRunning);

			if (_hostName == null) {
				return;
			}
			if (_idleRunning == false)
				return;

//			List<long> processedInquiries = new List<long>();
			//			using (Imap client = new Imap ()) {
			//			Imap client = new Imap ();
//			_client = new Imap ();
//			_client.Connect (_hostName);
//			_client.Login (_userName, _password);
			//Console.WriteLine ("InitReceiveMail Client = {0}", _client);
			FolderStatus folderStatus = _client.SelectInbox();
			int messages = ProcessSetOfMail (folderStatus);
			Console.WriteLine ("InitReceiveMail FolderStatus = {0} Messages ={1} Processed = {2}", 
				folderStatus.Name, folderStatus.MessageCount, messages);
		}

		public int ReceiveMail()
		{
			//Console.WriteLine ("ReceiveMail client= {0}", _client);
			if (_idleRunning == false) {
			
				//Console.WriteLine ("RecieveMail is Not Processing");
				return -1; // counld return -1 to stop the email monitor
			}
		

			//Console.WriteLine ("----ReceiveMail Starting Idle Mode");
			try {
				FolderStatus currentStatus = _client.Idle();

				//Console.WriteLine ("---ReceiveMail Idle returned {0} messages", currentStatus.MessageCount);
				return ProcessSetOfMail (currentStatus);
			}
			catch(Exception ex) 
			{
				//Console.WriteLine ("Mail idle threw exception {0}", ex.Message);
				return 0;
			}

		}


		private int ProcessSetOfMail(FolderStatus currentStatus)
		{		
			List<long> processedInquiries = new List<long>();
			foreach(long uid in _client.Search(Flag.Unseen))
			{
				IMail email = new MailBuilder().CreateFromEml(_client.GetHeadersByUID(uid));
				Console.WriteLine(email.Subject);
				if (ProcessInquiryOnline (_client, uid)) {
					processedInquiries.Add (uid);
				}
				if (_archiveInquiries) {
					//					Console.WriteLine ("Archive the inquiry");
					IndicateProcessed (_client, processedInquiries);
				}
				//				else
				//					Console.WriteLine ("Do not archive Inquiry");
			}
			return processedInquiries.Count;
		}



		public void ProcessNewMail (bool archiveInquiries)
		{

			//Console.WriteLine ("ProcessingNewMail");
			if (_hostName == null) {
				return;
//				throw new Exception ("MailServer is not set up");
			}
			//Console.WriteLine ("Process Client = {0}", _client);
			List<long> processedInquiries = new List<long>();

//			using (Imap client = new Imap ()) {
			Imap client = _client;
//			Imap client = new Imap ();
//			client.Connect (_hostName);
//			client.Login (_userName, _password);
//
//
//				bool supportsIdle = client.SupportedExtensions ()
//					.Contains (ImapExtension.Idle);
//				Console.WriteLine ("Supports IDLE: {0}", supportsIdle);
//				
			
			client.SelectInbox ();
			List<long> uidList = client.Search (Flag.Unseen);
			//Log.Debug(GetType().FullName, "Recovered instance state {0}", _emailServiceRunning);
			//Console.WriteLine (	"ProcessMail Number of inquiries = {0}", uidList.Count);
			foreach (long uid in uidList) {
				//Console.WriteLine ("ProcessMail Looking at uid {0}", uid);
				if (ProcessInquiryOnline (client, uid)) {
					processedInquiries.Add (uid);
				}
				//Console.WriteLine ("done processingInquiry {0}", uid);
			}
			if (archiveInquiries) {
				//Console.WriteLine ("Archive the inquiry");
				IndicateProcessed (client, processedInquiries);
			}
//			else
				//Console.WriteLine ("Do not archive Inquiry");

		}

		private bool ProcessInquiryOnline(Imap client, long uid)
		{
			//Console.WriteLine ("In ProcessInquiryOnline");
			var eml = client.PeekMessageByUID (uid);
			IMail email = new MailBuilder ().CreateFromEml (client.PeekMessageByUID (uid));
			VrMail vmail = new VrMail (email);
			vmail.UID = uid;
			//FIXME
//			vmail.RawEml = eml;


			//Console.WriteLine ("Calling ProcessMail");

			return _server.ProcessMail ( vmail, _apiKey);
		}

		private bool ProcessInquiry(Imap client, long uid)
		{
			//Console.WriteLine ("In ProcessInquiry");
			var eml = client.PeekMessageByUID (uid);

			IMail email = new MailBuilder ().CreateFromEml (client.PeekMessageByUID (uid));
//			Console.WriteLine ("Subject =  {0}", email.Subject);
			VrMail vmail = new VrMail (email);
			vmail.UID = uid;
//			Console.WriteLine ("Create MailProcessor");
			MailProcessor mp = new MailProcessor (vmail);
//			Console.WriteLine ("MailProcessor Created");
			Inquiry inq = mp.ExtractInquiry ();
			if (inq == null){
				Console.WriteLine ("---- Invalid ProcessInquiry Not a valid inquiry Skip it subject = {0}", email.Subject);
				return false;
			}
			Console.WriteLine ("---- Valid Inquiry for {0} from [{1}]", inq.GuestName, inq.Channel);

			return true ;

		}

	


		private void IndicateProcessed(Imap _client, List<long> uids)
		{
			if (Property.CurrentProperty.IsDemo) {
				Console.WriteLine ("Leave email for Demo");
				return;  //Leave emails there
			}

			Console.WriteLine ("Indicate Processed not Demo");

			FolderInfo archive = ArchiveFolder (_client);
			foreach(long uid in uids){
				_client.MarkMessageSeenByUID(uid);

				#if !DEBUG
				Console.WriteLine (	"Moving Processed Inquiry to {0}", ms.Archive);
				if (!String.IsNullOrWhiteSpace (ms.Archive)){
					_client.MoveByUID (uid, ms.Archive);
				}
				#endif
			}
		}

		private FolderInfo ArchiveFolder(Imap _client)
		{
			List<FolderInfo> all = _client.GetFolders();
			//			foreach(FolderInfo i in all)
			//				Console.WriteLine ("Folder [{0}]", i.Name);
			CommonFolders folders = new CommonFolders(all);
			#if DEBUG
			Console.WriteLine("Inbox folder: " + folders.Inbox.Name);
			Console.WriteLine("Sent folder: " + folders.Sent.Name);
			Console.WriteLine("Spam folder: " + folders.Spam.Name);
			#endif

			//			var archive = folders.Inbox.Name + "/" + ms.Archive;
			var archive = ms.Archive;
			//			FolderInfo folder = all.Find(x => x.Name == ms.Archive);
			FolderInfo folder = all.Find(x => x.Name == archive);
			if (folder == null)
			{
				//				Console.WriteLine ("Archive Folder [{0}] does not exist, creating", archive);
				_client.CreateFolder (archive);
				//				{
				//					Console.WriteLine ("---- Archive Folder was not created");
				//					return null;
				//				}
				folder = all.Find(x => x.Name == archive);
				//				Console.WriteLine ("After Create");
			}
			//			Console.WriteLine ("ArchiveFolder is returning {0}", folder.Name);
			return folder;
		}


	}
}



