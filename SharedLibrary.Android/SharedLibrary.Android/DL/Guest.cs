﻿using System;
using SharedLibrary.DAL;
using SQLite;

namespace SharedLibrary.DL
{
	public class Guest
	{
		[PrimaryKey, AutoIncrement]
		public int 		ID { get; set; }
		[Indexed]
		public string	RemoteID { get; set;}
		public string	Name { get; set;}
		public int		Adults {get set;};
		public int		Children { get; set;}
		public long		ArrivalJulian { get; set;}
		public long		DepartureJulian { get; set;}
		public string	Comments {get; set;}
		public string	Phone { get; set;}


		field :name,            type: String
		field :adults,          type: Integer
		field :children,        type: Integer
		field :arrival_julian,    type: Fixnum
		field :departure_julian,  type: Fixnum
		field :arrival,         type: Date
		field :departure,       type: Date
		field :comments,        type: String # their initial comments
		field :phone,           type: String
		field :property_num,    type: String
		field :channel,         type: String
		field :email,           type: String
		field :reply_to,        type: String
		field :inquire_date,    type: Date, :default => lambda{Date.today}
		field :status,          type: String # RESERVED, TENTATIVE, BLOCKED
		field :source_document, type: String   # actual original document requesting the inquiry
		field :comment_response, type: String  # my response to their comment
		field :my_comments,     type: String  #my comments about this inquiry
		field :event_comment,   type: String  # any special event being celebrated
		field :stay_reason,     type: String #WEDDING, ANNIV, GRAD, RETIRE, BIRTH, HONEY, VAC
		field :inquiry_email,   type: String # the original inquiry eml


		public Guest ()
		{
		}
	}
}

