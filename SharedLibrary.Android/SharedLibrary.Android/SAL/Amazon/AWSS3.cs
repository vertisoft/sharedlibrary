﻿using System;
using DataNuage.Aws;
using SharedLibrary.DAL;
using System.IO;

namespace SharedLibrary.SAL.Amazon
{
	public class AWSS3
	{
		public  S3  s3;

		public string Bucket { get; set;}
	

		public AWSS3 ()
		{
			s3 = new S3("<Your AWS S3 Access Key Id> - ignored by Trial version",
				"<Your AWS S3 Secret Access Key> - ignored by Trial version");


		}

		public async void BackupDatabase(string name)
		{
			Repository.Close ();
			Console.WriteLine ("Backing up {0}", Repository.DBPath);
			byte[] array = File.ReadAllBytes(Repository.DBPath);
			await s3.PutObjectAsync (Bucket, name, array);
			Repository.Open ();
		}

		public async void RestoreDataBase(string name)
		{
			Repository.Close ();
			byte[] current = File.ReadAllBytes(Repository.DBPath);
			byte[] archive = await s3.GetObjectAsByteArrayAsync (Bucket, name);
			if(Equality (current, archive))
				Console.WriteLine ("Archive and current are equal");
			else
				Console.WriteLine ("Archive is NOT equal to current");
			Repository.Open ();
		}

		public async void Save(string name, string _data)
		{
			await s3.PutObjectAsync(Bucket, name, _data);
			Console.WriteLine ("Saved in {0} - {1}",name, _data);

		}

		public async void  ReadString(string objName, string result)
		{
			var s = await s3.GetObjectAsStringAsync (Bucket, objName);
			Console.WriteLine ("ReadString Returned {0}", s);
			result = s;
		}

		public async void ReadData(string objName, Byte[] result)
		{
			var s = await s3.GetObjectAsByteArrayAsync (Bucket, objName);
			Console.WriteLine ("ReadData Returned {0}", s);
			result = s;
		}

		public async void Delete(string objName)
		{
			await s3.DeleteObjectAsync(Bucket, objName);
		}

		public async void DeleteBucket()
		{
			await s3.DeleteBucketAsync(Bucket);
			Bucket = null;
		}

		public async void CreateBucket(string name)
		{
			var random = new Random();
			var _mybucket = name + random.Next ();
			Bucket = _mybucket;
			Console.WriteLine ("Creating bucket {0}", Bucket);
			await s3.CreateBucketAsync(Bucket);
			Console.WriteLine ("Bucket is created {0}", Bucket);

		}

		private bool Equality(byte[] a1, byte[] b1)
		{
			if (a1 == null && b1 == null)
				return true;
			if(a1 == null || b1 == null)
				return false;
			int length = a1.Length;
			if(b1.Length != length)
				return false;
			while(length >0) {
				length--;
				if(a1[length] != b1[length])
					return false;           
			}
			return true;        
		}
	}
}

