using System;

namespace Rebex
{
	internal class TrialKey
	{
		/// <summary>
		/// Set the key to start your evaluation period.
		/// You can get your trial key at http://www.rebex.net/support/trial-key.aspx
		/// No key is needed for the full version.
		/// </summary>
		public const string Key = "==Ah5SGQ/xMjyG/yJ4nmGA8ELS1tC0R0TrUAnrxKkqTPes==";
	}
}

