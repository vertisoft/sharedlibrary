using System;
using NUnit.Framework;
using SharedLibrary.DAL;
using SharedLibrary.DL;


namespace SharedUnit
{
	[TestFixture ()]
	public class DiscountShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
//			Repository.DBName = "wizard.test.db3";
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}

		[Test]
		public void CreateNewRate()
		{
			var prop = Property.DemoProperty ();
			var rate = prop.AddRate ();
			rate.Name = "Spring";
			rate.StartDate = new DateTime (2014, 6, 1); //, 7, 47, 0); 
			rate.Save ();
			Assert.AreEqual (Rate.Count, 1);
		}

		[Test]
		[ExpectedException( "SQLite.SQLiteException", ExpectedMessage="Constraint" )]
		public void NotAllowDuplicate()
		{
			var prop = Property.DemoProperty ();
			var rate = prop.AddRate ();
			rate.Name = "Spring";
			rate.StartDate = new DateTime (2014, 6, 1); //, 7, 47, 0); 
			rate.Save ();

			rate = prop.AddRate ();
			rate.Name = "Spring";
			rate.StartDate = new DateTime (2014, 6, 1); //, 7, 47, 0); 
			rate.Save ();

		}

		[Test]
		[ExpectedException( "SQLite.SQLiteException", ExpectedMessage="Constraint" )]
		public void UpdateNotAllowDuplicate()
		{
			var prop = Property.DemoProperty ();
			var rate = prop.AddRate ();
			rate.Name = "Spring";
			rate.StartDate = new DateTime (2014, 6, 1); //, 7, 47, 0); 
			rate.Save ();

			var rate1 = prop.AddRate ();
			rate1.Name = "Fall";
			rate1.StartDate = new DateTime (2014, 6, 1); //, 7, 47, 0); 
			rate1.Save ();
			Assert.AreEqual (Rate.Count, 2);

			rate1.Name = "Spring";
			rate1.Save ();
		}

		[Test]
		public void Update()
		{
			var prop = Property.DemoProperty ();
			var rate = prop.AddRate ();
			rate.Name = "Spring";
			rate.StartDate = new DateTime (2014, 6, 1); //, 7, 47, 0); 
			rate.Save ();

			rate.Name = "Spring Rate";
			rate.Save ();
			Assert.AreEqual (Rate.Count, 1);
		}
	}
}



