using System;
using NUnit.Framework;
using SharedLibrary.DAL;
using SharedLibrary.DL;

namespace SharedUnit
{
	[TestFixture]
	public class RateShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.Open ();
			Repository.Remove ();
			Repository.DBName = "wizard.test.db3";
			Repository.Open ();
			var user = User.Current;
			Console.WriteLine ("Setup User {0} - {1} ", user.ID,user.Name );
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}

		[Test]
		public void CreateNewRate()
		{
			var prop = Property.DemoProperty ();
			var rate = prop.AddRate ();
			rate.Name = "Spring";
			rate.StartDate = new DateTime (2014, 6, 1); 
			rate.Save ();
			Assert.AreEqual (Rate.Count, 1);

			Assert.AreEqual ("6/1/2014", rate.StartDate.ToShortDateString());
			var rates = Repository.DB.Query<Rate> ("select * from Rate where StartDate = ?", new DateTime (2014, 6, 1));
			Assert.AreEqual (1, rates.Count);
		}

		[Test]
		[ExpectedException( "SQLite.SQLiteException", ExpectedMessage="Constraint" )]
		public void NotAllowDuplicate()
		{
			var prop = Property.DemoProperty ();
			var rate = prop.AddRate ();
			rate.Name = "Spring";
			rate.StartDate = new DateTime (2014, 6, 1); //, 7, 47, 0); 
			rate.Save ();

			rate = prop.AddRate ();
			rate.Name = "Spring";
			rate.StartDate = new DateTime (2014, 6, 1); //, 7, 47, 0); 
			rate.Save ();

		}

		[Test]
		[ExpectedException( "SQLite.SQLiteException", ExpectedMessage="Constraint" )]
		public void UpdateNotAllowDuplicate()
		{
			var prop = Property.DemoProperty ();
			var rate = prop.AddRate ();
			rate.Name = "Spring";
			rate.StartDate = new DateTime (2014, 6, 1); //, 7, 47, 0); 
			rate.Save ();

			var rate1 = prop.AddRate ();
			rate1.Name = "Fall";
			rate1.StartDate = new DateTime (2014, 6, 1); //, 7, 47, 0); 
			rate1.Save ();
			Assert.AreEqual (2, Rate.Count);

			rate1.Name = "Spring";
			rate1.Save ();
		}

		[Test]
		public void Update()
		{
			var prop = Property.DemoProperty ();
			var rate = prop.AddRate ();
			rate.Name = "Spring";
			rate.StartDate = new DateTime (2014, 6, 1); //, 7, 47, 0); 
			rate.Save ();

			rate.Name = "Spring Rate";
			rate.Save ();
			Assert.AreEqual (1, Rate.Count);
		}
	}
}



