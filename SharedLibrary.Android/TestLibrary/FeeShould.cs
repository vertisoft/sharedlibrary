using System;
using NUnit.Framework;
using SharedLibrary.DAL;
using SharedLibrary.DL;


namespace SharedUnit
{
	[TestFixture()]
	public class FeesShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			//			Repository.DBName = "wizard.test.db3";
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}


		[Test()]
		public void CreateFee ()
		{
			var demo = Property.DemoProperty ();
			var fee = demo.AddFee ();
			fee.Name = "Cleaning";
			fee.Description = "Post departure cleaning";
			fee.Required = true;
			fee.Taxable = true;
			fee.Amount = 125.00m;
			fee.Save ();
			Assert.AreEqual (1, Fee.Count);

//			var message = "";
//			try{
//				fee.Save();
//			}
//			catch(Exception e){
//				message = e.Message;
//			}
//			Assert.AreEqual ( "PropertyID must be set to a valid property", message);
//			fee.PropertyID = demo.ID;
//			message = "";
//			try{
//				fee.Save();
//			}
//			catch(Exception e){
//				message = e.Message;
//			}
//			Assert.AreEqual (message, "");
		}
	}
}



