using System;
using NUnit.Framework;
using SharedLibrary.DAL;
using SharedLibrary.DL;

namespace SharedUnit
{
	[TestFixture]
	public class ListingShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}

		[Test]
		public void CreateNewListing()
		{
			var demo = Property.DemoProperty ();
			var list = demo.AddListing ();
			list.ChannelCode = "VRBO";
			list.ListingNumber = "209114";
			list.Save ();
			Assert.AreEqual (Listing.Count, 1);
		}

		[Test]
		public void Update()
		{
			var demo = Property.DemoProperty ();
			var list = demo.AddListing ();
			list.ChannelCode = "VRBO";
			list.ListingNumber = "209114";
			list.Save ();
			list = demo.AddListing ();
			list.ChannelCode = "VRBO";
			list.ListingNumber = "1234";
			list.Save ();

			list.UserName = "testUser";
			list.Save ();
		}

		[Test]
		public void SaveWithDifferentCodeSameNumber()
		{
			var demo = Property.DemoProperty ();
			var list = demo.AddListing ();
			list.ChannelCode = "VRBO";
			list.ListingNumber = "209114";
			list.Save ();
			list = demo.AddListing ();
			list.ChannelCode = "HOME";
			list.ListingNumber = "209114";
			list.Save ();
		}

		[Test]
		[ExpectedException( typeof( SQLite.SQLiteException), ExpectedMessage="Constraint", MatchType=MessageMatch.Contains )]
		public void NotUpdateDuplicates()
		{
			var demo = Property.DemoProperty ();
			var list = demo.AddListing ();
			list.ChannelCode = "VRBO";
			list.ListingNumber = "209114";
			list.Save ();
			list = demo.AddListing ();
			list.ChannelCode = "VRBO";
			list.ListingNumber = "1234";
			list.Save ();

			list.ListingNumber = "209114";
			list.Save ();

		}

		[Test]
		[ExpectedException( typeof( SQLite.SQLiteException), ExpectedMessage="Constraint", MatchType=MessageMatch.Contains )]
		public void NotSaveDuplicates()
		{
			var demo = Property.DemoProperty ();
			var list = demo.AddListing ();
			list.ChannelCode = "VRBO";
			list.ListingNumber = "209114";
			list.Save ();
			list = demo.AddListing ();
			list.ChannelCode = "VRBO";
			list.ListingNumber = "209114";
			list.Save ();

		}
	}
}

