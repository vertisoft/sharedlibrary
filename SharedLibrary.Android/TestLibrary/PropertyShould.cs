using System;
using NUnit.Framework;
using SharedLibrary.DAL;
using SharedLibrary.DL;




namespace TestLibrary
{
	[TestFixture]
	public class PropertyShould
	{
		
		[SetUp]
		public void Setup ()
		{
			Console.WriteLine ("Setup");
			Repository.DBName = "wizard.tests.db3";
			Repository.DropDatabase ();
			Repository.DBName = "wizard.test.db3";
			Repository.Open ();
		}

		[TearDown]
		public void Tear ()
		{
			Repository.DropDatabase ();
		}

		[Test]
		public void FindTheDemo ()
		{
			Repository.Open ();
			var prop = Property.DemoProperty ();

			Assert.AreEqual (1, prop.ID);
			Console.WriteLine ("Find is Done");
			Console.WriteLine (" ");
		}

		[Test]
		public void CreateAProperty()
		{
			var demo = Property.DemoProperty ();
			var prop = new Property ();

			prop.Name = "Test Property";
			prop.ShortName = "TP";
			prop.Save ();
//			Console.WriteLine ("Demo = {0} - New = {1}", demo.ID, prop.ID);
			Assert.AreNotEqual (prop.ID, 0);
			var p = Property.Find (prop.ID);
//			Console.WriteLine ("New = {0} - {1}", p.ID, p.Name);
			var d = Property.Find (demo.ID);
//			Console.WriteLine ("Demo = {0} - {1}", d.ID, d.Name);			
			Assert.AreEqual (2, Property.Count);
		}

		[Test]
		public void CreateDemoProperty ()
		{
//			Console.WriteLine ("CreateDemoProperty");
//		    Console.WriteLine ("CreateDemoProperty is calling Demo with {0} demos", Property.DemoCount);
			var demo = Property.DemoProperty ();
//			Console.WriteLine("CreateDempPRoperty returns with id = {0} and {1} demos", demo.ID, Property.DemoCount);

			Assert.AreEqual (demo.Name, "Demo Property");
			Assert.AreEqual (demo.Weekday, 185.00m);
			Assert.AreEqual (1, Property.Count);
		}




		[Test]
		public void FindNoFees() {
			var demo = Property.DemoProperty ();
			var fees = demo.Fees ();
			Assert.AreEqual (fees.Count, 0);
		}

		[Test]
		public void FindFees() {
			var demo = Property.DemoProperty ();
			var fee = demo.AddFee();
			fee.Name = "Cleaning";
			fee.Amount = 125.00m;
			fee.Save ();
			fee = demo.AddFee ();
			fee.Name = "Damage Insurance";
			fee.Amount = 49.00m;
			fee.PropertyID = demo.ID;
			fee.Save ();

			Assert.AreEqual (demo.FeeCount, 2, "Was not one fee");
		}

		[Test]
		public void FindNoDeposits() {
			var demo = Property.DemoProperty ();
			Assert.AreEqual (demo.FeeCount, 0);
		}

		[Test]
		public void FindDeposits() {
			var demo = Property.DemoProperty ();
			var dep = demo.AddDeposit();
			dep.Name = "Security";
			dep.Amount = 200.00m;
			dep.Save ();
			dep = demo.AddDeposit ();
			dep.Name = "Key";
			dep.Amount = 100.00m;
			dep.Save ();
			Assert.AreEqual (demo.DepositCount, 2, "Was not two deposits");
		}

		[Test]
		public void FindNoReservedWords() {
			var demo = Property.DemoProperty ();
			//			var rw = demo.ReservedWords ();
			Assert.AreEqual (demo.ReservedWordCount, 0);
		}

		[Test]
		public void FindReservedWords() {
			var demo = Property.DemoProperty ();
			var rw = demo.AddReservedWord();
			rw.Name = "Signature";
			rw.Text = "Donald French";
			rw.Save ();
			rw = demo.AddReservedWord ();
			rw.Name = "Phone";
			rw.Text = "702-555-1212";
			rw.Save ();
			Assert.AreEqual (demo.ReservedWordCount, 2, "Was not two Reserved Words");
		}

		[Test]
		public void FindNoTaxes() {
			var demo = Property.DemoProperty ();
			//			var taxes = demo.Taxes ();
			Assert.AreEqual (demo.TaxCount, 0);
		}

		[Test]
		public void FindTaxes() {

			var demo = Property.DemoProperty ();
			var prop = new Property ();
			prop.Name = "Test Property";
			prop.ShortName = "TP";
			prop.Save ();
			var ptax = prop.AddTax ();
			ptax.Name = "Test Tax";
			ptax.Percentage = 10.00m;
			ptax.Save ();
			Assert.AreEqual (Tax.Count, 1);
			Assert.AreEqual (prop.TaxCount, 1);

			var tax = demo.AddTax();
			tax.Name = "HI Tax";
			tax.Description = "Hawaii GIT ";
			tax.Percentage = 13.42m;
			tax.Required = true;
			tax.Save ();

			Assert.AreEqual (demo.TaxCount, 1, "number of taxes was not 1");
			Assert.AreEqual (Tax.Count, 2);
		}

		[Test]
		public void FindNoDiscounts() {
			var demo = Property.DemoProperty ();
			//			var taxes = demo.Taxes ();
			Assert.AreEqual (demo.DiscountCount, 0);
		}

		[Test]
		public void FindDiscount() {

			var demo = Property.DemoProperty ();
			var prop = new Property ();
			prop.Name = "Test Property";
			prop.ShortName = "TP";
			prop.Save ();
			var pdisc = prop.AddDiscount ();
			pdisc.Name = "Test Discount";
			pdisc.Percentage = 10.00m;
			pdisc.Save ();
			Assert.AreEqual (Discount.Count, 1);
			Assert.AreEqual (prop.DiscountCount, 1);

			var disc = demo.AddDiscount();
			disc.Name = "Military";
			disc.Description = "Gift to Vets. ";
			disc.Percentage = 10.00m;
			disc.Save ();

			Assert.AreEqual (demo.DiscountCount, 1, "number of taxes was not 1");
			Assert.AreEqual (Discount.Count, 2);
		}

	}
}

