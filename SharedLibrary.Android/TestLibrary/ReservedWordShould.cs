using System;
using NUnit.Framework;
using SharedLibrary.DL;
using SharedLibrary.DAL;

namespace SharedUnit
{
	[TestFixture ()]
	public class ReservedWordShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}

		[Test]
		public void CreateCustomWord ()
		{
			var demo = Property.DemoProperty ();
			var rw = demo.AddReservedWord ();
			//			var dep = new Deposit ();

			rw.Name = "My Name";
			rw.Text = "Donald French";
			rw.Description = "To be used as a signature";
			rw.Save ();

			Assert.AreEqual (1, rw.ID);
		}


	}
}


