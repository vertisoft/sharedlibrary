using NUnit.Framework;
using System;

using SharedLibrary.SAL.MailProcessing;
using SharedLibrary.DAL;
using SharedLibrary;

namespace SharedUnit
{
	[TestFixture]
	public class VRBOFwdShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}

//		[Test]
//		public void IdentifyInquiry ()
//		{
//
//			var source = VRBO.From (new MailBuilder ().CreateFromEmlFile (@"../../Samples/vrbo.eml"));
//			Assert.IsNotNull (source);
//			var inq = source.Extract ();
//			Assert.AreEqual (inq.Channel, "HOME");
////			Assert.AreEqual ("F", inq.ReceivedMethod);
//			Assert.AreEqual (1, inq.PropertyID);
//			Assert.AreEqual ( new DateTime (2013,10,10), inq.ArrivalDate);
//			Assert.AreEqual ("Theresa French", inq.GuestName);
//			StringAssert.AreEqualIgnoringCase ("Theresa French", inq.GuestName);
//			StringAssert.AreEqualIgnoringCase ("dhf0820@gmail.com", inq.Email);
//			StringAssert.AreEqualIgnoringCase ("7035039665", inq.Phone);
//			StringAssert.Contains( "Inquiry from vrbo-209114", inq.Comment);
//			StringAssert.Contains ("Theresa French", inq.SourceDocument);
//		}

//		[Test]
//		[ExpectedException(typeof(VsListingException),
//		                   "Property number 209115 from VRBO does not exist")]
//		public void ErrorOnInvalidListingNumber()
//		{
//			string text = @"Vacation Rental Inquiry Notification
//
//Listing #:	209115
//Web Address:	http://www.vrbo.com/209114
//";
//			var source = VRBO.From (text, text);
//			Assert.IsNotNull (source);
//			var inq = source.Extract ();
//		}
	}
}

