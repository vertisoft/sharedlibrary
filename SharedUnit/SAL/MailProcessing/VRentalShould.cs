using NUnit.Framework;
using System;

using SharedLibrary.SAL.MailProcessing;
using SharedLibrary.DAL;
using SharedLibrary;



namespace SharedUnit
{
	[TestFixture]
	public class VRentalShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}

//		[Test]
//		public void IdentifyInquiry ()
//		{
//			var source = VRentals.From (new MailBuilder ().CreateFromEmlFile (@"../../Samples/VacationRentals.eml"));
//			Console.WriteLine ("Source = {0}", source);
//			Assert.IsNotNull (source);
//			var inq = source.Extract ();
//			Assert.AreEqual (inq.Channel, "VRental");
//			Assert.AreEqual (1, inq.PropertyID);
//			Assert.AreEqual ("Theresa French", inq.GuestName);
//			Assert.AreEqual ( new DateTime (2013,10,10), inq.ArrivalDate);
//			Assert.AreEqual ( new DateTime (2013,10,17), inq.DepartureDate);
//
//			Assert.AreEqual (2, inq.Adults);
//			Assert.AreEqual (1, inq.Children);
//
//			StringAssert.AreEqualIgnoringCase ("dhf0820@gmail.com", inq.Email);
//			StringAssert.AreEqualIgnoringCase ("702-526-2470", inq.Phone);
//			StringAssert.Contains( "Greate VacationRental 98869", inq.Comment);
//						StringAssert.Contains ("Theresa", inq.SourceDocument);
//		}

		//		[Test]
		//		[ExpectedException(typeof(VsListingException),
		//		                   "Property number 209115 from VRBO does not exist")]
		//		public void ErrorOnInvalidListingNumber()
		//		{
		//			string text = @"Vacation Rental Inquiry Notification
		//
		//Listing #:	209115
		//Web Address:	http://www.vrbo.com/209114
		//";
		//			var source = VRBO.From ();
		//			Assert.IsNotNull (source);
		//			var inq = source.Extract ();
		//		}
	}
}


