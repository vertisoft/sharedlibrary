using NUnit.Framework;
using System;

using SharedLibrary.SAL.MailProcessing;
using SharedLibrary.DAL;
using SharedLibrary;


namespace SharedUnit
{
	[TestFixture]
	public class VRBOShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}

//		[Test]
//		public void IdentifyInquiry ()
//		{
//			string text = @"From = Name='HomeAway' Address='inquiry@homeaway.com'
//Body = [HomeAway.com] [http://www.homeaway.com?
//utm_source=NL&utm_medium=email&utm_term=20131007&utm_content=logo_image_o_loth&utm_campaign=HAUSOwnerInquiry
//] Need Help? Contact Us [http://www.homeaway.com/info/contact-us?
//utm_source=NL&utm_medium=email&utm_term=20131007&utm_content=contactus1_text_o_loth&utm_campaign=HAUSOwnerInquiry
//]
//[http://www.vrbo.com/209114?
//utm_source=NL&utm_medium=email&utm_term=20131007&utm_content=propertythumbnail_image_o_loth&utm_campaign=HAUSOwnerInquiry
//]
//Inquiry for property #292391 [http://www.vrbo.com/209114?
//utm_source=NL&utm_medium=email&utm_term=20131007&utm_content=propertyid_text_o_loth&utm_campaign=HAUSOwnerInquiry
// ]
//Available
//Oct 10, 2013 - Oct 17, 2013 (7 nights)
//Reply [https://www.homeaway.com/haoi/121.292391.3076026/emailReplyAvailable.html?inquiryId=5cd920f87fb144a482b4d533aeffbed8&cid=E_OwnerInquiry_NL_O_20131007_RAA_button_LOTH]
//Inquiry Detail
//Inquiry from vrbo-209114
//Name Theresa French
//Dates Oct 10, 2013 - Oct 17, 2013
//Guests 2 adults, 1 children
//Phone 7035039665
//Email dhf0820@gmail.com
//Inquiry from VRBO.com
//Quick Links";
//
////			var source = VRBO.From (text, text);
//			var source = VRBO.From (new MailBuilder ().CreateFromEmlFile (@"../../Samples/vrbo.eml"));
//			Assert.IsNotNull (source);
//			var inq = source.Extract ();
//			Assert.AreEqual (inq.Channel, "HOME");
//			Assert.AreEqual (1, inq.PropertyID);
//			Assert.AreEqual ("Theresa French", inq.GuestName);
//			Assert.AreEqual ( new DateTime (2013,10,10), inq.ArrivalDate);
//			Assert.AreEqual ( new DateTime (2013,10,17), inq.DepartureDate);
//			Assert.AreEqual (2, inq.Adults);
//			Assert.AreEqual (1, inq.Children);
//
//			StringAssert.AreEqualIgnoringCase ("dhf0820@gmail.com", inq.Email);
//			StringAssert.AreEqualIgnoringCase ("7035039665", inq.Phone);
//			StringAssert.Contains( "Inquiry from", inq.Comment);
////			StringAssert.Contains ("Theresa French", inq.SourceDocument);
//		}

//		[Test]
//		[ExpectedException(typeof(VsListingException),
//		                   "Property number 209115 from VRBO does not exist")]
//		public void ErrorOnInvalidListingNumber()
//		{
//			string text = @"Vacation Rental Inquiry Notification
//
//Listing #:	209115
//Web Address:	http://www.vrbo.com/209114
//";
//			var source = VRBO.From ();
//			Assert.IsNotNull (source);
//			var inq = source.Extract ();
//		}
	}
}

