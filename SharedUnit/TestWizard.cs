using System;
using NUnit.Framework;
using SharedLibrary.SAL;
using RestSharp;
using SharedLibrary.DL;

namespace SharedUnit
{
	[TestFixture()]
	public class TestVrWizard
	{
		[SetUp] public void Init()
		{
			Console.WriteLine ("Setup");
			VrWizardApi.BaseUrl = null;
		}

		[Test]
		public void UrlShouldUseDefaultUrl()
		{
			Console.WriteLine ("Running TestURL");
			var wizard = new VrWizardApi("login", "password");
			StringAssert.AreEqualIgnoringCase ("http://localhost:3000", wizard.CurrentUrl);
		}

		[Test]
		public void UrlShouldUseSpecifiedUrl()
		{
			Console.WriteLine ("Running TestUrlSpecific");
			var wizard = new VrWizardApi("login", "password","http://vrcommand.herokuapp.com");
			StringAssert.AreEqualIgnoringCase("http://vrcommand.herokuapp.com", wizard.CurrentUrl);
		}

		[Test]
		public void WizardClassShouldUseDefaultUrl()
		{
			Console.WriteLine ("Running TestUrlSpecific");
			StringAssert.AreEqualIgnoringCase("http://localhost:3000", VrWizardApi.BaseUrl);
		}

		[Test]
		public void WizardClassShouldUseSpecifiedUrl()
		{
			Console.WriteLine ("Running TestUrlSpecific");
			VrWizardApi.BaseUrl = "http://vrcommand.herokuapp.com";
			StringAssert.AreEqualIgnoringCase("http://vrcommand.herokuapp.com", VrWizardApi.BaseUrl);
		}

		[Test]
		public void WizardShouldDefaultConstructorWithDefaultUrl()
		{
			VrWizardApi.DefaultLogin = "dhf0820";
			VrWizardApi.DefaultPassword = "password";
			var wizard = new VrWizardApi();
			StringAssert.AreEqualIgnoringCase("http://localhost:3000", wizard.CurrentUrl);
		}

		[Test]
		public void WizardShouldDefaultConstructorWithSetUrl()
		{
			Console.WriteLine ("Running TestDefaultConstructor");
			VrWizardApi.BaseUrl = "http://vrcommand.herokuapp.com";
			VrWizardApi.DefaultLogin = "dhf0820";
			VrWizardApi.DefaultPassword = "password";
			var wizard = new VrWizardApi();
			StringAssert.AreEqualIgnoringCase("http://vrcommand.herokuapp.com", wizard.CurrentUrl);
		}

	}
}

