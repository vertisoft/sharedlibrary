using System;
using NUnit.Framework;
using SharedLibrary.DAL;
using SharedLibrary.DL;
using SQLite;
using Microsoft.Win32;

namespace SharedUnit
{
	[TestFixture ()]
	public class ReservationShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			Repository.Open ();
			var prop = Property.DemoProperty ();
			new Reservation(prop.ID, new DateTime(2014, 08, 10),new DateTime( 2014, 08, 31), "HoneyMoon").Save ();
			var r = new Reservation (prop.ID, new DateTime (2014, 07, 20), new DateTime (2014, 07, 31), "Maintenance");
			r.Save();
			Console.WriteLine ("Reservation type = {0}", r.Type );
			Assert.AreEqual (5, prop.ReservationCount);
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}

		[Test]
		public void NotBeAvailableWhenDepartIsDuringReservation ()
		{
			var prop = Property.DemoProperty ();
			// Arrives before and departs during
			Assert.IsNotNull (Reservation.Availability (prop.ID, new DateTime (2014, 08, 05), new DateTime (2014, 08, 15)));
			//Arrives during and departs during
			Assert.IsNotNull (Reservation.Availability (prop.ID, new DateTime (2014, 08, 12), new DateTime (2014, 08, 15)));
			// Arrives on start and departs during
			Assert.IsNotNull (Reservation.Availability (prop.ID, new DateTime (2014, 08, 10), new DateTime (2014, 08, 15)));
			//Arrives on start and departs on end
			Assert.IsNotNull (Reservation.Availability (prop.ID, new DateTime (2014, 08, 10), new DateTime (2014, 08, 31)));
		}

		[Test]
		public void NotBeAvailableWhenArriveBeforeResevationEndsDepartAfterReservationStarts ()
		{
			var prop = Property.DemoProperty ();

			// Arrive before and departs after
			var r = Reservation.Availability (prop.ID, new DateTime (2014, 08, 05), new DateTime (2014, 09, 30));
			Assert.IsNotNull( r);

			// Arrive During first and departs after second
			r = Reservation.Availability (prop.ID, new DateTime (2014, 07, 15), new DateTime (2014, 09, 30));
			Assert.IsNotNull( r);;


			r = Reservation.Availability (prop.ID, new DateTime (2014, 07, 15), new DateTime (2014, 08, 05));
			Assert.IsNotNull (r);
//			Console.WriteLine ("Reservation conflict 7/15-8/05 {0}", r.Type);
		}

		[Test]
		public void ShowConflictTypeOfFirstFoundWhenMoreThanOne()
		{
			var prop = Property.DemoProperty ();

			//Arrives during first and departs after end and before next
			var r = Reservation.Availability (prop.ID, new DateTime (2014, 07, 15), new DateTime (2014, 08, 05));
			Assert.IsNotNull (r);

			// Part of first and all of second
			r = Reservation.Availability (prop.ID, new DateTime (2014, 07, 15), new DateTime (2014, 09, 30));
			Assert.IsNotNull( r);
			Assert.AreEqual ("B", r.Type);
//			Console.WriteLine ("Reservation conflict 7/15-9/30 {0}", r.Type);

			// Part of first and all of second
			r = Reservation.Availability (prop.ID, new DateTime (2014, 07, 15), new DateTime (2014, 08, 15));
			Assert.IsNotNull( r);
			Assert.AreEqual ("B", r.Type);
//			Console.WriteLine ("Reservation conflict 7/15-9/15 {0}", r.Type);

			// All of first and all of second
			r = Reservation.Availability (prop.ID, new DateTime (2014, 07, 1), new DateTime (2014, 08, 15));
			Assert.IsNotNull( r);
			Assert.AreEqual ("B", r.Type);
//			Console.WriteLine ("Reservation conflict 7/1-8/15 {0}", r.Type);
		}

		[Test]
		public void NotBeAvailableWhenArriveIsDuringReservation ()
		{
			var prop = Property.DemoProperty ();
			Assert.IsNotNull(  Reservation.Availability (prop.ID, new DateTime (2014, 08, 15), new DateTime (2014, 09, 15)));
			Assert.IsNotNull(  Reservation.Availability (prop.ID, new DateTime (2014, 08, 15), new DateTime (2014, 08, 18)));
			Assert.IsNotNull(  Reservation.Availability (prop.ID, new DateTime (2014, 08, 10), new DateTime (2014, 08, 18)));
	
		}

		[Test]
		public void BeAvailableBeforeExisting()
		{
			var prop = Property.DemoProperty ();
			Assert.IsNull(  Reservation.Availability (prop.ID, new DateTime (2014, 02, 05), new DateTime (2014, 02, 15)));

			// Requesting arrival on the departure date of the existing reservation is on it is a TurnAround 
			Assert.IsNull(  Reservation.Availability (prop.ID, new DateTime (2014, 08, 31), new DateTime (2014, 09, 15)));

			Assert.IsNull(  Reservation.Availability (prop.ID, new DateTime (2014, 08, 01), new DateTime (2014, 08, 10)));

		}

		[Test]
		public void BeAvailableWhenRequestArriveOnAfterDepartOfExisting()
		{
			var prop = Property.DemoProperty ();
			// Requesting arrival on the departure date of the existing reservation is on it is a TurnAround 
			Assert.IsNull(  Reservation.Availability (prop.ID, new DateTime (2014, 08, 31), new DateTime (2014, 09, 15)));
			// Request is after existing reservtion
			Assert.IsNull(  Reservation.Availability (prop.ID, new DateTime (2014, 09, 1), new DateTime (2014, 09, 15)));
		}


		// Edge Cases
		[Test]
		public void BeAvailableWhenRequestDepartOnBeforeArriveOfExisting()
		{
			var prop = Property.DemoProperty ();
			// Requesting depart on the arrival date of the existing reservation is on it is a TurnAround 
			Assert.IsNull(  Reservation.Availability (prop.ID, new DateTime (2014, 08, 01), new DateTime (2014, 08, 10)));
			Assert.IsNull(  Reservation.Availability (prop.ID, new DateTime (2014, 08, 01), new DateTime (2014, 08, 09)));
		}

		[Test]
		public void RetrieveListOfConflicts()
		{
			var prop = Property.DemoProperty ();
			var r = Reservation.Conflicts (prop.ID, new DateTime (2014, 07, 15), new DateTime (2014, 08, 15));
			foreach (Reservation rev in r){
				Console.WriteLine ("Arrival Date = {0}", rev.ArrivalDate.ToShortDateString());
			}
			Assert.AreEqual (2, r.Count);
		}
	}
}

