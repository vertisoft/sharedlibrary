using System;
using NUnit.Framework;
using SharedLibrary.DAL;
using SharedLibrary.DL;


namespace SharedUnit
{
	[TestFixture()]
	public class FeesShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}


		[Test()]
		[ExpectedException( typeof( SharedLibrary.VsPropertyIDMisingException), ExpectedMessage="valid", MatchType=MessageMatch.Contains )]
		public void CreateFee ()
		{
			var demo = TestProperty ();
			var fee = new Fee ();
			fee.Name = "Cleaning";
			fee.Description = "Post departure cleaning";
			fee.Required = true;
			fee.Taxable = true;
			fee.Amount = 125.00m;
//
//			var message = "";
//			try{
				fee.Save();
//			}
//			catch(Exception e){
//				message = e.Message;
//			}
//			Assert.AreEqual (message, "PropertyID must be set to a valid property");
//			fee.PropertyID = demo.ID;
//			message = "";
//			try{
//				fee.Save();
//			}
//			catch(Exception e){
//				message = e.Message;
//			}
//			Assert.AreEqual (message, "");
		}

		Property TestProperty()
		{
			var prop = new Property ();
			prop.Name = "TestProperty";
			prop.Save ();
			return prop;

		}
	}
}

