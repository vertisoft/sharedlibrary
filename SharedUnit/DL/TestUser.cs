using System;
using System.IO;
using NUnit.Framework;
using SharedLibrary.DL;
using SharedLibrary.DAL;

namespace SharedUnit
{
	[TestFixture()]
	public class NewUserTest
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}


		[Test]
		public void ShouldCreateDefaultUser()
		{
//			var users = Repository.DB.Query<User> ("Select * from user");
//			Console.WriteLine ("TEst Number of users = {0}", users.Count);
			var user = User.FindUser ();
//			Console.WriteLine ("Default User = {0}, - {1} - {2}", user.ID, user.Name, user.DemoId);
//			Console.WriteLine ("Default User = {0}, - {1} - {2}", User.Current.ID, User.Current.Name, User.Current.DemoId);
			Assert.AreEqual(user.ID, User.Current.ID, "Found User and Current user are Not the same");
		}
	

		[Test()]
		public void ShouldSaveItSelf ()
		{
			var user = new User ();
			user.Name = "Theresa French";
			user.Pin = "1234";
			user.Save();
//			Console.WriteLine ("New User = {0}, - {1} - {2}", user.ID, user.Name, user.Pin);
			Assert.AreEqual ( "Theresa French", user.Name, "Saved User name is not correct");
		}
//	}
//
//	[TestFixture()]
//	public class TestExistingUser
//	{
//		[SetUp]
//		public void Setup()
//		{
//			Repository.DBName = "wizard.test.db3";
//			Repository.DropDatabase ();
//			Repository.Open ();
//		}
//
//		[TearDown]
//		public void Teardown()
//		{
//			Repository.Close ();
//		}


		[Test()]
		public void ShouldFindUserByPin()
		{
			var user1 = new User ();
			user1.Name = "Theresa French";
			user1.Pin = "1234";
			user1.Save();

			var user = User.FindByPin ("1234");
//			Console.WriteLine ("1235 = {0} - {1}", user.ID, user.Name);
			Assert.AreEqual ("Theresa French", user.Name );
		}

		[Test()]
		public void ShouldNotFindUserByBadPin()
		{
			var user1 = new User ();
			user1.Name = "Theresa French";
			user1.Pin = "1234";
			user1.Save();
			var user = User.FindByPin ("4321");
			Assert.IsNull (user);
		}

		[Test]
		public void ShouldAllowOnlyOneUser()
		{

			var user = new User ();
			user.Name = "Don French";
			user.Pin = "1234";
			user.Save ();
//			Console.WriteLine ("Only Saved id = {0} - {1} - {2}", user.ID, user.Name, user.Pin);
			var users = Repository.DB.Query<User> ("select * from User");
//			Console.WriteLine ("Number of Users after update = {0}", users.Count);
			Assert.AreEqual (1, users.Count);
			Assert.IsNull (User.FindByPin ("1235"));


		}




	}
}

