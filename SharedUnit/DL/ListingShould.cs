using System;
using NUnit.Framework;
using SharedLibrary.DAL;
using SharedLibrary.DL;

namespace SharedUnit
{
	[TestFixture]
	public class ListingShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}

		[Test]
		public void CreateNewListing()
		{
			var prop = TestProperty ();
			var list = prop.NewListing ();
			list.ChannelCode = "VRBO";
			list.ListingNumber = "209114";
			list.Save ();
			Assert.AreEqual (1, prop.ListingCount);
		}

		// also checks for find by code and number
		[Test]
		public void Update()
		{
			var prop = TestProperty ();
			var list = prop.NewListing ();
			list.ChannelCode = "VRBO";
			list.ListingNumber = "209114";
			list.Save ();
			list = prop.NewListing ();
			list.ChannelCode = "VRBO";
			list.ListingNumber = "1234";
			list.Save ();

			list.UserName = "testUser";
			list.Save ();
			list = Listing.FindByCodeAndNumber ("VRBO", "1234");
			Assert.IsNotNull (list);
			Assert.AreEqual ("testUser", list.UserName);
		}

		[Test]
		public void SaveWithDifferentCodeSameNumber()
		{
			var prop = TestProperty ();
			var list = prop.NewListing ();
			list.ChannelCode = "VRBO";
			list.ListingNumber = "209114";
			list.Save ();
			list = prop.NewListing ();
			list.ChannelCode = "HOME";
			list.ListingNumber = "209114";
			list.Save ();
			Assert.AreEqual (2, prop.ListingCount);
		}

		[Test]
		[ExpectedException( typeof( SQLite.SQLiteException), ExpectedMessage="Constraint", MatchType=MessageMatch.Contains )]
		public void NotUpdateDuplicates()
		{
			var prop = TestProperty ();
			var list = prop.NewListing ();
			list.ChannelCode = "VRBO";
			list.ListingNumber = "209114";
			list.Save ();
			list = prop.NewListing ();
			list.ChannelCode = "VRBO";
			list.ListingNumber = "1234";
			list.Save ();

			list.ListingNumber = "209114";
			list.Save ();

		}

		[Test]
		[ExpectedException( typeof( SQLite.SQLiteException), ExpectedMessage="Constraint", MatchType=MessageMatch.Contains )]
		public void NotSaveDuplicates()
		{
			var prop = TestProperty ();
			var list = prop.NewListing ();
			list.ChannelCode = "VRBO";
			list.ListingNumber = "209114";
			list.Save ();
			list = prop.NewListing ();
			list.ChannelCode = "VRBO";
			list.ListingNumber = "209114";
			list.Save ();

		}

		Property TestProperty()
		{
			var prop = new Property ();
			prop.Name = "Test Property";
			prop.Save ();
			return prop;
		}
	}
}

