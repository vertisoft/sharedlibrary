using System;
using NUnit.Framework;
using SharedLibrary.DAL;
using SharedLibrary.DL;


namespace SharedUnit
{
	[TestFixture]
	public class DepositShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}

		[Test]
		public void CreateNewDeposit ()
		{
			var demo = Property.DemoProperty ();
			var dep = demo.NewDeposit ();
//			var dep = new Deposit ();
			dep.Name = "Security";
			dep.Description = "Refundable Security deposit. It will be refunded 2 weeks after you depart, assuming no damage.";
			dep.Amount = 200.00m;
			dep.PropertyID = demo.ID;
			dep.Save ();
			Assert.AreNotEqual (0, dep.ID,  "Deposit did not save.");

		}
	}
}

