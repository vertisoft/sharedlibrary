using System;
using NUnit.Framework;
using SharedLibrary.DAL;
using SharedLibrary.DL;


namespace SharedUnit
{
	[TestFixture]
	public class TaxShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}

		[Test]
		public void CreateATax ()
		{
			var prop = TestProperty ();
			var tax = prop.NewTax();

			tax.Name = "HI Tax";
			tax.Description = "Hawaii state tax";
			tax.Percentage = 13.42m;
			tax.Save ();
			Assert.AreEqual (1, prop.TaxCount);

		}

		[Test]
		public void DeleteItSelf()
		{
			var prop = TestProperty ();
			var tax = prop.NewTax ();
			tax.Name = "Hawaii Tax";
			tax.Description = "Hawaii State GIT";
			tax.Percentage = 13.42m;
			tax.Required = true;
			tax.Save ();
			Assert.AreEqual (1, prop.TaxCount);
			tax.Delete ();
			Assert.AreEqual (0, prop.TaxCount);
		}

		Property TestProperty()
		{
			var prop = new Property ();
			prop.Name = "Test Property";
			prop.Save ();
			return prop;
		}



	}
}


