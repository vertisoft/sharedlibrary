using System;
using System.IO;
using NUnit.Framework;
using SharedLibrary.DAL;
using SharedLibrary.DL;

namespace SharedUnit
{
	[TestFixture]
	public class PropertyShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}

		[Test]
		public void HaveDemoPropety ()
		{
			var demo = Property.DemoProperty ();
			Assert.AreEqual ("Demonstration Property", demo.Name );
			Assert.AreEqual (196.00m, demo.Weekday);
			Assert.AreEqual (1, Property.Count);
			Assert.AreSame (demo, Property.CurrentProperty);
		}

		[Test]
		public void CreateAProperty()
		{
//			var demo = Property.DemoProperty ();
			var prop = new Property ();

			prop.Name = "Test Property";
			prop.ShortName = "TP";
			prop.Save ();
			Assert.AreNotEqual (0, prop.ID);
			Assert.AreEqual (2, Property.Count);
			Assert.AreSame (prop, Property.CurrentProperty);
		}



		[Test]
		public void FindFees() {
			var prop = TestProperty ();
			var fee = prop.NewFee();
			fee.Name = "Cleaning";
			fee.Amount = 125.00m;
			fee.Save ();
			fee = prop.NewFee ();
			fee.Name = "Damage Insurance";
			fee.Amount = 49.00m;
			fee.PropertyID = prop.ID;
			fee.Save ();

			Assert.AreEqual ( 2, prop.FeeCount, "Was not one fee");
		}


		[Test]
		public void FindDeposits() {
			var prop = TestProperty ();
			var dep = prop.NewDeposit();
			dep.Name = "Security";
			dep.Amount = 200.00m;
			dep.Save ();
			dep = prop.NewDeposit ();
			dep.Name = "Key";
			dep.Amount = 100.00m;
			dep.Save ();
			Assert.AreEqual (2, prop.DepositCount, "Was not two deposits");
		}


		[Test]
		public void FindReservedWords() {
			var prop = TestProperty ();
			var rw = prop.NewReservedWord();
			rw.Name = "Signature";
			rw.Text = "Donald French";
			rw.Save ();
			rw = prop.NewReservedWord ();
			rw.Name = "Phone";
			rw.Text = "702-555-1212";
			rw.Save ();
			Assert.AreEqual (2, prop.ReservedWordCount,  "Was not two Reserved Words");
		}


		[Test]
		public void FindTaxes() {

			var prop = TestProperty ();
			var tax = prop.NewTax ();
			tax.Name = "Test 1";
			tax.Percentage = 5.01m;
			tax.Save ();
			var tax1 = prop.NewTax ();
			tax1.Name = "Tax 2";
			tax1.Percentage = 10.00m;
			tax1.Save ();


			Assert.AreEqual (5.01m, Tax.FindByName (prop.ID, "Test 1").Percentage);
		}


		[Test]
		public void FindDiscount() {


			var prop = TestProperty ();
			var disc = prop.NewDiscount ();
			disc.Name = "Test Discount";
			disc.Percentage = 10.00m;
			disc.Save ();;
			Assert.AreEqual (1, prop.DiscountCount);

		}

		Property TestProperty()
		{
			var prop = new Property ();
			prop.Name = "Test Property";
			prop.Save ();
			return prop;
		}
	
	}
}

