using System;
using NUnit.Framework;
using SharedLibrary.DAL;
using SharedLibrary.DL;

namespace SharedUnit
{
	[TestFixture ()]
	public class ChannelShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}

		[Test]
		public void CreateChannel()
		{
			var chan = new Channel ();
			chan.Name = "Test";
			chan.ChannelCode = "TEST";
			chan.Save ();
			Assert.AreEqual (6, Channel.Count);
		}

		[Test]
		[ExpectedException( "SQLite.SQLiteException", ExpectedMessage="Constraint" )]
		public void NotAllowDuplicate()
		{
			var chan1 = new Channel();
			chan1.Name = "TEST";
			chan1.ChannelCode = "TEST";
			chan1.Save ();

			var chan2 = new Channel ();
			chan2.Name = "TEST2";
			chan2.ChannelCode = "TEST";
			chan2.Save ();
		}

		[Test]
		public void GetListOfChannels()
		{
			var chan1 = new Channel();
			chan1.Name = "TEST";
			chan1.ChannelCode = "TEST";
			chan1.Save ();

			var chan2 = new Channel ();
			chan2.Name = "TEST1";
			chan2.ChannelCode = "TEST1";
			chan2.Save ();

			Assert.AreEqual (7, Channel.Count);
			Assert.AreEqual (7, Channel.Channels ().Count);
//			foreach(Channel c in Channel.Channels()) {
//				Console.WriteLine ("{0} = {1}", c.Name, c.ChannelCode);
//			}
		}

		[Test]
		public void DeleteItSelf()
		{
			var chan1 = new Channel();
			chan1.Name = "TEST";
			chan1.ChannelCode = "TEST";
			chan1.Save ();

			var chan2 = new Channel ();
			chan2.Name = "TEST1";
			chan2.ChannelCode = "TEST1";
			chan2.Save ();
			Assert.AreEqual (7, Channel.Count);
			chan2.Delete ();
			Assert.AreEqual (6, Channel.Count);
		}

		[Test]
		public void UpdateItSelf()
		{
			var chan1 = new Channel();
			chan1.Name = "Vacation Rental By Owner";
			chan1.ChannelCode = "TEST";
			chan1.Save ();	
			chan1.Name = "TEST";
			chan1.Update ();
			var chan = Channel.Find (chan1.ID);
			Assert.IsNotNull (chan);
			Assert.AreEqual ("TEST", chan.Name);
		}

		[Test]
		public void FindByChannelCode()
		{
			var chan1 = new Channel();
			chan1.Name = "Test";
			chan1.ChannelCode = "TEST";
			chan1.Save ();

			var chan2 = new Channel ();
			chan2.Name = "Test1";
			chan2.ChannelCode = "TEST1";
			chan2.Save ();

			var result = Channel.FindByChannel ("TEST");
			Assert.AreEqual("Test",result.Name);
		}

	}
}

