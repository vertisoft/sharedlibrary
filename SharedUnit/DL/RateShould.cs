using System;
using NUnit.Framework;
using SharedLibrary.DAL;
using SharedLibrary.DL;

namespace SharedUnit
{
	[TestFixture]
	public class RateShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}

		[Test]
		public void CreateNewRate()
		{
			var prop = NewProperty ();
			var rate = prop.NewRate ();
			rate.Name = "Test Spring";
			rate.StartDate = new DateTime (2014, 6, 1); 
			rate.Save ();
			Assert.AreEqual (1, rate.Count);

			Assert.AreEqual ("6/1/2014", rate.StartDate.ToShortDateString());
			var rates = Repository.DB.Query<Rate> ("select * from Rate where PropertyID = ? and StartDate = ?", prop.ID, new DateTime (2014, 6, 1));
			Assert.AreEqual (1, rates.Count);
		}

		[Test]
		[ExpectedException( "SQLite.SQLiteException", ExpectedMessage="Constraint" )]
		public void NotAllowDuplicate()
		{
			var prop = NewProperty();
			var rate = prop.NewRate ();
			rate.Name = "Test Spring";
			rate.StartDate = new DateTime (2014, 6, 1); //, 7, 47, 0); 
			rate.Save ();

			rate = prop.NewRate ();
			rate.Name = "Test Spring";
			rate.StartDate = new DateTime (2014, 6, 1); //, 7, 47, 0); 
			rate.Save ();

		}

		[Test]
		[ExpectedException( "SQLite.SQLiteException", ExpectedMessage="Constraint" )]
		public void UpdateNotAllowDuplicate()
		{
			Console.WriteLine ("Start NotAllowDuplicates");
			var prop = NewProperty ();
			Console.WriteLine ("Property id = {0}", prop.ID);
			var rate = prop.NewRate ();
			Console.WriteLine ("rate.PropertyID = {0}", rate.PropertyID);
			rate.Name = "Test Spring";
			rate.StartDate = new DateTime (2014, 6, 1); //, 7, 47, 0); 
			rate.Save ();

			var rate1 = prop.NewRate ();
			rate1.Name = "Test Fall";
			rate1.StartDate = new DateTime (2014, 6, 1); //, 7, 47, 0); 
			rate1.Save ();
			Assert.AreEqual (2, rate.Count);

			rate1.Name = "Test Spring";
			rate1.Save ();
		}

		[Test]
		public void Update()
		{
			var prop = NewProperty ();
			var rate = prop.NewRate ();
			rate.Name = "Test Spring";
			rate.StartDate = new DateTime (2014, 6, 1); //, 7, 47, 0); 
			rate.Save ();

			rate.Name = "Spring Rate";
			rate.Save ();
			Assert.AreEqual (1, rate.Count);
		}


		[Test]
		public void DetermineRateWithTwoRates()
		{
			var prop = Property.DemoProperty ();
			// arrival 9/1/2014  depart 10/1/2014 covers two periods (default and Fall
			var arrive = new DateTime (2014, 09, 01);
			var depart = new DateTime (2014, 10, 10);
			var rates = Rate.WhichRate (prop, arrive, depart);
			Console.WriteLine ("Rate count = {0}", rates.Count);
			Assert.AreEqual (2, rates.Count);
		}

		[Test]
		public void DetermineRateWithOneRate()
		{
			var prop = Property.DemoProperty ();
			// arrival 3/15/2014  depart 3/22/2014 covers spring period
			var arrive = new DateTime (2014, 03, 15);
			var depart = new DateTime (2014, 03, 22);
			var rates = Rate.WhichRate (prop, arrive, depart);
			Console.WriteLine ("Rate count = {0}", rates.Count);
			Assert.AreEqual (1, rates.Count);
		}

		[Test]
		public void HandleBoundaryWhenDetermineRate()
		{
			var prop = Property.DemoProperty ();
			Assert.AreEqual (1, Rate.WhichRate (prop, new DateTime (2014, 08, 10),new DateTime (2014, 08, 31) ).Count);
			Assert.AreEqual (2, Rate.WhichRate (prop, new DateTime (2014, 08, 10),new DateTime (2014, 09,01) ).Count);
			Assert.AreEqual (1, Rate.WhichRate (prop, new DateTime (2014, 06, 1),new DateTime (2014, 06, 10) ).Count);
			Assert.AreEqual (2, Rate.WhichRate (prop, new DateTime (2014, 05, 31),new DateTime (2014, 06, 10) ).Count);
		}


// TODO PRO handel special rates
//		[Test]
//		public void HandleSpecialRates()
//		{
//			var prop = Property.DemoProperty ();
//			// Starts before ends in
//
////			Console.WriteLine ("Starts before ends during");
//			Assert.AreEqual ("Remaining Dates", Rate.WhichSpecialRate (prop, new DateTime (2014, 01, 10), new DateTime (2014, 02, 20)).Name);
//
////			Console.WriteLine (" ");
////			Console.WriteLine ("Starts before ends after");
//			Assert.AreEqual ("Remaining Dates", Rate.WhichSpecialRate (prop, new DateTime (2014, 01, 10), new DateTime (2014, 05, 20)).Name);
//
////			Console.WriteLine (" ");
////			Console.WriteLine ("Starts during ends during");
//			Assert.AreEqual ("Remaining Dates", Rate.WhichSpecialRate (prop, new DateTime (2014, 02, 20), new DateTime (2014, 02, 28)).Name);
//
////			Console.WriteLine (" ");
////			Console.WriteLine ("Starts during ends after");
//			Assert.AreEqual ("Remaining Dates", Rate.WhichSpecialRate (prop, new DateTime (2014, 02, 20), new DateTime (2014, 05, 28)).Name);
//
////			Console.WriteLine (" ");
////			Console.WriteLine ("Starts on ends after");
//			Assert.AreEqual ("Remaining Dates", Rate.WhichSpecialRate (prop, new DateTime (2014, 02, 1), new DateTime (2014, 05, 28)).Name);
//
////			Console.WriteLine (" ");
////			Console.WriteLine ("Starts on ends on");
//			Assert.AreEqual ("Remaining Dates", Rate.WhichSpecialRate (prop, new DateTime (2014, 02, 1), new DateTime (2014, 03, 30)).Name);
//
////			Console.WriteLine (" ");
////			Console.WriteLine ("NO SPECIAL Starts before ends before");
//			Assert.IsNull(Rate.WhichSpecialRate (prop, new DateTime (2013, 02, 20), new DateTime (2013, 02, 28)));
//
////			Console.WriteLine (" ");
////			Console.WriteLine ("NO SPECIAL Starts after ends after");
//			Assert.IsNull (Rate.WhichSpecialRate (prop, new DateTime (2015, 02, 20), new DateTime (2015, 02, 28)));
//		}

		[Ignore]
		[Test]
		public void NotAllowSeasonalDateOverlay() 
		{

		}


		Property NewProperty()
		{
			var prop = new Property ();
			prop.Name = "test Property";
			prop.Save ();
			return prop;

		}
	}
}

