using System;
using System.Reflection;

using NUnit.Framework;

using SharedLibrary.DL;
using System.CodeDom;
using SharedLibrary.DAL;

namespace SharedUnit
{
	[TestFixture()]
	public class TemplateShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}

		[Test()]
		public void TestCase ()
		{
			var prop = TestProperty ();
			var temp = prop.NewTemplate ();
			temp.Name = "My Name";
			Console.WriteLine ("temp = {0}", temp.GetType ());
			MethodInfo mi = temp.GetType().GetMethod("get_Name");
			Console.WriteLine ("MI = {0}", mi);
			var val = mi.Invoke(temp, null).ToString ();
			Console.WriteLine ("Name = {0}", val);

		}


		Property TestProperty()
		{
			var prop = new Property ();
			prop.Name = "Test Property";
			prop.Save ();
			return prop;
		}

		void ShowMethods(Type type)
		{
			foreach (var method in type.GetMethods()) {
				Console.WriteLine ("Method = {0}", method.Name);
//				var parameters = method.GetParameters();
			}
		}
	}


}

