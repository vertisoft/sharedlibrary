using System;
using NUnit.Framework;
using SharedLibrary.DL;
using SharedLibrary.DAL;
using SharedLibrary.LIB;

namespace SharedUnit
{
	[TestFixture ()]
	public class ReservedWordShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}

		[Test]
		public void CreateCustomWord ()
		{
			var demo = Property.DemoProperty ();
			var rw = demo.NewReservedWord ();;

			rw.Name = "My Name";
			rw.Text = "Donald French";
			rw.Description = "To be used as a signature";
			rw.Save ();

			Assert.AreNotEqual (0, rw.ID);
		}

		[Test]
		public void CreateListOfReservedWords()
		{
			var demo = Property.DemoProperty ();
			var list = demo.ReservedWords;
			Assert.AreEqual (53, list.Count);
			Assert.AreEqual ("PropertyName", list[0].KeyWord);
			foreach(Word w in list)
			{
				Console.WriteLine ("  {0} - {1}", w.KeyWord, w.WordType);
			}

		}
	
	}
}

