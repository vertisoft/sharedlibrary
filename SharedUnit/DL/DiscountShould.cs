using System;
using NUnit.Framework;
using SharedLibrary.DAL;
using SharedLibrary.DL;


namespace SharedUnit
{
	[TestFixture ()]
	public class DiscountShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}

		[Test]
		public void CreateNewDiscount()
		{
			var prop = TestProperty ();
			var disc = prop.NewDiscount ();
			disc.Name = "Military";
			disc.Percentage = 10.00m;
			disc.Save ();
//			Console.WriteLine ("Discount is {0} prop = {1} {2}", disc.ID, disc.PropertyID, prop.DiscountCount);
			Assert.AreEqual (1, prop.DiscountCount);
		}

		[Test]
		[ExpectedException( "SQLite.SQLiteException", ExpectedMessage="Constraint" )]
		public void NotAllowDuplicate()
		{
			var prop = TestProperty ();
			var disc = prop.NewDiscount ();
			disc.Name = "Military";
			disc.Percentage = 10.00m;
			disc.Save ();

			Assert.AreEqual (1, prop.DiscountCount);
			disc = prop.NewDiscount ();
			disc.Name = "Military";
			disc.Percentage = 10.00m;
			disc.Save ();
		}

		[Test]
		[ExpectedException( "SQLite.SQLiteException", ExpectedMessage="Constraint" )]
		public void UpdateNotAllowDuplicate()
		{
			var prop = TestProperty ();
			var disc = prop.NewDiscount ();
			disc.Name = "Military";
			disc.Percentage = 10.00m;
			disc.Save ();

			Assert.AreEqual (prop.DiscountCount, 1);
			disc = prop.NewDiscount ();
			disc.Name = "Returning";
			disc.Percentage = 10.00m;
			disc.Save ();

			disc.Name = "Military";
			disc.Save ();
		}

		[Test]
		public void Update()
		{
			var prop = TestProperty ();
			var disc = prop.NewDiscount ();
			disc.Name = "Military";
			disc.Percentage = 10.00m;
			disc.Save ();

			Assert.AreEqual (1, prop.DiscountCount);
			disc.Percentage = 15.00m;
			disc.Save ();

			var disc1 = Discount.FindByName (prop.ID, "Military");
			Assert.AreEqual (15.00m, disc1.Percentage);
		}

		Property TestProperty ()
		{
			var prop = new Property ();
			prop.Name = "Test Property";
			prop.Save ();
			return prop;
		}

	}
}

