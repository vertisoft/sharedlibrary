using System;
using NUnit.Framework;
using SharedLibrary.DL;
using SharedLibrary.DAL;


namespace SharedUnit
{
	[TestFixture ()]
	public class QuoteShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}

		[Test]
		public void InitializeItSelf ()
		{
			var prop = Property.DemoProperty();
			var inqs = prop.InquiryFindByStatus ("N");
			var inq = inqs [0];
			Console.WriteLine ("inquiries = {0}", inqs.Count);
			var quote = new Quote (inq);
			Assert.AreEqual (inq.ID, quote.InquiryID);
		}
		[Test]
		public void GenerateStandardQuote()
		{
			var prop = Property.DemoProperty ();
			var inqs = prop.InquiryFindByStatus ("N");
			var inq = inqs [0];
			var quote = Quote.StandardQuote (inq);
			Assert.AreNotEqual (0, quote.ID);
			Assert.AreEqual (905.32m, quote.TotalCharge);
			Assert.AreEqual (101.32m, quote.TotalTaxes);
			Assert.AreEqual (174.00m, quote.TotalFees);
			Assert.AreEqual (755.00m, quote.Taxable);

			var details = quote.Details ();
			Assert.AreEqual (3, details.Count);
			details = quote.DetailsByType (("FEE"));
			Assert.AreEqual (2, details.Count);
			Assert.AreEqual ("Cleaning", details [0].DetailName);
		}
	
		[Test]
		public void ReCalculateQuote()
		{
			var prop = Property.DemoProperty ();
			var inqs = prop.InquiryFindByStatus ("N");
			var inq = inqs [0];
			var quote = Quote.StandardQuote (inq);


			var adHoc = quote.NewDetail ("FEE");
			adHoc.DetailName = "Special Fee";
			adHoc.DetailAmount = 100.00m;
			adHoc.DetailTaxable = false;
			adHoc.Save ();
			Console.WriteLine ("adHoc quoteid = {0}", adHoc.QuoteID);
			quote.Recalculate ();
			var details = quote.Details ();
			Assert.AreEqual (4, details.Count);
//			foreach(QuoteDetail q in details)
//			{
//				Console.WriteLine("Detail name = {0} type = {1} amount = {2} percentage {3}", q.DetailName, q.DetailType, q.DetailAmount, q.DetailPercentage);
//			}
			details = quote.DetailsByType (("FEE"));
			Assert.AreEqual (3, details.Count);
			Assert.AreEqual ("Cleaning", details [0].DetailName);
			Assert.AreEqual (1005.32m, quote.TotalCharge);
		}

		[Test]
		public void CreateProperResponse()
		{
			var prop = Property.DemoProperty ();
			var inqs = prop.InquiryFindByStatus ("N");
			var inq = inqs [0];
			var quote = inq.StandardQuote ();
			Console.WriteLine ("Template to use is {0}", quote.DefaultTemplate(null));

//			var resp = quote.ResponseText (quote.DefaultTemplate (null));
			var resp = quote.ResponseContent ();
			Console.WriteLine ("Response = {0}", resp);
//			StringAssert.IsMatch ("arrival May 25, 2014", resp);
		}
	}
}

