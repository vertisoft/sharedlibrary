using System;
using NUnit.Framework;
using SharedLibrary.DAL;
using SharedLibrary.DL;

namespace SharedUnit
{
	[TestFixture]
	public class InquiryShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}

		[Test]
		public void CreateNewInquiry ()
		{
			var prop = Property.DemoProperty ();
			var inq = prop.NewInquiry ();
			inq.GuestName = "Theresa French";
			inq.ArrivalDate = new DateTime (2014, 08, 10);
			inq.DepartureDate = new DateTime (2014, 08, 21);
			inq.Adults = 3;

			inq.Save ();
			Assert.AreEqual( 8, Inquiry.Count);
			Assert.AreEqual ("NEW", inq.Status);
		}

		[Test]
		public void UpdateItSelf ()
		{
			var prop = Property.DemoProperty ();
			var inq = prop.NewInquiry ();
			inq.GuestName = "Theresa French";
			inq.ArrivalDate = new DateTime (2014, 08, 10);
			inq.DepartureDate = new DateTime (2014, 08, 21);
			inq.Adults = 2;

			inq.Save ();
			var  id = inq.ID;
			Assert.AreEqual (8, Inquiry.Count);
			Assert.AreEqual ("NEW", inq.Status);
			inq.Status = "RESPONDED";
			inq.Save ();

			inq = Inquiry.Find (inq.ID);
			Assert.AreEqual ("RESPONDED", inq.Status);
		}

		[Test]
		public void CreateReservation ()
		{
			var prop = Property.DemoProperty ();
			var inq = prop.NewInquiry ();
			inq.GuestName = "Theresa French";
			inq.ArrivalDate = new DateTime (2014, 08, 10);
			inq.DepartureDate = new DateTime (2014, 08, 21);
			inq.Adults = 2;
			inq.Save ();

			var res = inq.Reserve("R");
			Assert.AreEqual (new DateTime (2014, 08, 10), res.ArrivalDate);
			Assert.AreEqual (inq.ID, res.InquiryID);
			Assert.AreEqual (inq.PropertyID, res.PropertyID);
		}

		[Test]
		public void SaveNewQuote ()
		{
			var prop = Property.DemoProperty ();
			var inq = prop.NewInquiry ();
			inq.GuestName = "Theresa French";
			inq.ArrivalDate = new DateTime (2014, 08, 10);
			inq.DepartureDate = new DateTime (2014, 08, 21);
			inq.Adults = 2;
			inq.Save ();

			var q = inq.NewQuote ();
			inq.SaveNewQuote (q);
			Assert.AreNotEqual (0, q.ID);
			Assert.AreEqual (inq.ID, q.InquiryID);
		}

		[Test]
		public void CalculateNights ()
		{
			var prop = Property.DemoProperty ();
			var inq = prop.NewInquiry ();
			inq.GuestName = "Theresa French";
			inq.ArrivalDate = new DateTime (2014, 08, 10);
			inq.DepartureDate = new DateTime (2014, 08, 21);
			Assert.AreEqual (11, inq.Nights);
			inq = prop.NewInquiry ();
			inq.GuestName = "Theresa French";
			Assert.AreEqual (0, inq.Nights);
		}


	}
}

