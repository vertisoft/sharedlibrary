using System;
using System.IO;
using NUnit.Framework;
using SharedLibrary.DL;
using SharedLibrary.LIB;

namespace SharedUnit
{
	[TestFixture()]
	public class TestUser
	{
		[SetUp]
		public void Init()
		{
			var db = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.Personal), "VrWizard.test.db3");
			if(File.Exists(db))
			{
				Console.WriteLine ( "Setup Deleting {0}", db);
				File.Delete(db);
			}
			WizardRepository.DBName = "VrWizard.test.db3";
		}

		[Test()]
		public void ShouldSaveItSelf ()
		{
			var user = new User ();
			user.Name = "Don French";
			user.Pin = "1234";
			user.Save();
			Console.WriteLine ("User.ID = {0}", user.ID);
			Assert.That (user.ID, Is.EqualTo( 1 ));
		}
	}
}

