using System;
using System.IO;
using NUnit.Framework;
using SharedLibrary.DAL;
using Mono.Data.Sqlite;
using System.Data;



namespace SharedUnit
{
	[TestFixture()]
	public class WizardTest
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			//			Repository.DBName = "wizard.test.db3";
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}



//		[Test()]
//		public void WizardShouldCreateConnection ()
//		{
//			Repository.DBName = "VrWizard.test1.db3";
//			var conn = Repository.GetConnection (); 
//			Console.WriteLine ("DataBase = {0}", conn.DatabasePath);
//			Assert.That (conn.DatabasePath, Is.EqualTo("/Users/dhf/VrWizard.test1.db3"));
//		}

//		[Test()]
//		public void WizardShouldUseDefaultDB()
//		{
//			Assert.That (Repository.DBName, Is.EqualTo ("VrWizard.test1.db3"));
//		}
//
		[Test()]
		public void WizardShouldUseSpecifyDB()
		{
			Repository.DBName = "VrWizard.test1.db3";
			Assert.AreEqual ("VrWizard.test1.db3", Repository.DBName);
		}
	}
}

//
//		[Test]
//		public void ShouldUseDefaultURLIfNoneGiven()
//		{
//			Console.WriteLine ("Running TestURL");
//			new VrWizardApi("login", "password");
//			StringAssert.AreEqualIgnoringCase ("http://localhost:3000", VrWizardApi.BaseUrl);
//		}
//
//		[Test]
//		public void ShouldSetTheBaseUrl()
//		{
//			Console.WriteLine ("TEsting Should set BaseUrl");
//			VrWizardApi.BaseUrl = "http://vrcommand.herokuapp.com";
//			Assert.That (VrWizardApi.BaseUrl, Is.EqualTo("http://vrcommand.herokuapp.com"));
//			var api = new VrWizardApi();
//			Assert.That (api.CurrentUrl, Is.EqualTo ("http://vrcommand.herokuapp.com"));            
//		}
//
//		[Test]
//		public void ShouldLoginWithValidCredentials()
//		{
//			Console.WriteLine ("TestValidateAuthentication");
//			var client = new VrWizardApi ("http://localhost:3000");
//			StringAssert.AreEqualIgnoringCase ("SZ1MDyZnB3QWs9pWYntV", client.Authenticate ("dhf0820", "password"));
//			Assert.That(client.Authenticate ("dhf0820", "password"), Is.EqualTo("SZ1MDyZnB3QWs9pWYntV"));
//			StringAssert.AreEqualIgnoringCase ("SZ1MDyZnB3QWs9pWYntV", VrWizardApi.AuthToken);
//			Assert.That (VrWizardApi.AuthToken, Is.EqualTo ("SZ1MDyZnB3QWs9pWYntV"));
//		}
//
//		[Test]
//		public void ShouldAuthorizeWithValidCredentials()
//		{
//			Console.WriteLine ("Authorize");
//			Assert.That(VrWizardApi.Authorize ("dhf0820", "password"), Is.EqualTo("SZ1MDyZnB3QWs9pWYntV"));
//			Assert.That (VrWizardApi.AuthToken, Is.EqualTo ("SZ1MDyZnB3QWs9pWYntV"));
//		}
//
//
//		[Test]
//		public void ShouldRaiseExceptionOnNullUninialized()
//		{
//			Console.WriteLine ("TestExceptions");
//			Assert.That (() => VrWizardApi.AuthToken, Is.Null);
//			//			            Throws.InstanceOf<InvalidOperationException>());
//		}
//
//		[Test]
//		public void ShouldNotAcceptInvalidLogin()
//		{
//			var api = new VrWizardApi ("http://localhost:3000");
//			Assert.That (api.Authenticate ("dhf", "junk"), Is.EqualTo (null));
//		}
//
//		[Test]
//		public void CreatingClientShouldSetCurrentUrl()
//		{
//			var api = new VrWizardApi();
//			api.Client ("http://vrcommand.herokuapp.com");
//			Assert.That (api.CurrentUrl, Is.EqualTo ("http://vrcommand.herokuapp.com"));
//		}


//	}
//}

