using System;
using NUnit.Framework;
using SharedLibrary.DAL;
using SharedLibrary.LIB;
using SharedLibrary.DL;


namespace SharedUnit
{
	[TestFixture()]
	public class DemoCreatorShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}

		[Test()]
		public void HaveRates ()
		{
			var prop = Property.DemoProperty ();
			Assert.AreEqual (3, prop.RateCount);

//TODO PRO allow special rates
//			Assert.AreEqual (1, prop.RateTypeCount ("P"));
//			Assert.AreEqual (1, prop.RateTypeCount ("S"));
		}

		[Test]
		public void  HaveFees()
		{
			var prop = Property.DemoProperty ();
			Assert.AreEqual (3, prop.FeeCount);
		}

		[Test]
		public void HaveDiscounts()
		{
			var prop = Property.DemoProperty ();
			Assert.AreEqual (3, prop.DiscountCount);
		}

		[Test]
		public void HaveTaxes()
		{
			var prop = Property.DemoProperty ();
			Assert.AreEqual (2, prop.TaxCount);
		}

		[Test]
		public void HaveTemplates()
		{
			var prop = Property.DemoProperty ();
			Assert.AreEqual (3, prop.TemplateCount);
		}

		[Test]
		public void HaveContentInTemplate()
		{
			var prop = Property.DemoProperty ();
			var t = prop.TemplateByName ("Available with Quote");
			Console.WriteLine ("Template = {0}", t);
//			Assert. ("Your Vacation condo", t.Content);
		}

		[Test]
		public void HaveDamageDeposits()
		{
			var prop = Property.DemoProperty ();
			Assert.AreEqual (2, prop.Deposits ().Count);

		}
	}
}

