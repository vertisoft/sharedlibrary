using System;
using NUnit.Framework;
using SharedLibrary.DAL;
using SharedLibrary.LIB;
using SharedLibrary.DL;

namespace SharedUnit
{
	[TestFixture]
	public class QuoteCalculatorShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}

		[Test]
		public void InitializeItSelf ()
		{
			var prop = Property.DemoProperty();
			var inqs = prop.InquiryFindByStatus ("N");
			var inq = inqs [0];
			Console.WriteLine ("inquiries = {0}", inqs.Count);
			var quote = new QuoteCalculator (inq);
			Assert.AreEqual (inq.ID, quote.InquiryID);
		}

		[Test]
		public void GenerateNewQuote()
		{
			var prop = Property.DemoProperty();
			var inqs = prop.InquiryFindByStatus ("N");
			var inq = inqs [0];
//			Console.WriteLine ("Quoting for {0} - {1}", inq.ArrivalDate, inq.DepartureDate);
			var calc = new QuoteCalculator (inq);
			Assert.AreEqual (630.00m, calc.Standard().BaseRental);

			inq = prop.NewInquiry ();
			inq.ArrivalDate = new DateTime (2014, 08, 25);
			inq.DepartureDate = new DateTime (2014, 09, 24);
			inq.Save ();
//			Console.WriteLine ("setup quote for inquiry {0}", inq.ID);
//			Console.WriteLine ("Quoting for {0} - {1}", inq.ArrivalDate, inq.DepartureDate);
			calc = new QuoteCalculator (inq);
			Assert.AreEqual (4540m, calc.Standard().BaseRental);

			inq = prop.NewInquiry ();
			inq.ArrivalDate = new DateTime (2014, 05, 31);
			inq.DepartureDate = new DateTime (2014, 06, 2);
			inq.Save ();
//			Console.WriteLine ("setup quote for inquiry {0}", inq.ID);
//			Console.WriteLine ("Quoting for {0} - {1}", inq.ArrivalDate, inq.DepartureDate);
			calc = new QuoteCalculator (inq);
			Assert.AreEqual (266m, calc.Standard().BaseRental);


		}

		[Test]
		public void CalculateFees()
		{
			var prop = Property.DemoProperty();
			var inqs = prop.InquiryFindByStatus ("N");
			var inq = inqs [0];
			var calc = new QuoteCalculator (inq);
			var quote = calc.Standard ();
			Assert.AreEqual (174.00m, quote.TotalFees);
		}

		[Test]
		public void CalculateTaxes()
		{
			var prop = Property.DemoProperty();
			var inqs = prop.InquiryFindByStatus ("N");
			var inq = inqs [0];
			var calc = new QuoteCalculator (inq);
			var quote = calc.Standard ();
			Assert.AreEqual (101.32m, quote.TotalTaxes);
		}

		[Test]
		public void CalculateTotalCharge()
		{
			var prop = Property.DemoProperty();
			var inqs = prop.InquiryFindByStatus ("N");
			var inq = inqs [0];
			var calc = new QuoteCalculator (inq);
			var quote = calc.Standard ();
			Assert.AreEqual (905.32m, quote.TotalCharge);
		}




	}
}

