using NUnit.Framework;
using System;
using SharedLibrary.DAL;
using SharedLibrary.DL;
using SharedLibrary.LIB;
using SharedLibrary;
using Rebex.Mail;


namespace SharedUnit
{
	[TestFixture ()]
	public class MailProcessorShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			Repository.Open ();
			Rebex.Licensing.Key = "==AMfjFgXggp72it4EZQaMj6x+Qngsmw/UxeKaNFdizlMo==";
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}

		[Test ()]
		public void CreateVRBOInquiry ()
		{
			MailMessage mail = new MailMessage ();
			mail.Load (@"../../Samples/vrbo1.eml");
			   //MailBuilder ().CreateFromEmlFile (@"../../Samples/vrbo.eml");
//			string text = mail.GetBodyAsText();
//			Console.WriteLine ("Mail text = {0}", text);
			MailProcessor mp = new MailProcessor (mail);
			Inquiry inq = mp.ExtractInquiry ();
			Assert.AreEqual ("292391", inq.ListingNumber);
			Assert.AreEqual ("HOMEAWAY", inq.Channel);
			Assert.AreEqual (new DateTime(2013, 10, 10), inq.ArrivalDate);
			Assert.AreEqual (new DateTime(2013, 10, 17), inq.DepartureDate);
			StringAssert.AreEqualIgnoringCase ("Theresa French", inq.GuestName);
			Assert.AreEqual (2, inq.Adults);
			Assert.AreEqual (1, inq.Children);
			Assert.AreEqual ("7035039665", inq.Phone);
			Assert.AreEqual ("dhf0820@gmail.com", inq.Email);
			StringAssert.IsMatch ("209114", inq.Comment);
			Console.WriteLine ("Comment = [{0}]", inq.Comment);
		}
//
//		[Test ()]
//		public void CreateVRBOFwdInquiry ()
//		{
//			IMail mail = new MailBuilder ().CreateFromEmlFile (@"../../Samples/vrbo-fwd.eml");
////			string text = mail.GetBodyAsText();
//			MailProcessor mp = new MailProcessor (mail);
//			Inquiry inq = mp.ExtractInquiry ();
//			Assert.AreEqual ("292391", inq.ListingNumber);
//			Assert.AreEqual ("HOME", inq.Channel);
//			StringAssert.AreEqualIgnoringCase ("Theresa French", inq.GuestName);
//
//
//		}
	}
}

