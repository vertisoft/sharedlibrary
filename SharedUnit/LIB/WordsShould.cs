using NUnit.Framework;
using System;
using SharedLibrary.DL;
using SharedLibrary.DAL;
using SharedLibrary.LIB;

namespace SharedUnit
{
	[TestFixture]
	public class WordsShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}


		[Test]
		public void CreateListOfAllWords()
		{
			var prop = Property.DemoProperty ();
			var words = new Words (prop);
			words.FillWords	(prop.InquiryFindByStatus ("N")[0]);
			var wordList = words.ReservedWords;
			foreach(Word w in wordList)
			{
				Console.WriteLine ("Word = [{0}] ({1})  = [{2}]", w.KeyWord, w.WordType, w.StringValue);
			}
			var word = words.Find ("Aloha");
			Assert.IsNotNull (word);
			StringAssert.IsMatch ("FirstName", word.StringValue);

			Console.WriteLine ("Word [{0}] =  [{1}]", word.KeyWord, word.StringValue);


		}

		[Test]
		public void FindSpecificWord()
		{
			var prop = Property.DemoProperty ();
			var words = new Words (prop);
			words.FillWords	(prop.InquiryFindByStatus ("N")[0]);;
			var word = words.Find ("Aloha");
			Assert.IsNotNull (word);
			StringAssert.IsMatch ("FirstName", word.StringValue);

			Console.WriteLine ("Word [{0}]", word);

			Console.WriteLine ("word {0} ({1}) = {2}",word.KeyWord, word.WordType, word.StringValue );
			Console.WriteLine ("Like returns {0}", words.Like ("Alo").Count);
			Assert.AreEqual (1, words.Like ("Alo").Count);
			Assert.AreEqual (8, words.Like("Prop").Count);
		}

		[Test]
		public void FindSpecificWordWithFormating()
		{
			var prop = Property.DemoProperty ();
			var words = new Words (prop);
			words.FillWords	(prop.InquiryFindByStatus ("N")[0]);;
		}
	

	}
}

