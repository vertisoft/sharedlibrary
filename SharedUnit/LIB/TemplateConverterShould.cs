using NUnit.Framework;
using System;
using SharedLibrary.DL;
using SharedLibrary.LIB;
using SharedLibrary.DAL;



namespace SharedUnit
{
	[TestFixture()]
	public class TemplateConverterShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}

		[Test]
		public void Convert ()
		{
			Console.WriteLine ("Start Test");
			var prop = Property.DemoProperty ();
//			var words = new Words (prop);
			Console.WriteLine ("Calling Getting inquiry");
			var inq = prop.InquiryFindByStatus ("N")[0];
//			Console.WriteLine ("Calling FillWords");
//			var wordList = words.FillWords (inq);
			var tc = new TemplateConverter (inq, "{Today}\n\n{Aloha}\nWelcome to {PropertyLongName}. It is a wonderful place you are arriving {Arrivaldate} departing {departuredateshort} bringing {Guests}.");
			Console.WriteLine ("calling Convert");
			var results = tc.Convert ("Today is @Today@ for @GuestName@");
//			StringAssert.IsMatch ("arriving May 25, 2014", results);
			Console.WriteLine ("Results = {0}", results);
		}
//
//		[Test]
//		public void FindReservedWord ()
//		{
//			var prop = Property.DemoProperty ();
//			var inq = prop.InquiryFindByStatus ("N")[0];
//
//			var tc = new TemplateConverter (inq, "Welcome to {PropertyLongName}. It is {TotalCharge@R,20} ");
//			Console.WriteLine ("calling Convert");
//			var results = tc.Convert ();
//			Console.WriteLine ("Results = {0}", results);
//		}
//
		[Test]
		public void CreatesTemplateFromName ()
		{
			var prop = Property.DemoProperty ();
			var inq = prop.InquiryFindByStatus ("N")[0];
			var tc = new TemplateConverter (inq, null);
			Console.WriteLine ("calling Convert");
			var results = tc.Convert ("Available with Quote");
			Console.WriteLine ("Results = {0}", results);
			StringAssert.IsMatch ("arrival May 25, 2014", results);

		}
	}
}

