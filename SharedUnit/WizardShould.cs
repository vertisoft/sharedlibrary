using System;
using System.IO;
using SharedLibrary.DL;
using SharedLibrary.DAL;
 
using NUnit.Framework;

namespace SharedUnit
{
	[TestFixture]
	public class VrWizardShould
	{
		[SetUp]
		public void Setup()
		{
			Repository.DBName = "wizard.test.db3";
			Repository.DropDatabase ();
			Repository.Open ();
		}

		[TearDown]
		public void Teardown()
		{
			Repository.Close ();
		}

		[Test]
		public void ProcessAnInquiry()
		{
			var prop = Property.DemoProperty ();

			var inq = prop.InquiryFindByStatus ("N")[0];
			var qt = inq.StandardQuote ();
			Console.WriteLine ("Total = {0}", qt.TotalCharge);
			Assert.AreEqual (905.32m , qt.TotalCharge);
			qt.DefaultTemplate ("Available with Quote");
//			var template = qt.GetTemplateContent ("Available with Quote");
			var template = qt.ResponseContent ();
			StringAssert.Contains("is currently available", template);
			qt.DefaultTemplate("Available with Quote");
			var resp = qt.ResponseContent ();
			StringAssert.Contains("is currently available", resp);
			StringAssert.Contains ("2 Adults, No Children", resp);
		
		}

	}
}

